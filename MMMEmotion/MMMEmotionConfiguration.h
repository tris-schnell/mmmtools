#ifndef __MMMEmotion_H_
#define __MMMEmotion_H_

#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <string>
#include <iostream>
#include <VirtualRobot/RuntimeEnvironment.h>

/*!
    Configuration of XML motion completer.
    By default some standard parameters are set.
*/
struct MMMEmotionConfiguration
{
    //! Initialize with standard parameter set
    MMMEmotionConfiguration()
    {
//        motionFile = std::string(MMMEmotion_BASE_DIR)+std::string("/../data/Motions/sagittal_flexion_elbow_right.xml"); //original version
        motionFileBase = std::string(MMMEmotion_BASE_DIR)+std::string("/../data/Motions/MMMEmotion/processed/waving_sad0");
        motionFile = std::string(MMMEmotion_BASE_DIR)+std::string("/../data/Motions/MMMEmotion/processed/waving_neutral02.xml");
        motion2File = std::string(MMMEmotion_BASE_DIR)+std::string("/../data/Motions/MMMEmotion/processed/waving_sad02.xml");
        transformData = std::string(MMMEmotion_BASE_DIR)+std::string("/../data/Motions/MMMEmotion/transformData");
        transformFactors = std::string(MMMEmotion_BASE_DIR)+std::string("/../data/Motions/MMMEmotion/transformFactors");
        postureData = std::string(MMMEmotion_BASE_DIR)+std::string("/../data/Motions/MMMEmotion/postureData");
        postureValues = std::string(MMMEmotion_BASE_DIR)+std::string("/../data/Motions/MMMEmotion/postureValues");
        gmm = 0;
        dmp = 0;
        gmmFactorCalc = 0;
        gmmFactorCombine = 1;
        gmmSavedFactor = 0;
        collectPostureData = 0;
        usePostureData = 1;

        gmmStateNb = 20;
        relevantFactorPercent = 0.2;


    }

    //! checks for command line parameters and updates configuration accordingly.
    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("motion");
        VirtualRobot::RuntimeEnvironment::considerKey("eMotion");
        VirtualRobot::RuntimeEnvironment::considerKey("gmm");
        VirtualRobot::RuntimeEnvironment::considerKey("gmmstates");
        VirtualRobot::RuntimeEnvironment::considerKey("dmp");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        VirtualRobot::RuntimeEnvironment::addDataPath(MMMEmotion_BASE_DIR);

        if (VirtualRobot::RuntimeEnvironment::hasValue("motion"))
            motionFile = VirtualRobot::RuntimeEnvironment::getValue("motion");

        if (VirtualRobot::RuntimeEnvironment::hasValue("eMotion"))
            motion2File = VirtualRobot::RuntimeEnvironment::getValue("eMotion");

        if (VirtualRobot::RuntimeEnvironment::hasValue("gmm"))
            gmm = std::stof(VirtualRobot::RuntimeEnvironment::getValue("gmm"));

        if (VirtualRobot::RuntimeEnvironment::hasValue("gmmstates"))
            gmmStateNb = std::stof(VirtualRobot::RuntimeEnvironment::getValue("gmmstates"));

        if (VirtualRobot::RuntimeEnvironment::hasValue("dmp"))
            dmp = std::stof(VirtualRobot::RuntimeEnvironment::getValue("dmp"));

        if (!VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(motionFile))
        {
            std::cout << "Could not find MMM motion file" << std::endl;
            return false;
        }
        return true;
    }


    void print()
    {
        std::cout << "*** MMMEmotion Configuration ***" << std::endl;
        std::cout << "Motion file " << motionFile << std::endl;
    }

    std::string motionFileBase;
    std::string motionFile;
    std::string motion2File;
    std::string transformData;
    std::string transformFactors;
    std::string postureData;
    std::string postureValues;
    bool gmm;
    int gmmStateNb;
    bool dmp;
    bool gmmFactorCalc;
    bool gmmFactorCombine;
    bool gmmSavedFactor;
    bool collectPostureData;
    bool usePostureData;

    float relevantFactorPercent;
};

#endif //__MMMEmotionConfiguration_H_


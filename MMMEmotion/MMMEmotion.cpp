#include <iostream>
#include <cmath>
#include <Eigen/Core>
#include <boost/filesystem.hpp>
#include <3rdParty/gmm-gmr/gmr.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMMSimoxTools/MMMSimoxTools.h>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>

#include <VirtualRobot/RobotNodeSet.h>

#include "MMMEmotionConfiguration.h"

int loadFactors (float *factors, float *mufactors, MMMEmotionConfiguration c) {

    std::ifstream infile;
    infile.open(c.transformFactors);

    for (int i = 0; i < 43; i++) {
        infile >> factors[i];
    }

    for (int i = 0; i < 43; i++) {
        infile >> mufactors[i];
    }

    return 1;
}

int loadPosture (float *posture, MMMEmotionConfiguration c) {
    std::ifstream infile;
    infile.open(c.postureValues);

    for (int i = 0; i < 43; i++) {
        infile >> posture[i];
    }

    return 1;
}

GaussianMixture loadGMM(Matrix rawData, int nbData, int nbVar, int gmmstates) {

    GaussianMixture g;
    Matrix interpol, dataset;
    interpol.Resize(nbData,nbVar);
    g.HermitteSplineFit(rawData,nbData,interpol);
    dataset = interpol.VCat(dataset);

    g.initEM_TimeSplit(gmmstates,dataset); // initialize the model
    g.doEM(dataset); // performs EM

    return g;
}

GaussianMixture applyPosture(GaussianMixture g, float value, int var, int gmmstates) {
    float firstMu = g.mu(0,var);

    for (int i = 0; i < gmmstates; i++) {
        g.mu(i,var) = value + (g.mu(i,var) - firstMu);
    }

    return g;
}

float changeFactor(GaussianMixture g, GaussianMixture g2, int var, int gmmstates) {
    float avg = 0.f;
    float avg2 = 0.f;

    for (int i = 0; i < gmmstates; i++) {
        avg += std::abs(g.sigma[i](0,var));
        avg2 += std::abs(g2.sigma[i](0,var));
    }

    avg = avg / (float)gmmstates;
    avg2 = avg2 / (float)gmmstates;

    return avg2 / avg;
}

float muChangeFactor(GaussianMixture g, GaussianMixture g2, int var, int gmmstates) {
    float firstMu = g.mu(0,var);
    float firstMu2 = g2.mu(0,var);
    float avgMuChange = 0;
    float avgMuChange2 = 0;

    for (int i = 0; i < gmmstates; i++) {
        avgMuChange += std::abs(g.mu(i,var) - firstMu);
        avgMuChange2 += std::abs(g2.mu(i,var) - firstMu2);
    }

    avgMuChange = avgMuChange / (float) gmmstates;
    avgMuChange2 = avgMuChange2 / (float) gmmstates;

    return avgMuChange2 / avgMuChange;
}

float timeChangeFactor(GaussianMixture g, GaussianMixture g2, int gmmstates) {
    float avg = 0.f;
    float avg2 = 0.f;

    for (int i = 0; i < gmmstates; i++) {
        avg += std::abs(g.sigma[i](0,0));
        avg2 += std::abs(g2.sigma[i](0,0));
    }

    avg = avg / (float)gmmstates;
    avg2 = avg2 / (float)gmmstates;

    return avg2 / avg;
}

float timeMuChangeFactor(GaussianMixture g, GaussianMixture g2, int var, int gmmstates) {
    int maxcount = 4;
    float highMu[maxcount];
    float highMuT[maxcount];
    float highMu2[maxcount];
    float highMu2T[maxcount];

    for (int i = 0; i < maxcount; i++) {
        highMu[i] = 0;
        highMuT[i] = 0;
        highMu2[i] = 0;
        highMu2T[i] = 0;
    }

    for (int i = 1; i < gmmstates; i++) {
        float tmp = std::abs(g.mu(i,var));
        for (int k = 0; k < maxcount; k++) {
            if (tmp > highMu[k]) {
                for (int m = maxcount-1; m > k; m--) {
                    highMu[m] = highMu[m-1];
                    highMuT[m] = highMuT[m-1];
                }
                highMu[k] = tmp;
                highMuT[k] = g.mu(i,0);
                break;
            }
        }

        tmp = std::abs(g2.mu(i,var));
        for (int k = 0; k < maxcount; k++) {
            if (tmp > highMu2[k]) {
                for (int m = maxcount-1; m > k; m--) {
                    highMu2[m] = highMu2[m-1];
                    highMu2T[m] = highMu2T[m-1];
                }
                highMu2[k] = tmp;
                highMu2T[k] = g2.mu(i,0);
                break;
            }
        }
    }

    std::sort(highMuT, highMuT+maxcount);
    std::sort(highMu2T, highMu2T+maxcount);

    float tAvgDif = 0;
    float tAvgDif2 = 0;

    for (int i = 0; i < maxcount; i++) {
        //std::cout << highMuT[i] << " - " << highMu2T[i] << std::endl;
    }

    float highMuDif = highMuT[0] - highMuT[maxcount-1];
    float highMuDif2 = highMu2T[0] - highMu2T[maxcount-1];

    return highMuDif2 / highMuDif;
}

GaussianMixture changeGMMwithFactors(GaussianMixture g, float factor, float mufactor, int var, int gmmstates) {
    float firstMu = g.mu(0,var);

    for (int i = 0; i < gmmstates; i++) {
        g.sigma[i](0,var) = g.sigma[i](0,var) * factor;
        g.sigma[i](var,0) = g.sigma[i](0,var);

        g.mu(i,var) = firstMu + (g.mu(i,var) - firstMu)*mufactor;
    }

    return g;
}

GaussianMixture changeGMM(GaussianMixture g, GaussianMixture g2, int var, int gmmstates) {
    float factor =  changeFactor(g, g2, var, gmmstates);
    float mufactor = muChangeFactor(g, g2, var, gmmstates);

    g = changeGMMwithFactors(g, factor, mufactor, var, gmmstates);

    return g;
}

int collectPostureData(MMMEmotionConfiguration c) {

    float values[43];

    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr motion = r->loadMotion(c.motion2File,r->getMotionNames(c.motion2File)[0]);

    std::cout << "Loading motion: " << motion->getNumFrames() << std::endl;

    MMM::MotionFramePtr frame = motion->getMotionFrame(0);

    for (int i = 0; i < 3; i++) {
        values[i] = frame->getRootPos()[i];
        values[i+3] = frame->getRootRot()[i];
    }

    Eigen::VectorXf joint = frame->joint;

    for (int i = 0; i < 37; i++) {
        values[i+6] = joint[i];
    }

    std::ofstream f;
    f.open(c.postureData, std::fstream::app);

    for (int i = 0; i < 43; i++) {
        f << values[i] << " ";
    }
    f<<std::endl;

    f.close();
    return 1;
}

GaussianMixture calcGMM(std::vector<MMM::MotionFramePtr> frames, std::vector<MMM::MotionFramePtr> frames2, int nbData, int nbData2, int gmmstates, int startJoint, int endJoint) {

    GaussianMixture g;
    GaussianMixture g2;
    int nbVar = endJoint - startJoint + 2;
    Matrix rawData(nbData, nbVar);
    Matrix rawData2(nbData2, nbVar);



    //fill rawData
    int count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData(count,0) = frame->timestep;
        //root pos

        Eigen::VectorXf joint = frame->joint;

        for(int k = 1; k < nbVar; k++) {
            rawData(count, k) = joint[k+startJoint-1];
        }


        count++;
    }

    count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames2.begin(); i != frames2.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData2(count,0) = frame->timestep;
        //root pos

        Eigen::VectorXf joint = frame->joint;

        for(int k = 1; k < nbVar; k++) {
            rawData2(count, k) = joint[k+startJoint-1];
        }


        count++;
    }

    //first motion
    Matrix interpol, dataset;
    interpol.Resize(nbData,nbVar);
    std::cout << "hermitte" << std::endl;
    g.HermitteSplineFit(rawData,nbData,interpol);
    dataset = interpol.VCat(dataset);

    std::cout << "emtimesplit" << std::endl;
    g.initEM_TimeSplit(gmmstates,dataset); // initialize the model
    std::cout << "em" << std::endl;
    g.doEM(dataset); // performs EM

    //second motion
    Matrix interpol2, dataset2;
    interpol2.Resize(nbData2,nbVar);
    std::cout << "hermitte" << std::endl;
    g2.HermitteSplineFit(rawData2,nbData2,interpol2);
    dataset2 = interpol2.VCat(dataset2);

    std::cout << "emtimesplit" << std::endl;
    g2.initEM_TimeSplit(gmmstates,dataset2); // initialize the model
    std::cout << "em" << std::endl;
    g2.doEM(dataset2); // performs EM

    //change first motion based on difference to second
    for (int i = 1; i < nbVar; i++) {
        g = changeGMM(g, g2, i, gmmstates);
    }

    //regression
    Vector inC(1), outC(nbVar-1);
    inC(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
        outC(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData = interpol.GetColumnSpace(0,1);
    Matrix *outSigma;
    outSigma = new Matrix[nbData];
    Matrix outData = g.doRegression(inData,outSigma,inC,outC);



    //read outData
    count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;

        for(int k = 1; k < nbVar; k++) {
            frame->joint[k+startJoint-1] = outData(count, k-1);
        }

        count++;
    }


    return g;
}

GaussianMixture calcGMMAlt(std::vector<MMM::MotionFramePtr> frames, int nbData, int gmmstates, int startJoint, int endJoint) {

    GaussianMixture g;
    int nbVar = endJoint - startJoint + 2;
    Matrix rawData(nbData, nbVar);



    //fill rawData
    int count = 1;
    MMM::MotionFramePtr lastframe = *(frames.begin());
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = (frames.begin()+1); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData(count,0) = frame->timestep;
        //root pos

        Eigen::VectorXf joint = frame->joint;
        Eigen::VectorXf lastjoint = lastframe->joint;

        for(int k = 1; k < nbVar; k++) {
            rawData(count, k) = joint[k+startJoint-1] - lastjoint[k+startJoint-1];
        }


        count++;
    }


    Matrix interpol, dataset;
    interpol.Resize(nbData,nbVar);
    std::cout << "hermitte" << std::endl;
    g.HermitteSplineFit(rawData,nbData,interpol);
    dataset = interpol.VCat(dataset);

    std::cout << "emtimesplit" << std::endl;
    g.initEM_TimeSplit(gmmstates,dataset); // initialize the model
    std::cout << "em" << std::endl;
    g.doEM(dataset); // performs EM

    Vector inC(1), outC(nbVar-1);
    inC(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
        outC(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData = interpol.GetColumnSpace(0,1);
    Matrix *outSigma;
    outSigma = new Matrix[nbData];
    Matrix outData = g.doRegression(inData,outSigma,inC,outC);



    //read outData
    count = 1;
    lastframe = *(frames.begin());

    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;

        for(int k = 1; k < nbVar; k++) {
            frame->joint[k+startJoint-1] = lastframe->joint[k+startJoint-1] + outData(count, k-1);
        }

        lastframe = *i;
        count++;
    }


    return g;
}

int changeJointGMMfactors (float *factors, float *mufactors, float *posture, std::vector<MMM::MotionFramePtr> frames,
                           int nbData, int gmmstates, bool usePosture, int startJoint, int endJoint) {
    std::cout << "joint: " << startJoint << " - " << endJoint << std::endl;
    GaussianMixture g;
    int nbVar = endJoint - startJoint + 2;
    Matrix rawData(nbData, nbVar);

    //fill rawData
    int count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData(count,0) = frame->timestep;
        //root pos

        Eigen::VectorXf joint = frame->joint;

        for(int k = 1; k < nbVar; k++) {
            rawData(count, k) = joint[k+startJoint-1];
        }
        count++;
    }

    //first motion
    Matrix interpol, dataset;
    interpol.Resize(nbData,nbVar);
    g.HermitteSplineFit(rawData,nbData,interpol);
    dataset = interpol.VCat(dataset);

    g.initEM_TimeSplit(gmmstates,dataset); // initialize the model
    g.doEM(dataset); // performs EM


    if (usePosture) {
        for (int i = 1; i < nbVar; i++) {
            g = applyPosture(g, posture[i+startJoint+5], i, gmmstates);
        }
    }

    GaussianMixture g2 = g;

    for (int i = 1; i < nbVar; i++) {
        g = changeGMMwithFactors(g, factors[i+startJoint+5], mufactors[i+startJoint+5], i, gmmstates);
    }

    //TODO remove, basic test

    float firstmu = g.mu(0,0);
    for (int i = 0; i < gmmstates; i++) {
        g.mu(i,0) = firstmu + (g.mu(i,0) - firstmu) * 1.0;

        g.sigma[i](0,0) = g.sigma[i](0,0)*1.7;
    }

    //regression
    Vector inC(1), outC(nbVar-1);
    inC(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
        outC(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData = interpol.GetColumnSpace(0,1);
    Matrix *outSigma;
    outSigma = new Matrix[nbData];
    Matrix outData = g.doRegression(inData,outSigma,inC,outC);

    //regression g2
    Vector inC2(1), outC2(nbVar-1);
    inC2(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
        outC2(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData2 = interpol.GetColumnSpace(0,1);
    Matrix *outSigma2;
    outSigma2 = new Matrix[nbData];
    Matrix outData2 = g2.doRegression(inData2,outSigma2,inC2,outC2);



    std::ofstream f;
    f.open(std::string("/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/trajectories/")+std::to_string(startJoint)+std::string("wav"));
    std::ofstream f2;
    f2.open(std::string("/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/trajectories/")+std::to_string(startJoint)+std::string("wavgs"));
    std::ofstream f3;
    f3.open(std::string("/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/trajectories/")+std::to_string(startJoint)+std::string("wavgn"));

    //read outData
    count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;

        f << count;
        f2 << count;
        f3 << count;

        for(int k = 1; k < nbVar; k++) {
            f << " " << frame->joint[k+startJoint-1];
            f2 << " " << outData(count, k-1);
            f3 << " " << outData2(count, k-1);
            frame->joint[k+startJoint-1] = outData(count, k-1);
        }

        f << std::endl;
        f2 << std::endl;
        f3 << std::endl;

        count++;
    }

    f.close();
    f2.close();
    f3.close();

    return 1;
}

int GMMmotion (MMMEmotionConfiguration c) {

    //loading motion
    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr motion = r->loadMotion(c.motionFile,r->getMotionNames(c.motionFile)[0]);
    MMM::MotionPtr motion2 = r->loadMotion(c.motion2File, r->getMotionNames(c.motion2File)[0]);

    std::cout << "Loading motions: " << motion->getNumFrames() << " " << motion2->getNumFrames() << std::endl;

    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();
    std::vector<MMM::MotionFramePtr> frames2 = motion2->getMotionFrames();

    GaussianMixture g;
    GaussianMixture g2;
    int nbData = motion->getNumFrames();
    int nbData2 = motion2->getNumFrames();
    int nbVar = 7;
    Matrix rawData(nbData, nbVar);
    Matrix rawData2(nbData2, nbVar);


    //fill rawData
    int count = 0;

    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData(count,0) = frame->timestep;
        //root pos
        for(int k = 0; k < 3; k++) {
            rawData(count,k+1) = frame->getRootPos()[k];
        }
        //root rot
        for(int k = 0; k < 3; k++) {
            rawData(count,k+4) = frame->getRootRot()[k];
        }

        count++;
    }
    count = 0;

    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames2.begin(); i != frames2.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData2(count,0) = frame->timestep;
        //root pos
        for(int k = 0; k < 3; k++) {
            rawData2(count,k+1) = frame->getRootPos()[k];
        }
        //root rot
        for(int k = 0; k < 3; k++) {
            rawData2(count,k+4) = frame->getRootRot()[k];
        }

        count++;
    }

    //first motion
    Matrix interpol, dataset;
    interpol.Resize(nbData,nbVar);
    std::cout << "hermitte" << std::endl;
    g.HermitteSplineFit(rawData,nbData,interpol);
    dataset = interpol.VCat(dataset);

    std::cout << "emtimesplit" << std::endl;
    g.initEM_TimeSplit(c.gmmStateNb,dataset); // initialize the model
    std::cout << "em" << std::endl;
    g.doEM(dataset); // performs EM

    //second motion
    Matrix interpol2, dataset2;
    interpol2.Resize(nbData2,nbVar);
    std::cout << "hermitte" << std::endl;
    g2.HermitteSplineFit(rawData2,nbData2,interpol2);
    dataset2 = interpol2.VCat(dataset2);

    std::cout << "emtimesplit" << std::endl;
    g2.initEM_TimeSplit(c.gmmStateNb,dataset2); // initialize the model
    std::cout << "em" << std::endl;
    g2.doEM(dataset2); // performs EM

    for (int i = 1; i < nbVar; i++) {
        g = changeGMM(g, g2, i, c.gmmStateNb);
    }

    //regression
    Vector inC(1), outC(nbVar-1);
    inC(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
        outC(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData = interpol.GetColumnSpace(0,1);
    Matrix *outSigma;
    outSigma = new Matrix[nbData];
    Matrix outData = g.doRegression(inData,outSigma,inC,outC);

    /*
    Vector inC2(1), outC2(nbVar-1);
    inC2(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
        outC2(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData2 = interpol2.GetColumnSpace(0,1);
    Matrix *outSigma2;
    outSigma2 = new Matrix[nbData2];
    Matrix outData2 = g2.doRegression(inData2,outSigma2,inC2,outC2);
    regression on second motion not needed atm
    */
    //read outData    
    count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;

        //root pos
        Eigen::Vector3f pos;
        for(int k = 0; k < 3; k++) {
            pos[k] = outData(count, k);
        }
        frame->setRootPos(pos);

        //root rot
        Eigen::Vector3f rot;
        for(int k = 0; k < 3; k++) {
            rot[k] = outData(count, k+3);
        }
        frame->setRootRot(rot);

        count++;
    }

    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 0, 2);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 3, 5);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 6, 8);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 9, 11);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 12, 16);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 17, 20);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 21, 25);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 26, 30);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 31, 34);
    calcGMM(frames, frames2, nbData, nbData2, c.gmmStateNb, 35, 36);

    //saving motion
    motion->setName("walking_forward");

    std::string motionXML = motion->toXML();
    std::string filename = c.motionFile;

    boost::filesystem::path tmppath = boost::filesystem::canonical(boost::filesystem::path(filename));
    boost::filesystem::path path = tmppath.remove_filename() / boost::filesystem::path(tmppath.stem().generic_string() +"_gmm.xml");

    std::string filename_completed = path.generic_string();

    if (!MMM::XML::saveXML(filename_completed, motionXML))
    {
        std::cout << " Could not write to file " << filename_completed << std::endl;
    }

    return 1;
}

int GMMmotionAlt (MMMEmotionConfiguration c) {

    //loading motion
    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr motion = r->loadMotion(c.motionFile,r->getMotionNames(c.motionFile)[0]);

    std::cout << "Loading motion: " << motion->getNumFrames() << std::endl;

    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();

    GaussianMixture g;
    int nbData = motion->getNumFrames();
    int nbVar = 7;
    Matrix rawData(nbData, nbVar);



    //fill rawData
    int count = 1;
    MMM::MotionFramePtr lastframe = *(frames.begin());
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = (frames.begin()+1); i != frames.end(); ++i) {
         std::cout << "test" << std::endl;
        MMM::MotionFramePtr frame = *i;
        rawData(count,0) = frame->timestep;
        //root pos
        for(int k = 0; k < 3; k++) {
            rawData(count,k+1) = frame->getRootPos()[k] - lastframe->getRootPos()[k];
        }

        for(int k = 0; k < 3; k++) {
            rawData(count,k+4) = frame->getRootRot()[k] - lastframe->getRootRot()[k];
        }

        lastframe = *i;
        count++;
    }


    std::cout << nbVar << std::endl;

    Matrix interpol, dataset;
    interpol.Resize(nbData,nbVar);
    std::cout << "hermitte" << std::endl;
    g.HermitteSplineFit(rawData,nbData,interpol);
    dataset = interpol.VCat(dataset);

    std::cout << "emtimesplit" << std::endl;
    g.initEM_TimeSplit(c.gmmStateNb,dataset); // initialize the model
    std::cout << "em" << std::endl;
    g.doEM(dataset); // performs EM

    Vector inC(1), outC(nbVar-1);
    inC(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
        outC(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData = interpol.GetColumnSpace(0,1);
    Matrix *outSigma;
    outSigma = new Matrix[nbData];
    Matrix outData = g.doRegression(inData,outSigma,inC,outC);



    //read outData
    count = 1;
    lastframe = *(frames.begin());

    for (std::vector<MMM::MotionFramePtr>::const_iterator i = (frames.begin()+1); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;

        //root pos
        for(int k = 0; k < 3; k++) {
            frame->getRootPos()[k] = lastframe->getRootPos()[k] + outData(count, k);
        }

        for(int k = 0; k < 3; k++) {
            frame->getRootRot()[k] = lastframe->getRootRot()[k] + outData(count, k+3);
        }

        lastframe = *i;

        count++;
    }

    calcGMMAlt(frames, nbData, c.gmmStateNb, 0, 2);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 3, 5);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 6, 8);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 9, 11);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 12, 16);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 17, 20);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 21, 25);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 26, 30);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 31, 34);
    calcGMMAlt(frames, nbData, c.gmmStateNb, 35, 39);

    //saving motion
    motion->setName("walking_forward");

    std::string motionXML = motion->toXML();
    std::string filename = c.motionFile;

    boost::filesystem::path tmppath = boost::filesystem::canonical(boost::filesystem::path(filename));
    boost::filesystem::path path = tmppath.remove_filename() / boost::filesystem::path(tmppath.stem().generic_string() +"_gmm.xml");

    std::string filename_completed = path.generic_string();

    if (!MMM::XML::saveXML(filename_completed, motionXML))
    {
        std::cout << " Could not write to file " << filename_completed << std::endl;
    }

    return 1;
}

int DMPmotion (MMMEmotionConfiguration c) {

    //loading motion
    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr motion = r->loadMotion(c.motionFile,r->getMotionNames(c.motionFile)[0]);

    std::cout << "Loading motion: " << motion->getNumFrames() << std::endl;

    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();

    /*
    MMM::SampledTrajectoryV2 traj;
    traj = MMMConverter::fromMMMJoints(motion);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    */

    //saving motion
    motion->setName("walking_forward");

    std::string motionXML = motion->toXML();
    std::string filename = c.motionFile;

    boost::filesystem::path tmppath = boost::filesystem::canonical(boost::filesystem::path(filename));
    boost::filesystem::path path = tmppath.remove_filename() / boost::filesystem::path(tmppath.stem().generic_string() +"_dmp.xml");

    std::string filename_completed = path.generic_string();

    if (!MMM::XML::saveXML(filename_completed, motionXML))
    {
        std::cout << " Could not write to file " << filename_completed << std::endl;
    }

    return 1;
}

int BasicMotion(MMMEmotionConfiguration c) {

    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr motion = r->loadMotion(c.motionFile,r->getMotionNames(c.motionFile)[0]);
    //emotional motion
    MMM::MotionPtr motion2 = r->loadMotion(c.motionFile,r->getMotionNames(c.motion2File)[0]);

    std::cout << "Loading motion: " << motion->getNumFrames() << std::endl;

    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();


    float changeFactor[46];
    int count = 0;

    std::vector<MMM::MotionFramePtr>::const_iterator j = frames.begin();
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        //root pos
        for(int k = 0; k < 3; k++) {
            //changeFactor[k] += frame->getRootPos()[];
        }

        for(int k = 0; k < 3; k++) {
            //rawData(count,k+4) = frame->getRootRot()[k];
        }

        count++;
    }

    motion->setName("walking_forward");

    std::string motionXML = motion->toXML();
    std::string filename = c.motionFile;

    boost::filesystem::path tmppath = boost::filesystem::canonical(boost::filesystem::path(filename));
    boost::filesystem::path path = tmppath.remove_filename() / boost::filesystem::path(tmppath.stem().generic_string() +"_basic.xml");

    std::string filename_completed = path.generic_string();

    if (!MMM::XML::saveXML(filename_completed, motionXML))
    {
        std::cout << " Could not write to file " << filename_completed << std::endl;
    }


    return 1;
}

float calcGMMfactors(float *factors, float *mufactors, std::vector<MMM::MotionFramePtr> frames, std::vector<MMM::MotionFramePtr> frames2, int nbData, int nbData2, int gmmstates, int startJoint, int endJoint) {
    std::cout << "joint: " << startJoint << " - " << endJoint << std::endl;
    GaussianMixture g;
    GaussianMixture g2;
    int nbVar = endJoint - startJoint + 2;
    Matrix rawData(nbData, nbVar);
    Matrix rawData2(nbData2, nbVar);

    //fill rawData
    int count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData(count,0) = frame->timestep;
        //root pos

        Eigen::VectorXf joint = frame->joint;

        for(int k = 1; k < nbVar; k++) {
            rawData(count, k) = joint[k+startJoint-1];
        }


        count++;
    }

    count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames2.begin(); i != frames2.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData2(count,0) = frame->timestep;
        //root pos

        Eigen::VectorXf joint = frame->joint;

        for(int k = 1; k < nbVar; k++) {
            rawData2(count, k) = joint[k+startJoint-1];
        }


        count++;
    }

    //first motion
    Matrix interpol, dataset;
    interpol.Resize(nbData,nbVar);
    g.HermitteSplineFit(rawData,nbData,interpol);
    dataset = interpol.VCat(dataset);

    g.initEM_TimeSplit(gmmstates,dataset); // initialize the model
    g.doEM(dataset); // performs EM

    //second motion
    Matrix interpol2, dataset2;
    interpol2.Resize(nbData2,nbVar);
    g2.HermitteSplineFit(rawData2,nbData2,interpol2);
    dataset2 = interpol2.VCat(dataset2);

    g2.initEM_TimeSplit(gmmstates,dataset2); // initialize the model
    g2.doEM(dataset2); // performs EM

    float timefactor = 0;

    for(int k =  1; k < nbVar; k++) {
        factors[k+startJoint+5] = changeFactor(g, g2, k, gmmstates);
        mufactors[k+startJoint+5] = muChangeFactor(g, g2, k, gmmstates);
        std::cout << "timefactor: " << timeMuChangeFactor(g, g2, k, gmmstates) << std::endl;
        timefactor += timeMuChangeFactor(g, g2, k, gmmstates);
    }

    timefactor = timefactor / (float) (nbVar-1);
    std::cout << "avgtimefactor: " << timefactor << std::endl;
    return timefactor;
}

int changeMotionSavedFactors(MMMEmotionConfiguration c) {
    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr motion = r->loadMotion(c.motionFile,r->getMotionNames(c.motionFile)[0]);

    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();

    float factors[43];
    float mufactors[43];
    float posture[43];

    loadFactors(&factors[0], &mufactors[0], c);
    if (c.usePostureData) loadPosture(&posture[0], c);

    GaussianMixture g;
    int nbData = motion->getNumFrames();
    int nbVar = 7;
    Matrix rawData(nbData, nbVar);

    //fill rawData
    int count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData(count,0) = frame->timestep;
        //root pos
        for(int k = 0; k < 3; k++) {
            rawData(count,k+1) = frame->getRootPos()[k];
        }
        //root rot
        for(int k = 0; k < 3; k++) {
            rawData(count,k+4) = frame->getRootRot()[k];
        }

        count++;
    }

    //first motion
    Matrix interpol, dataset;
    interpol.Resize(nbData,nbVar);
    g.HermitteSplineFit(rawData,nbData,interpol);
    dataset = interpol.VCat(dataset);

    g.initEM_TimeSplit(c.gmmStateNb,dataset); // initialize the model
    g.doEM(dataset); // performs EM

    if (c.usePostureData) {
        for (int i = 1; i < nbVar; i++) {
            g = applyPosture(g, posture[i-1], i, c.gmmStateNb);
        }
    }

    for (int i = 1; i < nbVar; i++) {
        g = changeGMMwithFactors(g, factors[i-1], mufactors[i-1], i, c.gmmStateNb);
    }

    //TODO remove, basic test
    float firstmu = g.mu(0,0);
    for (int i = 0; i < c.gmmStateNb; i++) {
        g.mu(i,0) = firstmu + (g.mu(i,0) - firstmu) * 1.5;

        g.sigma[i](0,0) = g.sigma[i](0,0)*1.7;
    }

    //regression
    Vector inC(1), outC(nbVar-1);
    inC(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
        outC(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData = interpol.GetColumnSpace(0,1);
    /*Matrix inData(nbData*2,1);
    for (int i = 0; i < inData.RowSize(); i++) {
        inData(i,0) = i;
    }*/
    Matrix *outSigma;
    outSigma = new Matrix[nbData];//*2 if indata*2
    Matrix outData = g.doRegression(inData,outSigma,inC,outC);

    std::ofstream f;
    f.open("/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/trajectories/baseneutral");
    std::ofstream f2;
    f2.open("/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/trajectories/gmm");

    //read outData
    count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;

        f << count;
        f2 << count;

        //root pos
        Eigen::Vector3f pos;
        for(int k = 0; k < 3; k++) {
            f << " " <<frame->getRootPos()[k];
            f2 << " " << outData(count, k);
            pos[k] = outData(count, k);
        }
        frame->setRootPos(pos);

        //root rot
        Eigen::Vector3f rot;
        for(int k = 0; k < 3; k++) {
            f << " " <<frame->getRootRot()[k];
            f2 << " " << outData(count, k+3);
            rot[k] = outData(count, k+3);
        }
        frame->setRootRot(rot);

        f << std::endl;
        f2 << std::endl;

        count++;
    }

    f.close();
    f2.close();

    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 0, 2);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 3, 5);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 6, 8);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 9, 11);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 12, 16);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 17, 20);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 21, 25);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 26, 30);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 31, 34);
    changeJointGMMfactors(&factors[0], &mufactors[0], &posture[0], frames, nbData, c.gmmStateNb, c.usePostureData, 35, 36);

    std::string motionXML = motion->toXML();
    std::string filename = c.motionFile;

    boost::filesystem::path tmppath = boost::filesystem::canonical(boost::filesystem::path(filename));
    boost::filesystem::path path = tmppath.remove_filename() / boost::filesystem::path(tmppath.stem().generic_string() +"_gmmsad.xml");

    std::string filename_completed = path.generic_string();

    if (!MMM::XML::saveXML(filename_completed, motionXML))
    {
        std::cout << " Could not write to file " << filename_completed << std::endl;
    }

    return 1;
}

int combinePostures(MMMEmotionConfiguration c) {
    float values[43];

    for (int i = 0; i < 43; i++) {
        values[i] = 0;
    }

    std::ifstream infile(c.postureData);

    std::string  line;
    int linecount = 0;
    while (std::getline(infile, line)) {
        std::istringstream iss(line);
        int count = 0;
        float tmp;
        while (iss >> tmp)  {
            if (count < 43) {
                values[count] += tmp;
            } else {
                std::cout << "error in joint number" << std::endl;
            }
            count++;
        }
        linecount++;
    }

    for (int i = 0; i < 43; i++) {
        values[i] = values[i] / (float) linecount;
    }

    std::ofstream  f;
    f.open(c.postureValues);

    for (int i = 0; i < 43; i++) {
        f << values[i] << " ";
    }
    f << std::endl;
    f.close();

    return 1;
}

int combineFactors(MMMEmotionConfiguration c) {

    float factors[43];
    float sigcount[43];
    float mufactors[43];
    float mucount[43];

    for (int i = 0; i < 43; i++) {
        factors[i] = 0;
        sigcount[i] = 0;
        mufactors[i] = 0;
        mucount[i] = 0;
    }

    std::ifstream infile(c.transformData);

    std::string  line;
    int linecount = 0;
    while (std::getline(infile, line)) {
        std::istringstream iss(line);
        int count = 0;
        float tmp;
        while (iss >> tmp)  {
            if (count < 43) {
                //filter extremes
                if (tmp > 0.3 && tmp < 2.0) {
                    factors[count] += tmp;
                    sigcount[count] += 1;
                }
            } else if(count < 86) {
                if (tmp > 0.3 && tmp < 2.0) {
                    mufactors[count-43] += tmp;
                    mucount[count-43] += 1;
                }
            }
            count++;
        }
        linecount++;
    }

    for (int i  =  0; i < 43; i++) {
        std::cout << i << " - " << sigcount[i] << " - " << mucount[i] << std::endl;
        if(sigcount[i] < (linecount * c.relevantFactorPercent) ) {
            factors[i] = 1.0;
        } else {
            factors[i] = factors[i] / sigcount[i];
        }
        if(mucount[i] < (linecount * c.relevantFactorPercent) ) {
            mufactors[i] = 1.0;
        } else {
            mufactors[i] = mufactors[i] / mucount[i];
        }
    }

    std::ofstream  f;
    f.open(c.transformFactors);

    for (int i = 0; i < 43; i++) {
        f << factors[i] << " ";
    }

    for (int i = 0; i < 43; i++) {
        f << mufactors[i] << " ";
    }

    f << std::endl;
    f.close();

    if (c.usePostureData) combinePostures(c);

    return 1;
}

int calculateGMMTransform(MMMEmotionConfiguration c) {
    //loading motion
    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr motion = r->loadMotion(c.motionFile,r->getMotionNames(c.motionFile)[0]);
    MMM::MotionPtr motion2 = r->loadMotion(c.motion2File, r->getMotionNames(c.motion2File)[0]);

    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();
    std::vector<MMM::MotionFramePtr> frames2 = motion2->getMotionFrames();

    int nbData = motion->getNumFrames();
    int nbData2 = motion2->getNumFrames();
    int nbVar = 7;
    Matrix rawData(nbData, nbVar);
    Matrix rawData2(nbData, nbVar);

    int count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames.begin(); i != frames.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData(count,0) = frame->timestep;
        //root pos
        for(int k = 0; k < 3; k++) {
            rawData(count,k+1) = frame->getRootPos()[k];
        }
        //root rot
        for(int k = 0; k < 3; k++) {
            rawData(count,k+4) = frame->getRootRot()[k];
        }
        count++;
    }

    count = 0;
    for (std::vector<MMM::MotionFramePtr>::const_iterator i = frames2.begin(); i != frames2.end(); ++i) {
        MMM::MotionFramePtr frame = *i;
        rawData2(count,0) = frame->timestep;
        //root pos
        for(int k = 0; k < 3; k++) {
            rawData2(count,k+1) = frame->getRootPos()[k];
        }
        //root rot    MMM::MotionPtr motion = r->loadMotion(c.motionFile,r->getMotionNames(c.motionFile)[0]);
        for(int k = 0; k < 3; k++) {
            rawData2(count,k+4) = frame->getRootRot()[k];
        }
        count++;
    }

    GaussianMixture g = loadGMM(rawData, nbData, nbVar, c.gmmStateNb);
    GaussianMixture g2 = loadGMM(rawData2, nbData2, nbVar, c.gmmStateNb);

    float factors[43];
    float mufactors[43];
    float timefactor = 0;

    for (int i = 1; i < nbVar; i++) {
        factors[i-1] = changeFactor(g, g2, i, c.gmmStateNb);
        mufactors[i-1] = muChangeFactor(g, g2, i, c.gmmStateNb);
        std::cout << "timefactor: " << timeMuChangeFactor(g, g2, i, c.gmmStateNb) << std::endl;
        timefactor += timeMuChangeFactor(g, g2, i, c.gmmStateNb);
    }

    timefactor = timefactor / (float) (nbVar-1);
    std::cout << "avgtimefactor: " << timefactor << std::endl;
    float timefactors[11];
    timefactors[0] = timefactor;

    timefactors[1] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 0, 2);
    timefactors[2] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 3, 5);
    timefactors[3] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 6, 8);
    timefactors[4] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 9, 11);
    timefactors[5] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 12, 16);
    timefactors[6] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 17, 20);
    timefactors[7] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 21, 25);
    timefactors[8] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 26, 30);
    timefactors[9] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 31, 34);
    timefactors[10] = calcGMMfactors(&factors[0], &mufactors[0], frames, frames2, nbData, nbData2, c.gmmStateNb, 35, 36);

    std::sort(timefactors, timefactors+11);

    float avgtimefactor = 0;
    for(int i = 0; i < 11; i++) {
        avgtimefactor += timefactors[i];
    }
    avgtimefactor = avgtimefactor / 11.0;

    std::cout << "--------------------------------------" << std::endl;
    std::cout << "Overall Avg. Timefactor: " << avgtimefactor << std::endl;
    std::cout << "Median: " << timefactors[5] << std::endl;

    std::ofstream f;
    f.open(c.transformData, std::fstream::app);

    f << avgtimefactor << " ";

    for (int i = 0; i < 43; i++) {
        std::cout << factors[i] << " ";
        f << factors[i] << " ";
    }
    std::cout << std::endl << "------------"  << std::endl;

    for (int i = 0; i < 43; i++) {
        std::cout << mufactors[i] << " ";
        f << mufactors[i] << " ";
    }
    f<<std::endl;
    f.close();
    std::cout << std::endl << "------------"  << std::endl;

    return 1;
}

int calcAvgTrajectoryParams (MMMEmotionConfiguration c) {


    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr basemotion = r->loadMotion(c.motion2File, r->getMotionNames(c.motion2File)[0]);
    int fileNB = 4;
    int jointNB = basemotion->getJointNames().size();

    for (int k = 0; k < jointNB; k++) {
        std::cout << "joint " << k << std::endl;
        GaussianMixture g;
        Matrix rawData[fileNB];
        int totalnbData = 0;
        int nbVar = 2;

        for (int i = 0; i < fileNB; i++) {
            std::string filename = c.motionFileBase + std::to_string(i+1) + std::string(".xml");
            std::cout << filename << std::endl;
            MMM::MotionPtr motion = r->loadMotion(filename,r->getMotionNames(filename)[0]);
            int nbData = motion->getNumFrames();
            totalnbData += nbData;
            std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();

            Matrix tmpRaw(nbData, nbVar);



            int count = 0;
            for (std::vector<MMM::MotionFramePtr>::const_iterator it = frames.begin(); it != frames.end(); ++it) {
                MMM::MotionFramePtr frame = *it;
                tmpRaw(count,0) = frame->timestep;
                Eigen::VectorXf joint = frame->joint;
                tmpRaw(count, 1) = joint[k];

                count++;
            }

            rawData[i] = tmpRaw;
        }

        totalnbData = (int) (totalnbData/fileNB);

        Matrix interpol, dataset;
        interpol.Resize(totalnbData, nbVar);

        for (int i = 0; i < fileNB; i++) {
            g.HermitteSplineFit(rawData[i], totalnbData, interpol);
            dataset = interpol.VCat(dataset);
        }

        g.initEM_TimeSplit(c.gmmStateNb, dataset);
        g.doEM(dataset);

        char paramfile[1024];
        sprintf(paramfile, "/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/gmmparams/waving_sad_params%d", k);
        g.saveParams(paramfile);

        Vector inC(1), outC(nbVar-1);
        inC(0)=0; // Columns of the input data for regression (here, time)
        for(unsigned int i=0;i<nbVar-1;i++)
          outC(i)=(float)(i+1); // Columns for output : remainings
        Matrix inData(basemotion->getNumFrames(),1);
        for(int row = 0; row < inData.RowSize(); row++) {
            inData(row,0) = basemotion->getMotionFrame(row)->timestep * 100.0;
        }
        Matrix *outSigma;
        outSigma = new Matrix[basemotion->getNumFrames()];
        Matrix outData = g.doRegression(inData,outSigma,inC,outC);

        std::vector<MMM::MotionFramePtr> frames = basemotion->getMotionFrames();

        int count = 0;
        for (std::vector<MMM::MotionFramePtr>::const_iterator it = frames.begin(); it != frames.end(); ++it) {
            MMM::MotionFramePtr frame = *it;
            frame->joint[k] = outData(count,0);
            count++;
        }
    }

    std::string targetfile = std::string("/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/processed/waving_sad_avg");
    std::string motionXML = basemotion->toXML();
    if (!MMM::XML::saveXML(targetfile, motionXML))
    {
        std::cout << " Could not write to file " << targetfile << std::endl;
    }
}

double testPoint (GaussianMixture g, float time, float value, int states) {
    Vector point(2);
    point(0) = time*100.0;
    point(1) = value;

    double prob = 0;

    for (int i = 0; i < states; i++) {
        prob += g.pdfState(point, i);
    }

    return prob;
}

double testMSE (MMM::MotionPtr motion, GaussianMixture g, int jointindex) {

    int nbVar = 2;
    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();

    Vector inC(1), outC(nbVar-1);
    inC(0)=0; // Columns of the input data for regression (here, time)
    for(unsigned int i=0;i<nbVar-1;i++)
      outC(i)=(float)(i+1); // Columns for output : remainings
    Matrix inData(motion->getNumFrames(),1);
    for(int row = 0; row < inData.RowSize(); row++) {
        inData(row,0) = motion->getMotionFrame(row)->timestep * 100.0;
    }
    Matrix *outSigma;
    outSigma = new Matrix[motion->getNumFrames()];
    Matrix outData = g.doRegression(inData,outSigma,inC,outC);

    double MSE = 0;

    double count = 0;

    for (std::vector<MMM::MotionFramePtr>::const_iterator it = frames.begin(); it != frames.end(); ++it) {
        MMM::MotionFramePtr frame = *it;

        MSE += pow((frame->joint[jointindex] - outData(count, 0)),2.0);

        count += 1.0;
    }

    MSE = MSE / count;

    return MSE;
}

int test() {
    std::string testfile = std::string("/home/schnell/workspace/mmmtools/data/Motions/MMMEmotion/processed/walk_neutral05_gmmsad.xml");

    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());

    MMM::MotionPtr motion = r->loadMotion(testfile,r->getMotionNames(testfile)[0]);

    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();

    double totalprobsum = 0;
    int jointNB = motion->getJointNames().size();

    for (int k = 0; k < jointNB; k++) {
        GaussianMixture g;
        char paramfile[1024];
        sprintf(paramfile, "/home/schnell/workspace/mmmtools/data/Motions/MMMEmotion/gmmparams/walk_sad_params%d", k);
        g.loadParams(paramfile);

        double probsum = 0;
        double count = 0;
        for (std::vector<MMM::MotionFramePtr>::const_iterator it = frames.begin(); it != frames.end(); ++it) {
            MMM::MotionFramePtr frame = *it;

            probsum += testPoint(g, frame->timestep, frame->joint[k], 20);

            count += 1.0;
        }
        probsum = probsum / count;
        std::cout << probsum << std::endl;
        totalprobsum += probsum;
    }

    std::cout << "total: " << totalprobsum / (double) jointNB << std::endl;

    totalprobsum = 0;

    for (int k = 0; k < jointNB; k++) {
        GaussianMixture g;
        char paramfile[1024];
        sprintf(paramfile, "/home/schnell/workspace/mmmtools/data/Motions/MMMEmotion/gmmparams/walk_sad_params%d", k);
        g.loadParams(paramfile);

        totalprobsum += testMSE(motion, g, k);
    }

    //totalprobsum = totalprobsum / (double) jointNB;

    std::cout << "total MSE: " << totalprobsum << std::endl;

}

int manualPreprocessing () {
    std::string file = std::string("/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/converted/walk_sad05.xml");
    std::string targetfile = std::string("/home/SMBAD/schnell/home/mmmtools/data/Motions/MMMEmotion/processed/walk_sad05.xml");

    int deleteStart = 40;
    int deleteEnd = 0;

    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());

    MMM::MotionPtr motion = r->loadMotion(file,r->getMotionNames(file)[0]);

    //processing
    for (int i = 0; i < deleteStart; i++) {
        motion->removeMotionFrame(0);
    }

    std::vector<MMM::MotionFramePtr> frames = motion->getMotionFrames();

    for (std::vector<MMM::MotionFramePtr>::const_iterator it = frames.begin(); it != frames.end(); ++it) {
        MMM::MotionFramePtr frame = *it;

        frame->timestep = floor((frame->timestep - ((float) deleteStart)/100.0)*100+0.5)/100;
    }

    for (int i = 0; i < deleteEnd; i++) {
        motion->removeMotionFrame(motion->getNumFrames() - 1);
    }

    //save motion
    std::string motionXML = motion->toXML();
    if (!MMM::XML::saveXML(targetfile, motionXML))
    {
        std::cout << " Could not write to file " << targetfile << std::endl;
    }
}

int testworkspaceposition(){

    std::string file = std::string("/home/schnell/workspace/mmmtools/data/Motions/MMMEmotion/processed/walk_neutral01_dmpsad.xml");

    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());

    MMM::MotionPtr motion = r->loadMotion(file,r->getMotionNames(file)[0]);

    MMM::MotionFramePtr firstframe = motion->getMotionFrame(0);
    MMM::MotionFramePtr midframe = motion->getMotionFrame(100);
    MMM::MotionFramePtr lastframe = motion->getMotionFrame(motion->getNumFrames()-1);

    VirtualRobot::RobotPtr robot = MMM::SimoxTools::buildModel(motion->getModel(), false);

    std::vector<std::string> jointnames = motion->getJointNames();

    VirtualRobot::RobotNodeSetPtr kc = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "KC", jointnames, "", "", true);

    kc->setJointValues(firstframe->joint);

    int leftfoot = 10;
    int rightfoot = 24;
    int d = 2;
    float maxdist = 0;
    int maxindex = 0;
    float startz = kc->getNode(rightfoot)->getPositionInRootFrame()[2];
    float minz = startz;
    float maxzdist = 0;

    for (int i = 0; i < motion->getNumFrames(); i++) {
        kc->setJointValues(motion->getMotionFrame(i)->joint);

        Eigen::Vector3f r = kc->getNode(rightfoot)->getPositionInRootFrame();
        Eigen::Vector3f l = kc->getNode(leftfoot)->getPositionInRootFrame();

        float dist = std::sqrt((r[0]-l[0])*(r[0]-l[0]) + (r[1]-l[1])*(r[1]-l[1]));// + (r[2]-l[2])*(r[2]-l[2]));
        float zdist = r[2] - startz;

        if (r[2] < minz) {
            minz = r[2];
        }

        if (zdist > maxzdist) {
            maxzdist = zdist;
        }

        if (dist > maxdist) {
            maxindex = i;
            maxdist = dist;
        }

       std::cout << zdist << std::endl;
    }

    std::cout << "frame: " << maxindex << " dist: " << maxdist << std::endl;
    std::cout << "zdist: " << maxzdist << " minz: " << minz  << " startz: " << startz << std::endl;

    /*std::cout << kc->getNode(leftfoot)->getName() << std::endl;
    std::cout << kc->getNode(leftfoot)->getPositionInRootFrame() << std::endl;

    std::cout << kc->getNode(rightfoot)->getName() << std::endl;
    std::cout << kc->getNode(rightfoot)->getPositionInRootFrame() << std::endl;


    kc->setJointValues(midframe->joint);

    std::cout << kc->getNode(leftfoot)->getName() << std::endl;
    std::cout << kc->getNode(leftfoot)->getPositionInRootFrame() << std::endl;

    std::cout << kc->getNode(rightfoot)->getName() << std::endl;
    std::cout << kc->getNode(rightfoot)->getPositionInRootFrame() << std::endl;

    kc->setJointValues(lastframe->joint);

    std::cout << kc->getNode(leftfoot)->getName() << std::endl;
    std::cout << kc->getNode(leftfoot)->getPositionInRootFrame() << std::endl;

    std::cout << kc->getNode(rightfoot)->getName() << std::endl;
    std::cout << kc->getNode(rightfoot)->getPositionInRootFrame() << std::endl;*/


    //std::vector<VirtualRobot::RobotNodePtr> nodes = kc->getAllRobotNodes();

    //VirtualRobot::KinematicChain kcp;// = VirtualRobot::KinematicChain("test", robot, &nodes, nodes[nodes.size()-1], nodes[0]);

    //if (!kc->isKinematicChain()) std::cout << "bla" << std::endl;
  /*  std::vector<VirtualRobot::RobotNodePtr> nodes = kc->getAllRobotNodes();

    /*for (int i = 0; i < kc->getSize(); i++) {
        std::cout << i << nodes[i]->getName() << std::endl;
    }

    VirtualRobot::RobotNodePtr rightFootNode = nodes[24];

    std::cout << rightFootNode->getName() << " " << rightFootNode->getAllParents(kc).size() << std::endl;

    std::vector<VirtualRobot::RobotNodePtr> parentnodes = rightFootNode->getAllParents(kc);

    for (int i = 0; i < parentnodes.size(); i++) {
        std::cout << parentnodes[i]->getName() << std::cout;
    }*/

}

int main(int argc, char *argv[]) {

    std::cout << " --- MMMEmotion --- " << std::endl;
    MMMEmotionConfiguration c;
    if (!c.processCommandLine(argc,argv))
    {
        std::cout << "Error while processing command line, aborting..." << std::endl;
        return -1;
    }
/*
    if(c.gmmFactorCalc) calculateGMMTransform(c);
    if(c.gmmFactorCombine) combineFactors(c);
    if(c.collectPostureData) collectPostureData(c);
    if(c.gmmSavedFactor) changeMotionSavedFactors(c);
    if(c.gmm) GMMmotion(c);
    if(c.dmp) DMPmotion(c);

    calcAvgTrajectoryParams(c);
    //test();
    //manualPreprocessing();
*/

    testworkspaceposition();
    return 0;
}

#ifndef __MMMConverterGuiConfiguration_H_
#define __MMMConverterGuiConfiguration_H_

#include <MMM/ConverterFactory.h>

#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <string>
#include <iostream>
#include <VirtualRobot/RuntimeEnvironment.h>

// setup for converter plugin search, other search paths can be added with the command line parameter "libPath"
#ifdef WIN32
#define MMMConverterGUI_LIB_EXTENSION ".dll"
#ifdef _DEBUG
#define MMMConverterGUI_STANDARD_LIB_SEARCH_PATH MMMConverterGUI_BASE_DIR "/../build/bin/Debug"
#else
#define MMMConverterGUI_STANDARD_LIB_SEARCH_PATH MMMConverterGUI_BASE_DIR "/../build/bin/Release"
#endif
#else
#define MMMConverterGUI_LIB_EXTENSION ".so"
#define MMMConverterGUI_STANDARD_LIB_SEARCH_PATH MMMConverterGUI_BASE_DIR "/../build/lib/"
#endif

/*!
    Configuration of converter gui.
    By default some standard parameters are set.
*/
struct MMMConverterGuiConfiguration
{
    //! Initialize with standard parameter set
    MMMConverterGuiConfiguration()
    {
        converterName = "ConverterVicon2MMM";
        converterConfigFile = std::string(MMMConverterGUI_BASE_DIR)+std::string("/../data/ConverterVicon2MMM_WinterConfig.xml");
        mmmModelFile = std::string(MMMConverterGUI_BASE_DIR)+std::string("/../data/Model/Winter/mmm.xml");
        modelProcessorName = "Winter";
        modelProcessorConfigFile = std::string(MMMConverterGUI_BASE_DIR)+std::string("/../data/ModelProcessor_Winter_1.79.xml");
        viconInputFile = std::string(MMMConverterGUI_BASE_DIR)+std::string("/../data/WalkingStraightForward07.c3d");

        // init with standard lib path
        std::string testLibPath(MMMConverterGUI_STANDARD_LIB_SEARCH_PATH);
        converterLibSearchPaths.push_back(testLibPath);
	converterLibSearchPaths.push_back(std::string(MMMTools_LIB_DIR)); // Variable defined in MMMToolsConfig.cmake. Use ADD_DEFINITIONS() in your CMakeList.txt to make this path available at compile time.
    }


    // By going through all plugin libraries and initializing them, the ConverterFactories are available for later usage
    std::vector<MMM::ConverterFactoryPtr> checkForFactories(std::vector<std::string> &libPaths)
    {
        std::vector<MMM::ConverterFactoryPtr> result;

        for (size_t i = 0; i < libPaths.size(); i++)
        {

            std::string lp = libPaths[i];
            boost::filesystem::path targetDir(lp);
            bool isFile = false;

            try 
            {
                isFile = is_regular_file(targetDir);
            } catch (...)
            {
            }
            if (isFile)
            {
                MMM_ERROR << "Path '" <<  lp << "' is not a directory" << std::endl;
                continue;
            }
            try 
            {
                boost::filesystem::directory_iterator it(targetDir), eod;
                boost::filesystem::path libPath;
                //look through all files in MMMConverterGUI_STANDARD_LIB_SEARCH_PATH
                BOOST_FOREACH(boost::filesystem::path const &p, std::make_pair(it, eod))
                {
                    //MMM_INFO << "checking " << p.string() << std::endl;
                    if (p.extension().string() == MMMConverterGUI_LIB_EXTENSION && is_regular_file(p))
                    {

                        boost::extensions::shared_library lib(p.string());
                        if (lib.open())
                        {
                            boost::function<boost::shared_ptr<MMM::ConverterFactory>()> getFactory;
                            getFactory = lib.get<boost::shared_ptr<MMM::ConverterFactory> >("getFactory");
                            if (getFactory)
                            {
                                MMM::ConverterFactoryPtr converterFactory = getFactory();
                                result.push_back(converterFactory);
                            }
                        }
                        else
                        {
                            MMM_INFO << "Could not open lib " << p.string()  << std::endl;

                        }
                    }

                }
            } catch (...)
            {
            }
        }
        return result;
    }

    void setupConverterFactories()
    {
        // The factory pointers are not explicitly needed, but we need to load all plugins, so that the static AbstractFactory methods are available
        std::vector<MMM::ConverterFactoryPtr> factories = checkForFactories(converterLibSearchPaths);
        std::vector< std::string > factoryList = MMM::ConverterFactory::getSubclassList();
        MMM_INFO << "Available ConverterFactories: " << std::endl;
        for (size_t i = 0; i < factoryList.size(); i++)
        {
            std::cout << " * " << factoryList[i] << std::endl;
        }
    }

    //! checks for command line parameters and updates configuration accordingly.
    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("outputModel");
        VirtualRobot::RuntimeEnvironment::considerKey("outputModelProcessor");
        VirtualRobot::RuntimeEnvironment::considerKey("outputModelProcessorConfigFile");
        VirtualRobot::RuntimeEnvironment::considerKey("inputDataVicon");
        VirtualRobot::RuntimeEnvironment::considerKey("libPath");
        VirtualRobot::RuntimeEnvironment::considerKey("converter");
        VirtualRobot::RuntimeEnvironment::considerKey("converterConfigFile");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        VirtualRobot::RuntimeEnvironment::addDataPath(MMMConverterGUI_BASE_DIR);

        if (VirtualRobot::RuntimeEnvironment::hasValue("libPath"))
        {
            std::string libpaths = VirtualRobot::RuntimeEnvironment::getValue("libPath");
            std::vector<std::string> strs;
            std::string splitStr = ";,";
            boost::split(strs, libpaths, boost::is_any_of(splitStr));
            converterLibSearchPaths.insert(converterLibSearchPaths.end(), strs.begin(), strs.end());
        }

        if (VirtualRobot::RuntimeEnvironment::hasValue("outputModel"))
            mmmModelFile = VirtualRobot::RuntimeEnvironment::getValue("outputModel");

        if (VirtualRobot::RuntimeEnvironment::hasValue("outputModelProcessor"))
            modelProcessorName = VirtualRobot::RuntimeEnvironment::getValue("outputModelProcessor");

        if (VirtualRobot::RuntimeEnvironment::hasValue("outputModelProcessorConfigFile"))
            modelProcessorConfigFile = VirtualRobot::RuntimeEnvironment::getValue("outputModelProcessorConfigFile");

        if (VirtualRobot::RuntimeEnvironment::hasValue("converter"))
            converterName = VirtualRobot::RuntimeEnvironment::getValue("converter");

        if (VirtualRobot::RuntimeEnvironment::hasValue("converterConfigFile"))
            converterConfigFile = VirtualRobot::RuntimeEnvironment::getValue("converterConfigFile");

        if (VirtualRobot::RuntimeEnvironment::hasValue("inputDataVicon"))
        {
            viconInputFile = VirtualRobot::RuntimeEnvironment::getValue("inputDataVicon");
        }

        if (!VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(viconInputFile))
        {
            MMM_ERROR << "Could not find vicon input data file" << std::endl;
            return false;
        }

        if (!mmmModelFile.empty() && !VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(mmmModelFile))
        {
            MMM_ERROR << "Could not find output mmm model file " << mmmModelFile << std::endl;
            return false;
        }
        if (!mmmModelFile.empty() && !modelProcessorConfigFile.empty() && !VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(modelProcessorConfigFile))
            MMM_WARNING << "Could not find output model processor config file:" << modelProcessorConfigFile << std::endl;

        setupConverterFactories();

        if (!converterConfigFile.empty() && !VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(converterConfigFile))
            MMM_WARNING << "Could not find converter config file:" << converterConfigFile << std::endl;

        return true;
    }
    

    void print()
    {
        MMM_INFO << "*** MMMConverterGUI Configuration ***" << std::endl;
        std::cout << "Output MMM model " << mmmModelFile << std::endl;
        std::cout << "Output ModelProcessor " << modelProcessorName << std::endl;
        std::cout << "Output ModelProcessor config file " << modelProcessorConfigFile << std::endl;
        std::cout << "Using Vicon input data " << viconInputFile << std::endl;
        std::cout << "Using Converter " << converterName << std::endl;
        std::cout << "Using Converter config file " << converterConfigFile << std::endl;
    }

    std::string converterName;
    std::string converterConfigFile;
    std::string mmmModelFile;
    std::string modelProcessorName;
    std::string modelProcessorConfigFile;
    std::string viconInputFile;

    std::vector<std::string> converterLibSearchPaths;

};

#endif //__MMMConverterGuiConfiguration_H_


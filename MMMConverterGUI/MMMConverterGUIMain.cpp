
#include <VirtualRobot/VirtualRobotException.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/RuntimeEnvironment.h>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/Qt/SoQt.h>
#include <boost/shared_ptr.hpp>
#include <string>
#include <iostream>


#include <Eigen/Core>
#include <Eigen/Geometry>
#include "MMMConverterGUIWindow.h"
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionReaderC3D.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMM/ConverterFactory.h>

#include "MMMConverterGuiConfiguration.h"

using std::cout;
using std::endl;
using namespace VirtualRobot;

// exemplary command line options
// --converterConfigFile "../data/ConverterVicon2MMM_WinterConfig.xml" --converter "ConverterVicon2MMM" --outputModelProcessorConfigFile "../data/ModelProcessor_Winter_1.70.xml" --outputModelProcessor "Winter" --outputModel "../data/Model/Winter/mmm.xml" --inputDataVicon "../data/Motions/BendingLeftArm 1.c3d"
// --converterConfigFile "../data/ConverterVicon2MMM_BottleConfig.xml" --converter "RigidBodyConverterVicon2MMM" --outputModelProcessorConfigFile "" --outputModelProcessor "" --outputModel "../data/Model/Objects/Bottle/Bottle.xml" --inputDataVicon "../data/Motions/FetchBottle 7.c3d"
// --converterConfigFile "../data/ConverterVicon2MMM_BowlConfig.xml" --converter "RigidBodyConverterVicon2MMM" --outputModelProcessorConfigFile "" --outputModelProcessor "" --outputModel "../data/Model/Objects/OrangeBowl/Bowl201407.xml" --inputDataVicon "../data/Motions/FetchBottle 7.c3d"
// --converterConfigFile "../data/ConverterVicon2MMM_WinterConfig.xml" --converter "ConverterVicon2MMM" --outputModelProcessorConfigFile "../data/ModelProcessor_Winter_1.70.xml" --outputModelProcessor "Winter" --outputModel "../data/Model/Winter/mmm.xml" --inputDataVicon "../data/Motions/FetchBottle 7.c3d"
// --converterConfigFile "../data/ConverterVicon2MMM_WinterConfig.xml" --converter "ConverterVicon2MMM" --outputModelProcessorConfigFile "../data/ModelProcessor_Winter_1.80.xml" --outputModelProcessor "Winter" --outputModel "../data/Model/Winter/mmm.xml" --inputDataVicon "../data/Motions/walking_slow02.c3d"
// --converterConfigFile "../data/ConverterVicon2MMM_WinterConfig.xml" --converter "ConverterVicon2MMM" --outputModelProcessorConfigFile "../data/ModelProcessor_Winter_1.80.xml" --outputModelProcessor "Winter" --outputModel "../data/Model/Winter/mmm.xml" --inputDataVicon "../data/Motions/step_stones08.c3d"


int main(int argc, char *argv[])
{
	SoDB::init();
	SoQt::init(argc,argv,"MMMConverterGUI");

	cout << " --- MMMConverterGUI --- " << endl;
    if ((argc==2) && (std::string(argv[1]).compare("--help") == 0) )
    {
        std::cout << "Use the following syntax: " << std::endl;
        std::cout << "MMMConverterGUI --<parameter> <value>" << std::endl
                  << "where" << std::endl
                  << "<parameter>\t\t<value>" << std::endl \
                  << "converterConfigFile\tXML file containing converter configurations" << std::endl
                  << "converter\t\tName of the converter to use, for example 'RigidBodyConverterVicon2MMM'" << std::endl
                  << "outputModel\t\tXML file for the target model" << std::endl
                  << "inputDataVicon\t\tC3D file, containing the recorded marker data" << std::endl;
                     /*
                     --outputModelProcessorConfigFile ""
                     --outputModelProcessor ""
                     --outputModel "/home/SMBAD/terlemez/local/intern/model/objects/Beam/beam.xml"
                     --inputDataVicon "/home/SMBAD/terlemez/local/intern/model/objects/Beam/test.c3d"*/
        return 0;
    }
    MMMConverterGuiConfiguration c;
    if (!c.processCommandLine(argc,argv))
    {
        MMM_ERROR << "Error processing command line, aborting..." << endl;
        return -1;
    }
    c.print();

	MMMConverterGUIWindow mgw(c.mmmModelFile, c.viconInputFile, c.modelProcessorName, c.modelProcessorConfigFile, c.converterName, c.converterConfigFile);

	mgw.main();


	cout << " --- END --- " << endl;
	return 0;
}

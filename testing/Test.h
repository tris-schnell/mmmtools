/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_Test_H_
#define __MMM_Test_H_

#ifdef WIN32
    #include <boost/test/included/unit_test.hpp>
#else
    #define BOOST_TEST_DYN_LINK
    #include <boost/test/included/unit_test.hpp>
#endif

#include <string>
#include <map>
#include <fstream>
#include <iostream>

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

namespace testout
{
    /**
     * typedef for tee_device redirecting output stream to another output stream
     */
    typedef boost::iostreams::tee_device<std::ostream, std::ostream> ostream_tee_device;
    /**
     * typedef for tee_device to output stream redirection
     */
    typedef boost::iostreams::stream<ostream_tee_device> tee_ostream;
}

namespace MMM
{
    std::string getCmakeValue(const std::string& varName)
    {
        std::map<std::string, std::string> cmakeVars;
        cmakeVars["BUILDNAME"]="Linux-c++";
cmakeVars["BUILD_NAME_SYSTEM_NAME"]="Linux";
cmakeVars["BUILD_TESTING"]="ON";
cmakeVars["BZRCOMMAND"]="BZRCOMMAND-NOTFOUND";
cmakeVars["BZR_UPDATE_OPTIONS"]="";
cmakeVars["CMAKE_AR"]="/usr/bin/ar";
cmakeVars["CMAKE_BASE_NAME"]="g++";
cmakeVars["CMAKE_BINARY_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["CMAKE_BUILD_TOOL"]="/usr/bin/make";
cmakeVars["CMAKE_BUILD_TYPE"]="Debug";
cmakeVars["CMAKE_CFG_INTDIR"]=".";
cmakeVars["CMAKE_CODEBLOCKS_EXECUTABLE"]="CMAKE_CODEBLOCKS_EXECUTABLE-NOTFOUND";
cmakeVars["CMAKE_COLOR_MAKEFILE"]="ON";
cmakeVars["CMAKE_COMMAND"]="/usr/bin/cmake";
cmakeVars["CMAKE_COMPILER_IS_GNUCC"]="1";
cmakeVars["CMAKE_COMPILER_IS_GNUCXX"]="1";
cmakeVars["CMAKE_CPACK_COMMAND"]="/usr/bin/cpack";
cmakeVars["CMAKE_CROSSCOMPILING"]="FALSE";
cmakeVars["CMAKE_CTEST_COMMAND"]="/usr/bin/ctest";
cmakeVars["CMAKE_CURRENT_BINARY_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["CMAKE_CURRENT_LIST_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["CMAKE_CURRENT_LIST_FILE"]="/home/SMBAD/schnell/home/mmmtools/CMakeLists.txt";
cmakeVars["CMAKE_CURRENT_SOURCE_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["CMAKE_CXX_ABI_COMPILED"]="TRUE";
cmakeVars["CMAKE_CXX_ARCHIVE_APPEND"]="<CMAKE_AR> r  <TARGET> <LINK_FLAGS> <OBJECTS>";
cmakeVars["CMAKE_CXX_ARCHIVE_CREATE"]="<CMAKE_AR> cr <TARGET> <LINK_FLAGS> <OBJECTS>";
cmakeVars["CMAKE_CXX_ARCHIVE_FINISH"]="<CMAKE_RANLIB> <TARGET>";
cmakeVars["CMAKE_CXX_COMPILER"]="/usr/bin/c++";
cmakeVars["CMAKE_CXX_COMPILER_ABI"]="ELF";
cmakeVars["CMAKE_CXX_COMPILER_ARG1"]="";
cmakeVars["CMAKE_CXX_COMPILER_ENV_VAR"]="CXX";
cmakeVars["CMAKE_CXX_COMPILER_ID"]="GNU";
cmakeVars["CMAKE_CXX_COMPILER_ID_PLATFORM_CONTENT"]="/* Identify known platforms by name.  */\n#if defined(__linux) || defined(__linux__) || defined(linux)\n# define PLATFORM_ID \"Linux\"\n\n#elif defined(__CYGWIN__)\n# define PLATFORM_ID \"Cygwin\"\n\n#elif defined(__MINGW32__)\n# define PLATFORM_ID \"MinGW\"\n\n#elif defined(__APPLE__)\n# define PLATFORM_ID \"Darwin\"\n\n#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)\n# define PLATFORM_ID \"Windows\"\n\n#elif defined(__FreeBSD__) || defined(__FreeBSD)\n# define PLATFORM_ID \"FreeBSD\"\n\n#elif defined(__NetBSD__) || defined(__NetBSD)\n# define PLATFORM_ID \"NetBSD\"\n\n#elif defined(__OpenBSD__) || defined(__OPENBSD)\n# define PLATFORM_ID \"OpenBSD\"\n\n#elif defined(__sun) || defined(sun)\n# define PLATFORM_ID \"SunOS\"\n\n#elif defined(_AIX) || defined(__AIX) || defined(__AIX__) || defined(__aix) || defined(__aix__)\n# define PLATFORM_ID \"AIX\"\n\n#elif defined(__sgi) || defined(__sgi__) || defined(_SGI)\n# define PLATFORM_ID \"IRIX\"\n\n#elif defined(__hpux) || defined(__hpux__)\n# define PLATFORM_ID \"HP-UX\"\n\n#elif defined(__HAIKU__)\n# define PLATFORM_ID \"Haiku\"\n\n#elif defined(__BeOS) || defined(__BEOS__) || defined(_BEOS)\n# define PLATFORM_ID \"BeOS\"\n\n#elif defined(__QNX__) || defined(__QNXNTO__)\n# define PLATFORM_ID \"QNX\"\n\n#elif defined(__tru64) || defined(_tru64) || defined(__TRU64__)\n# define PLATFORM_ID \"Tru64\"\n\n#elif defined(__riscos) || defined(__riscos__)\n# define PLATFORM_ID \"RISCos\"\n\n#elif defined(__sinix) || defined(__sinix__) || defined(__SINIX__)\n# define PLATFORM_ID \"SINIX\"\n\n#elif defined(__UNIX_SV__)\n# define PLATFORM_ID \"UNIX_SV\"\n\n#elif defined(__bsdos__)\n# define PLATFORM_ID \"BSDOS\"\n\n#elif defined(_MPRAS) || defined(MPRAS)\n# define PLATFORM_ID \"MP-RAS\"\n\n#elif defined(__osf) || defined(__osf__)\n# define PLATFORM_ID \"OSF1\"\n\n#elif defined(_SCO_SV) || defined(SCO_SV) || defined(sco_sv)\n# define PLATFORM_ID \"SCO_SV\"\n\n#elif defined(__ultrix) || defined(__ultrix__) || defined(_ULTRIX)\n# define PLATFORM_ID \"ULTRIX\"\n\n#elif defined(__XENIX__) || defined(_XENIX) || defined(XENIX)\n# define PLATFORM_ID \"Xenix\"\n\n#else /* unknown platform */\n# define PLATFORM_ID \"\"\n\n#endif\n\n/* For windows compilers MSVC and Intel we can determine\n   the architecture of the compiler being used.  This is because\n   the compilers do not have flags that can change the architecture,\n   but rather depend on which compiler is being used\n*/\n#if defined(_WIN32) && defined(_MSC_VER)\n# if defined(_M_IA64)\n#  define ARCHITECTURE_ID \"IA64\"\n\n# elif defined(_M_X64) || defined(_M_AMD64)\n#  define ARCHITECTURE_ID \"x64\"\n\n# elif defined(_M_IX86)\n#  define ARCHITECTURE_ID \"X86\"\n\n# elif defined(_M_ARM)\n#  define ARCHITECTURE_ID \"ARM\"\n\n# elif defined(_M_MIPS)\n#  define ARCHITECTURE_ID \"MIPS\"\n\n# elif defined(_M_SH)\n#  define ARCHITECTURE_ID \"SHx\"\n\n# else /* unknown architecture */\n#  define ARCHITECTURE_ID \"\"\n# endif\n\n#else\n#  define ARCHITECTURE_ID \"\"\n#endif\n\n/* Convert integer to decimal digit literals.  */\n#define DEC(n)                   \\\n  ('0' + (((n) / 10000000)%10)), \\\n  ('0' + (((n) / 1000000)%10)),  \\\n  ('0' + (((n) / 100000)%10)),   \\\n  ('0' + (((n) / 10000)%10)),    \\\n  ('0' + (((n) / 1000)%10)),     \\\n  ('0' + (((n) / 100)%10)),      \\\n  ('0' + (((n) / 10)%10)),       \\\n  ('0' +  ((n) % 10))\n\n/* Convert integer to hex digit literals.  */\n#define HEX(n)             \\\n  ('0' + ((n)>>28 & 0xF)), \\\n  ('0' + ((n)>>24 & 0xF)), \\\n  ('0' + ((n)>>20 & 0xF)), \\\n  ('0' + ((n)>>16 & 0xF)), \\\n  ('0' + ((n)>>12 & 0xF)), \\\n  ('0' + ((n)>>8  & 0xF)), \\\n  ('0' + ((n)>>4  & 0xF)), \\\n  ('0' + ((n)     & 0xF))\n\n/* Construct a string literal encoding the version number components. */\n#ifdef COMPILER_VERSION_MAJOR\nchar const info_version[] = {\n  'I', 'N', 'F', 'O', ':',\n  'c','o','m','p','i','l','e','r','_','v','e','r','s','i','o','n','[',\n  COMPILER_VERSION_MAJOR,\n# ifdef COMPILER_VERSION_MINOR\n  '.', COMPILER_VERSION_MINOR,\n#  ifdef COMPILER_VERSION_PATCH\n   '.', COMPILER_VERSION_PATCH,\n#   ifdef COMPILER_VERSION_TWEAK\n    '.', COMPILER_VERSION_TWEAK,\n#   endif\n#  endif\n# endif\n  ']','\\0'};\n#endif\n\n/* Construct the string literal in pieces to prevent the source from\n   getting matched.  Store it in a pointer rather than an array\n   because some compilers will just produce instructions to fill the\n   array rather than assigning a pointer to a static array.  */\nchar const* info_platform = \"INFO\" \":\" \"platform[\" PLATFORM_ID \"]\";\nchar const* info_arch = \"INFO\" \":\" \"arch[\" ARCHITECTURE_ID \"]\";\n\n";
cmakeVars["CMAKE_CXX_COMPILER_ID_RUN"]="1";
cmakeVars["CMAKE_CXX_COMPILER_ID_TEST_FLAGS"]="-c";
cmakeVars["CMAKE_CXX_COMPILER_ID_VENDORS"]="IAR";
cmakeVars["CMAKE_CXX_COMPILER_ID_VENDOR_REGEX_IAR"]="IAR .+ Compiler";
cmakeVars["CMAKE_CXX_COMPILER_INIT"]="NOTFOUND";
cmakeVars["CMAKE_CXX_COMPILER_LIST"]="c++;CC;g++;aCC;cl;bcc;xlC;clang++";
cmakeVars["CMAKE_CXX_COMPILER_LOADED"]="1";
cmakeVars["CMAKE_CXX_COMPILER_NAMES"]="c++";
cmakeVars["CMAKE_CXX_COMPILER_VERSION"]="4.8.4";
cmakeVars["CMAKE_CXX_COMPILER_WORKS"]="TRUE";
cmakeVars["CMAKE_CXX_COMPILE_OBJECT"]="<CMAKE_CXX_COMPILER>  <DEFINES> <FLAGS> -o <OBJECT> -c <SOURCE>";
cmakeVars["CMAKE_CXX_COMPILE_OPTIONS_PIC"]="-fPIC";
cmakeVars["CMAKE_CXX_COMPILE_OPTIONS_PIE"]="-fPIE";
cmakeVars["CMAKE_CXX_COMPILE_OPTIONS_VISIBILITY"]="-fvisibility=";
cmakeVars["CMAKE_CXX_COMPILE_OPTIONS_VISIBILITY_INLINES_HIDDEN"]="-fvisibility-inlines-hidden";
cmakeVars["CMAKE_CXX_CREATE_ASSEMBLY_SOURCE"]="<CMAKE_CXX_COMPILER> <DEFINES> <FLAGS> -S <SOURCE> -o <ASSEMBLY_SOURCE>";
cmakeVars["CMAKE_CXX_CREATE_PREPROCESSED_SOURCE"]="<CMAKE_CXX_COMPILER> <DEFINES> <FLAGS> -E <SOURCE> > <PREPROCESSED_SOURCE>";
cmakeVars["CMAKE_CXX_CREATE_SHARED_LIBRARY"]="<CMAKE_CXX_COMPILER> <CMAKE_SHARED_LIBRARY_CXX_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>";
cmakeVars["CMAKE_CXX_CREATE_SHARED_MODULE"]="<CMAKE_CXX_COMPILER> <CMAKE_SHARED_LIBRARY_CXX_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>";
cmakeVars["CMAKE_CXX_FLAGS"]=" -std=gnu++0x";
cmakeVars["CMAKE_CXX_FLAGS_DEBUG"]="-g";
cmakeVars["CMAKE_CXX_FLAGS_DEBUG_INIT"]="-g";
cmakeVars["CMAKE_CXX_FLAGS_MINSIZEREL"]="-Os -DNDEBUG";
cmakeVars["CMAKE_CXX_FLAGS_MINSIZEREL_INIT"]="-Os -DNDEBUG";
cmakeVars["CMAKE_CXX_FLAGS_RELEASE"]="-O3 -DNDEBUG";
cmakeVars["CMAKE_CXX_FLAGS_RELEASE_INIT"]="-O3 -DNDEBUG";
cmakeVars["CMAKE_CXX_FLAGS_RELWITHDEBINFO"]="-O2 -g -DNDEBUG";
cmakeVars["CMAKE_CXX_FLAGS_RELWITHDEBINFO_INIT"]="-O2 -g -DNDEBUG";
cmakeVars["CMAKE_CXX_IGNORE_EXTENSIONS"]="inl;h;hpp;HPP;H;o;O;obj;OBJ;def;DEF;rc;RC";
cmakeVars["CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES"]="/usr/include";
cmakeVars["CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES"]="/usr/lib/gcc/x86_64-linux-gnu/4.8;/usr/lib/x86_64-linux-gnu;/usr/lib;/lib/x86_64-linux-gnu;/lib";
cmakeVars["CMAKE_CXX_IMPLICIT_LINK_FRAMEWORK_DIRECTORIES"]="";
cmakeVars["CMAKE_CXX_IMPLICIT_LINK_LIBRARIES"]="stdc++;m;c";
cmakeVars["CMAKE_CXX_INFORMATION_LOADED"]="1";
cmakeVars["CMAKE_CXX_LIBRARY_ARCHITECTURE"]="x86_64-linux-gnu";
cmakeVars["CMAKE_CXX_LINKER_PREFERENCE"]="30";
cmakeVars["CMAKE_CXX_LINKER_PREFERENCE_PROPAGATES"]="1";
cmakeVars["CMAKE_CXX_LINK_EXECUTABLE"]="<CMAKE_CXX_COMPILER>  <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>";
cmakeVars["CMAKE_CXX_OUTPUT_EXTENSION"]=".o";
cmakeVars["CMAKE_CXX_PLATFORM_ID"]="Linux";
cmakeVars["CMAKE_CXX_SIZEOF_DATA_PTR"]="8";
cmakeVars["CMAKE_CXX_SOURCE_FILE_EXTENSIONS"]="C;M;c++;cc;cpp;cxx;m;mm;CPP";
cmakeVars["CMAKE_CXX_VERBOSE_FLAG"]="-v";
cmakeVars["CMAKE_C_ABI_COMPILED"]="TRUE";
cmakeVars["CMAKE_C_ARCHIVE_APPEND"]="<CMAKE_AR> r  <TARGET> <LINK_FLAGS> <OBJECTS>";
cmakeVars["CMAKE_C_ARCHIVE_CREATE"]="<CMAKE_AR> cr <TARGET> <LINK_FLAGS> <OBJECTS>";
cmakeVars["CMAKE_C_ARCHIVE_FINISH"]="<CMAKE_RANLIB> <TARGET>";
cmakeVars["CMAKE_C_COMPILER"]="/usr/bin/cc";
cmakeVars["CMAKE_C_COMPILER_ABI"]="ELF";
cmakeVars["CMAKE_C_COMPILER_ARG1"]="";
cmakeVars["CMAKE_C_COMPILER_ENV_VAR"]="CC";
cmakeVars["CMAKE_C_COMPILER_ID"]="GNU";
cmakeVars["CMAKE_C_COMPILER_ID_PLATFORM_CONTENT"]="/* Identify known platforms by name.  */\n#if defined(__linux) || defined(__linux__) || defined(linux)\n# define PLATFORM_ID \"Linux\"\n\n#elif defined(__CYGWIN__)\n# define PLATFORM_ID \"Cygwin\"\n\n#elif defined(__MINGW32__)\n# define PLATFORM_ID \"MinGW\"\n\n#elif defined(__APPLE__)\n# define PLATFORM_ID \"Darwin\"\n\n#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)\n# define PLATFORM_ID \"Windows\"\n\n#elif defined(__FreeBSD__) || defined(__FreeBSD)\n# define PLATFORM_ID \"FreeBSD\"\n\n#elif defined(__NetBSD__) || defined(__NetBSD)\n# define PLATFORM_ID \"NetBSD\"\n\n#elif defined(__OpenBSD__) || defined(__OPENBSD)\n# define PLATFORM_ID \"OpenBSD\"\n\n#elif defined(__sun) || defined(sun)\n# define PLATFORM_ID \"SunOS\"\n\n#elif defined(_AIX) || defined(__AIX) || defined(__AIX__) || defined(__aix) || defined(__aix__)\n# define PLATFORM_ID \"AIX\"\n\n#elif defined(__sgi) || defined(__sgi__) || defined(_SGI)\n# define PLATFORM_ID \"IRIX\"\n\n#elif defined(__hpux) || defined(__hpux__)\n# define PLATFORM_ID \"HP-UX\"\n\n#elif defined(__HAIKU__)\n# define PLATFORM_ID \"Haiku\"\n\n#elif defined(__BeOS) || defined(__BEOS__) || defined(_BEOS)\n# define PLATFORM_ID \"BeOS\"\n\n#elif defined(__QNX__) || defined(__QNXNTO__)\n# define PLATFORM_ID \"QNX\"\n\n#elif defined(__tru64) || defined(_tru64) || defined(__TRU64__)\n# define PLATFORM_ID \"Tru64\"\n\n#elif defined(__riscos) || defined(__riscos__)\n# define PLATFORM_ID \"RISCos\"\n\n#elif defined(__sinix) || defined(__sinix__) || defined(__SINIX__)\n# define PLATFORM_ID \"SINIX\"\n\n#elif defined(__UNIX_SV__)\n# define PLATFORM_ID \"UNIX_SV\"\n\n#elif defined(__bsdos__)\n# define PLATFORM_ID \"BSDOS\"\n\n#elif defined(_MPRAS) || defined(MPRAS)\n# define PLATFORM_ID \"MP-RAS\"\n\n#elif defined(__osf) || defined(__osf__)\n# define PLATFORM_ID \"OSF1\"\n\n#elif defined(_SCO_SV) || defined(SCO_SV) || defined(sco_sv)\n# define PLATFORM_ID \"SCO_SV\"\n\n#elif defined(__ultrix) || defined(__ultrix__) || defined(_ULTRIX)\n# define PLATFORM_ID \"ULTRIX\"\n\n#elif defined(__XENIX__) || defined(_XENIX) || defined(XENIX)\n# define PLATFORM_ID \"Xenix\"\n\n#else /* unknown platform */\n# define PLATFORM_ID \"\"\n\n#endif\n\n/* For windows compilers MSVC and Intel we can determine\n   the architecture of the compiler being used.  This is because\n   the compilers do not have flags that can change the architecture,\n   but rather depend on which compiler is being used\n*/\n#if defined(_WIN32) && defined(_MSC_VER)\n# if defined(_M_IA64)\n#  define ARCHITECTURE_ID \"IA64\"\n\n# elif defined(_M_X64) || defined(_M_AMD64)\n#  define ARCHITECTURE_ID \"x64\"\n\n# elif defined(_M_IX86)\n#  define ARCHITECTURE_ID \"X86\"\n\n# elif defined(_M_ARM)\n#  define ARCHITECTURE_ID \"ARM\"\n\n# elif defined(_M_MIPS)\n#  define ARCHITECTURE_ID \"MIPS\"\n\n# elif defined(_M_SH)\n#  define ARCHITECTURE_ID \"SHx\"\n\n# else /* unknown architecture */\n#  define ARCHITECTURE_ID \"\"\n# endif\n\n#else\n#  define ARCHITECTURE_ID \"\"\n#endif\n\n/* Convert integer to decimal digit literals.  */\n#define DEC(n)                   \\\n  ('0' + (((n) / 10000000)%10)), \\\n  ('0' + (((n) / 1000000)%10)),  \\\n  ('0' + (((n) / 100000)%10)),   \\\n  ('0' + (((n) / 10000)%10)),    \\\n  ('0' + (((n) / 1000)%10)),     \\\n  ('0' + (((n) / 100)%10)),      \\\n  ('0' + (((n) / 10)%10)),       \\\n  ('0' +  ((n) % 10))\n\n/* Convert integer to hex digit literals.  */\n#define HEX(n)             \\\n  ('0' + ((n)>>28 & 0xF)), \\\n  ('0' + ((n)>>24 & 0xF)), \\\n  ('0' + ((n)>>20 & 0xF)), \\\n  ('0' + ((n)>>16 & 0xF)), \\\n  ('0' + ((n)>>12 & 0xF)), \\\n  ('0' + ((n)>>8  & 0xF)), \\\n  ('0' + ((n)>>4  & 0xF)), \\\n  ('0' + ((n)     & 0xF))\n\n/* Construct a string literal encoding the version number components. */\n#ifdef COMPILER_VERSION_MAJOR\nchar const info_version[] = {\n  'I', 'N', 'F', 'O', ':',\n  'c','o','m','p','i','l','e','r','_','v','e','r','s','i','o','n','[',\n  COMPILER_VERSION_MAJOR,\n# ifdef COMPILER_VERSION_MINOR\n  '.', COMPILER_VERSION_MINOR,\n#  ifdef COMPILER_VERSION_PATCH\n   '.', COMPILER_VERSION_PATCH,\n#   ifdef COMPILER_VERSION_TWEAK\n    '.', COMPILER_VERSION_TWEAK,\n#   endif\n#  endif\n# endif\n  ']','\\0'};\n#endif\n\n/* Construct the string literal in pieces to prevent the source from\n   getting matched.  Store it in a pointer rather than an array\n   because some compilers will just produce instructions to fill the\n   array rather than assigning a pointer to a static array.  */\nchar const* info_platform = \"INFO\" \":\" \"platform[\" PLATFORM_ID \"]\";\nchar const* info_arch = \"INFO\" \":\" \"arch[\" ARCHITECTURE_ID \"]\";\n\n";
cmakeVars["CMAKE_C_COMPILER_ID_RUN"]="1";
cmakeVars["CMAKE_C_COMPILER_ID_TEST_FLAGS"]="-c;-Aa";
cmakeVars["CMAKE_C_COMPILER_ID_VENDORS"]="IAR";
cmakeVars["CMAKE_C_COMPILER_ID_VENDOR_REGEX_IAR"]="IAR .+ Compiler";
cmakeVars["CMAKE_C_COMPILER_INIT"]="NOTFOUND";
cmakeVars["CMAKE_C_COMPILER_LIST"]="cc;gcc;cl;bcc;xlc;clang";
cmakeVars["CMAKE_C_COMPILER_LOADED"]="1";
cmakeVars["CMAKE_C_COMPILER_NAMES"]="cc";
cmakeVars["CMAKE_C_COMPILER_VERSION"]="4.8.4";
cmakeVars["CMAKE_C_COMPILER_WORKS"]="TRUE";
cmakeVars["CMAKE_C_COMPILE_OBJECT"]="<CMAKE_C_COMPILER> <DEFINES> <FLAGS> -o <OBJECT>   -c <SOURCE>";
cmakeVars["CMAKE_C_COMPILE_OPTIONS_PIC"]="-fPIC";
cmakeVars["CMAKE_C_COMPILE_OPTIONS_PIE"]="-fPIE";
cmakeVars["CMAKE_C_COMPILE_OPTIONS_VISIBILITY"]="-fvisibility=";
cmakeVars["CMAKE_C_CREATE_ASSEMBLY_SOURCE"]="<CMAKE_C_COMPILER> <DEFINES> <FLAGS> -S <SOURCE> -o <ASSEMBLY_SOURCE>";
cmakeVars["CMAKE_C_CREATE_PREPROCESSED_SOURCE"]="<CMAKE_C_COMPILER> <DEFINES> <FLAGS> -E <SOURCE> > <PREPROCESSED_SOURCE>";
cmakeVars["CMAKE_C_CREATE_SHARED_LIBRARY"]="<CMAKE_C_COMPILER> <CMAKE_SHARED_LIBRARY_C_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>";
cmakeVars["CMAKE_C_CREATE_SHARED_MODULE"]="<CMAKE_C_COMPILER> <CMAKE_SHARED_LIBRARY_C_FLAGS> <LANGUAGE_COMPILE_FLAGS> <LINK_FLAGS> <CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS> <SONAME_FLAG><TARGET_SONAME> -o <TARGET> <OBJECTS> <LINK_LIBRARIES>";
cmakeVars["CMAKE_C_FLAGS"]="";
cmakeVars["CMAKE_C_FLAGS_DEBUG"]="-g";
cmakeVars["CMAKE_C_FLAGS_DEBUG_INIT"]="-g";
cmakeVars["CMAKE_C_FLAGS_MINSIZEREL"]="-Os -DNDEBUG";
cmakeVars["CMAKE_C_FLAGS_MINSIZEREL_INIT"]="-Os -DNDEBUG";
cmakeVars["CMAKE_C_FLAGS_RELEASE"]="-O3 -DNDEBUG";
cmakeVars["CMAKE_C_FLAGS_RELEASE_INIT"]="-O3 -DNDEBUG";
cmakeVars["CMAKE_C_FLAGS_RELWITHDEBINFO"]="-O2 -g -DNDEBUG";
cmakeVars["CMAKE_C_FLAGS_RELWITHDEBINFO_INIT"]="-O2 -g -DNDEBUG";
cmakeVars["CMAKE_C_IGNORE_EXTENSIONS"]="h;H;o;O;obj;OBJ;def;DEF;rc;RC";
cmakeVars["CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES"]="/usr/include";
cmakeVars["CMAKE_C_IMPLICIT_LINK_DIRECTORIES"]="/usr/lib/gcc/x86_64-linux-gnu/4.8;/usr/lib/x86_64-linux-gnu;/usr/lib;/lib/x86_64-linux-gnu;/lib";
cmakeVars["CMAKE_C_IMPLICIT_LINK_FRAMEWORK_DIRECTORIES"]="";
cmakeVars["CMAKE_C_IMPLICIT_LINK_LIBRARIES"]="c";
cmakeVars["CMAKE_C_INFORMATION_LOADED"]="1";
cmakeVars["CMAKE_C_LIBRARY_ARCHITECTURE"]="x86_64-linux-gnu";
cmakeVars["CMAKE_C_LINKER_PREFERENCE"]="10";
cmakeVars["CMAKE_C_LINK_EXECUTABLE"]="<CMAKE_C_COMPILER> <FLAGS> <CMAKE_C_LINK_FLAGS> <LINK_FLAGS> <OBJECTS>  -o <TARGET> <LINK_LIBRARIES>";
cmakeVars["CMAKE_C_OUTPUT_EXTENSION"]=".o";
cmakeVars["CMAKE_C_PLATFORM_ID"]="Linux";
cmakeVars["CMAKE_C_SIZEOF_DATA_PTR"]="8";
cmakeVars["CMAKE_C_SOURCE_FILE_EXTENSIONS"]="c";
cmakeVars["CMAKE_C_VERBOSE_FLAG"]="-v";
cmakeVars["CMAKE_DEPFILE_FLAGS_C"]="-MMD -MT <OBJECT> -MF <DEPFILE>";
cmakeVars["CMAKE_DEPFILE_FLAGS_CXX"]="-MMD -MT <OBJECT> -MF <DEPFILE>";
cmakeVars["CMAKE_DL_LIBS"]="dl";
cmakeVars["CMAKE_EDIT_COMMAND"]="/usr/bin/ccmake";
cmakeVars["CMAKE_EXECUTABLE_FORMAT"]="ELF";
cmakeVars["CMAKE_EXECUTABLE_RPATH_LINK_CXX_FLAG"]="-Wl,-rpath-link,";
cmakeVars["CMAKE_EXECUTABLE_RPATH_LINK_C_FLAG"]="-Wl,-rpath-link,";
cmakeVars["CMAKE_EXECUTABLE_RUNTIME_CXX_FLAG"]="-Wl,-rpath,";
cmakeVars["CMAKE_EXECUTABLE_RUNTIME_CXX_FLAG_SEP"]=":";
cmakeVars["CMAKE_EXECUTABLE_RUNTIME_C_FLAG"]="-Wl,-rpath,";
cmakeVars["CMAKE_EXECUTABLE_RUNTIME_C_FLAG_SEP"]=":";
cmakeVars["CMAKE_EXECUTABLE_SUFFIX"]="";
cmakeVars["CMAKE_EXE_EXPORTS_CXX_FLAG"]="-Wl,--export-dynamic";
cmakeVars["CMAKE_EXE_EXPORTS_C_FLAG"]="-Wl,--export-dynamic";
cmakeVars["CMAKE_EXE_LINKER_FLAGS"]=" ";
cmakeVars["CMAKE_EXE_LINKER_FLAGS_DEBUG"]="";
cmakeVars["CMAKE_EXE_LINKER_FLAGS_MINSIZEREL"]="";
cmakeVars["CMAKE_EXE_LINKER_FLAGS_RELEASE"]="";
cmakeVars["CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO"]="";
cmakeVars["CMAKE_EXE_LINK_DYNAMIC_CXX_FLAGS"]="-Wl,-Bdynamic";
cmakeVars["CMAKE_EXE_LINK_DYNAMIC_C_FLAGS"]="-Wl,-Bdynamic";
cmakeVars["CMAKE_EXE_LINK_STATIC_CXX_FLAGS"]="-Wl,-Bstatic";
cmakeVars["CMAKE_EXE_LINK_STATIC_C_FLAGS"]="-Wl,-Bstatic";
cmakeVars["CMAKE_EXPORT_COMPILE_COMMANDS"]="OFF";
cmakeVars["CMAKE_EXTRA_GENERATOR"]="CodeBlocks";
cmakeVars["CMAKE_EXTRA_GENERATOR_CXX_SYSTEM_DEFINED_MACROS"]="__STDC__;1;__STDC_HOSTED__;1;__GNUC__;4;__GNUC_MINOR__;8;__GNUC_PATCHLEVEL__;4;__VERSION__;\"4.8.4\";__ATOMIC_RELAXED; ;__ATOMIC_SEQ_CST;5;__ATOMIC_ACQUIRE;2;__ATOMIC_RELEASE;3;__ATOMIC_ACQ_REL;4;__ATOMIC_CONSUME;1;__FINITE_MATH_ONLY__; ;_LP64;1;__LP64__;1;__SIZEOF_INT__;4;__SIZEOF_LONG__;8;__SIZEOF_LONG_LONG__;8;__SIZEOF_SHORT__;2;__SIZEOF_FLOAT__;4;__SIZEOF_DOUBLE__;8;__SIZEOF_LONG_DOUBLE__;16;__SIZEOF_SIZE_T__;8;__CHAR_BIT__;8;__BIGGEST_ALIGNMENT__;16;__ORDER_LITTLE_ENDIAN__;1234;__ORDER_BIG_ENDIAN__;4321;__ORDER_PDP_ENDIAN__;3412;__BYTE_ORDER__;__ORDER_LITTLE_ENDIAN__;__FLOAT_WORD_ORDER__;__ORDER_LITTLE_ENDIAN__;__SIZEOF_POINTER__;8;__SIZE_TYPE__;long unsigned int;__PTRDIFF_TYPE__;long int;__WCHAR_TYPE__;int;__WINT_TYPE__;unsigned int;__INTMAX_TYPE__;long int;__UINTMAX_TYPE__;long unsigned int;__CHAR16_TYPE__;short unsigned int;__CHAR32_TYPE__;unsigned int;__SIG_ATOMIC_TYPE__;int;__INT8_TYPE__;signed char;__INT16_TYPE__;short int;__INT32_TYPE__;int;__INT64_TYPE__;long int;__UINT8_TYPE__;unsigned char;__UINT16_TYPE__;short unsigned int;__UINT32_TYPE__;unsigned int;__UINT64_TYPE__;long unsigned int;__INT_LEAST8_TYPE__;signed char;__INT_LEAST16_TYPE__;short int;__INT_LEAST32_TYPE__;int;__INT_LEAST64_TYPE__;long int;__UINT_LEAST8_TYPE__;unsigned char;__UINT_LEAST16_TYPE__;short unsigned int;__UINT_LEAST32_TYPE__;unsigned int;__UINT_LEAST64_TYPE__;long unsigned int;__INT_FAST8_TYPE__;signed char;__INT_FAST16_TYPE__;long int;__INT_FAST32_TYPE__;long int;__INT_FAST64_TYPE__;long int;__UINT_FAST8_TYPE__;unsigned char;__UINT_FAST16_TYPE__;long unsigned int;__UINT_FAST32_TYPE__;long unsigned int;__UINT_FAST64_TYPE__;long unsigned int;__INTPTR_TYPE__;long int;__UINTPTR_TYPE__;long unsigned int;__GXX_ABI_VERSION;1002;__SCHAR_MAX__;127;__SHRT_MAX__;32767;__INT_MAX__;2147483647;__LONG_MAX__;9223372036854775807L;__LONG_LONG_MAX__;9223372036854775807LL;__WCHAR_MAX__;2147483647;__WCHAR_MIN__;(-__WCHAR_MAX__ - 1);__WINT_MAX__;4294967295U;__WINT_MIN__;0U;__PTRDIFF_MAX__;9223372036854775807L;__SIZE_MAX__;18446744073709551615UL;__INTMAX_MAX__;9223372036854775807L;__INTMAX_C(c);c ## L;__UINTMAX_MAX__;18446744073709551615UL;__UINTMAX_C(c);c ## UL;__SIG_ATOMIC_MAX__;2147483647;__SIG_ATOMIC_MIN__;(-__SIG_ATOMIC_MAX__ - 1);__INT8_MAX__;127;__INT16_MAX__;32767;__INT32_MAX__;2147483647;__INT64_MAX__;9223372036854775807L;__UINT8_MAX__;255;__UINT16_MAX__;65535;__UINT32_MAX__;4294967295U;__UINT64_MAX__;18446744073709551615UL;__INT_LEAST8_MAX__;127;__INT8_C(c);c;__INT_LEAST16_MAX__;32767;__INT16_C(c);c;__INT_LEAST32_MAX__;2147483647;__INT32_C(c);c;__INT_LEAST64_MAX__;9223372036854775807L;__INT64_C(c);c ## L;__UINT_LEAST8_MAX__;255;__UINT8_C(c);c;__UINT_LEAST16_MAX__;65535;__UINT16_C(c);c;__UINT_LEAST32_MAX__;4294967295U;__UINT32_C(c);c ## U;__UINT_LEAST64_MAX__;18446744073709551615UL;__UINT64_C(c);c ## UL;__INT_FAST8_MAX__;127;__INT_FAST16_MAX__;9223372036854775807L;__INT_FAST32_MAX__;9223372036854775807L;__INT_FAST64_MAX__;9223372036854775807L;__UINT_FAST8_MAX__;255;__UINT_FAST16_MAX__;18446744073709551615UL;__UINT_FAST32_MAX__;18446744073709551615UL;__UINT_FAST64_MAX__;18446744073709551615UL;__INTPTR_MAX__;9223372036854775807L;__UINTPTR_MAX__;18446744073709551615UL;__FLT_EVAL_METHOD__; ;__DEC_EVAL_METHOD__;2;__FLT_RADIX__;2;__FLT_MANT_DIG__;24;__FLT_DIG__;6;__FLT_MIN_EXP__;(-125);__FLT_MIN_10_EXP__;(-37);__FLT_MAX_EXP__;128;__FLT_MAX_10_EXP__;38;__FLT_DECIMAL_DIG__;9;__FLT_MAX__;3.40282346638528859812e+38F;__FLT_MIN__;1.17549435082228750797e-38F;__FLT_EPSILON__;1.19209289550781250000e-7F;__FLT_DENORM_MIN__;1.40129846432481707092e-45F;__FLT_HAS_DENORM__;1;__FLT_HAS_INFINITY__;1;__FLT_HAS_QUIET_NAN__;1;__DBL_MANT_DIG__;53;__DBL_DIG__;15;__DBL_MIN_EXP__;(-1021);__DBL_MIN_10_EXP__;(-307);__DBL_MAX_EXP__;1024;__DBL_MAX_10_EXP__;308;__DBL_DECIMAL_DIG__;17;__DBL_MAX__;((double)1.79769313486231570815e+308L);__DBL_MIN__;((double)2.22507385850720138309e-308L);__DBL_EPSILON__;((double)2.22044604925031308085e-16L);__DBL_DENORM_MIN__;((double)4.94065645841246544177e-324L);__DBL_HAS_DENORM__;1;__DBL_HAS_INFINITY__;1;__DBL_HAS_QUIET_NAN__;1;__LDBL_MANT_DIG__;64;__LDBL_DIG__;18;__LDBL_MIN_EXP__;(-16381);__LDBL_MIN_10_EXP__;(-4931);__LDBL_MAX_EXP__;16384;__LDBL_MAX_10_EXP__;4932;__DECIMAL_DIG__;21;__LDBL_MAX__;1.18973149535723176502e+4932L;__LDBL_MIN__;3.36210314311209350626e-4932L;__LDBL_EPSILON__;1.08420217248550443401e-19L;__LDBL_DENORM_MIN__;3.64519953188247460253e-4951L;__LDBL_HAS_DENORM__;1;__LDBL_HAS_INFINITY__;1;__LDBL_HAS_QUIET_NAN__;1;__DEC32_MANT_DIG__;7;__DEC32_MIN_EXP__;(-94);__DEC32_MAX_EXP__;97;__DEC32_MIN__;1E-95DF;__DEC32_MAX__;9.999999E96DF;__DEC32_EPSILON__;1E-6DF;__DEC32_SUBNORMAL_MIN__;0.000001E-95DF;__DEC64_MANT_DIG__;16;__DEC64_MIN_EXP__;(-382);__DEC64_MAX_EXP__;385;__DEC64_MIN__;1E-383DD;__DEC64_MAX__;9.999999999999999E384DD;__DEC64_EPSILON__;1E-15DD;__DEC64_SUBNORMAL_MIN__;0.000000000000001E-383DD;__DEC128_MANT_DIG__;34;__DEC128_MIN_EXP__;(-6142);__DEC128_MAX_EXP__;6145;__DEC128_MIN__;1E-6143DL;__DEC128_MAX__;9.999999999999999999999999999999999E6144DL;__DEC128_EPSILON__;1E-33DL;__DEC128_SUBNORMAL_MIN__;0.000000000000000000000000000000001E-6143DL;__REGISTER_PREFIX__; ;__USER_LABEL_PREFIX__; ;__GNUC_GNU_INLINE__;1;__NO_INLINE__;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8;1;__GCC_ATOMIC_BOOL_LOCK_FREE;2;__GCC_ATOMIC_CHAR_LOCK_FREE;2;__GCC_ATOMIC_CHAR16_T_LOCK_FREE;2;__GCC_ATOMIC_CHAR32_T_LOCK_FREE;2;__GCC_ATOMIC_WCHAR_T_LOCK_FREE;2;__GCC_ATOMIC_SHORT_LOCK_FREE;2;__GCC_ATOMIC_INT_LOCK_FREE;2;__GCC_ATOMIC_LONG_LOCK_FREE;2;__GCC_ATOMIC_LLONG_LOCK_FREE;2;__GCC_ATOMIC_TEST_AND_SET_TRUEVAL;1;__GCC_ATOMIC_POINTER_LOCK_FREE;2;__GCC_HAVE_DWARF2_CFI_ASM;1;__PRAGMA_REDEFINE_EXTNAME;1;__SSP__;1;__SIZEOF_INT128__;16;__SIZEOF_WCHAR_T__;4;__SIZEOF_WINT_T__;4;__SIZEOF_PTRDIFF_T__;8;__amd64;1;__amd64__;1;__x86_64;1;__x86_64__;1;__ATOMIC_HLE_ACQUIRE;65536;__ATOMIC_HLE_RELEASE;131072;__k8;1;__k8__;1;__code_model_small__;1;__MMX__;1;__SSE__;1;__SSE2__;1;__FXSR__;1;__SSE_MATH__;1;__SSE2_MATH__;1;__gnu_linux__;1;__linux;1;__linux__;1;linux;1;__unix;1;__unix__;1;unix;1;__ELF__;1;__DECIMAL_BID_FORMAT__;1;_STDC_PREDEF_H;1;__STDC_IEC_559__;1;__STDC_IEC_559_COMPLEX__;1;__STDC_ISO_10646__;201103L;__STDC_NO_THREADS__;1;__STDC__;1;__cplusplus;199711L;__STDC_HOSTED__;1;__GNUC__;4;__GNUC_MINOR__;8;__GNUC_PATCHLEVEL__;4;__VERSION__;\"4.8.4\";__ATOMIC_RELAXED; ;__ATOMIC_SEQ_CST;5;__ATOMIC_ACQUIRE;2;__ATOMIC_RELEASE;3;__ATOMIC_ACQ_REL;4;__ATOMIC_CONSUME;1;__FINITE_MATH_ONLY__; ;_LP64;1;__LP64__;1;__SIZEOF_INT__;4;__SIZEOF_LONG__;8;__SIZEOF_LONG_LONG__;8;__SIZEOF_SHORT__;2;__SIZEOF_FLOAT__;4;__SIZEOF_DOUBLE__;8;__SIZEOF_LONG_DOUBLE__;16;__SIZEOF_SIZE_T__;8;__CHAR_BIT__;8;__BIGGEST_ALIGNMENT__;16;__ORDER_LITTLE_ENDIAN__;1234;__ORDER_BIG_ENDIAN__;4321;__ORDER_PDP_ENDIAN__;3412;__BYTE_ORDER__;__ORDER_LITTLE_ENDIAN__;__FLOAT_WORD_ORDER__;__ORDER_LITTLE_ENDIAN__;__SIZEOF_POINTER__;8;__GNUG__;4;__SIZE_TYPE__;long unsigned int;__PTRDIFF_TYPE__;long int;__WCHAR_TYPE__;int;__WINT_TYPE__;unsigned int;__INTMAX_TYPE__;long int;__UINTMAX_TYPE__;long unsigned int;__CHAR16_TYPE__;short unsigned int;__CHAR32_TYPE__;unsigned int;__SIG_ATOMIC_TYPE__;int;__INT8_TYPE__;signed char;__INT16_TYPE__;short int;__INT32_TYPE__;int;__INT64_TYPE__;long int;__UINT8_TYPE__;unsigned char;__UINT16_TYPE__;short unsigned int;__UINT32_TYPE__;unsigned int;__UINT64_TYPE__;long unsigned int;__INT_LEAST8_TYPE__;signed char;__INT_LEAST16_TYPE__;short int;__INT_LEAST32_TYPE__;int;__INT_LEAST64_TYPE__;long int;__UINT_LEAST8_TYPE__;unsigned char;__UINT_LEAST16_TYPE__;short unsigned int;__UINT_LEAST32_TYPE__;unsigned int;__UINT_LEAST64_TYPE__;long unsigned int;__INT_FAST8_TYPE__;signed char;__INT_FAST16_TYPE__;long int;__INT_FAST32_TYPE__;long int;__INT_FAST64_TYPE__;long int;__UINT_FAST8_TYPE__;unsigned char;__UINT_FAST16_TYPE__;long unsigned int;__UINT_FAST32_TYPE__;long unsigned int;__UINT_FAST64_TYPE__;long unsigned int;__INTPTR_TYPE__;long int;__UINTPTR_TYPE__;long unsigned int;__GXX_WEAK__;1;__DEPRECATED;1;__GXX_RTTI;1;__EXCEPTIONS;1;__GXX_ABI_VERSION;1002;__SCHAR_MAX__;127;__SHRT_MAX__;32767;__INT_MAX__;2147483647;__LONG_MAX__;9223372036854775807L;__LONG_LONG_MAX__;9223372036854775807LL;__WCHAR_MAX__;2147483647;__WCHAR_MIN__;(-__WCHAR_MAX__ - 1);__WINT_MAX__;4294967295U;__WINT_MIN__;0U;__PTRDIFF_MAX__;9223372036854775807L;__SIZE_MAX__;18446744073709551615UL;__INTMAX_MAX__;9223372036854775807L;__INTMAX_C(c);c ## L;__UINTMAX_MAX__;18446744073709551615UL;__UINTMAX_C(c);c ## UL;__SIG_ATOMIC_MAX__;2147483647;__SIG_ATOMIC_MIN__;(-__SIG_ATOMIC_MAX__ - 1);__INT8_MAX__;127;__INT16_MAX__;32767;__INT32_MAX__;2147483647;__INT64_MAX__;9223372036854775807L;__UINT8_MAX__;255;__UINT16_MAX__;65535;__UINT32_MAX__;4294967295U;__UINT64_MAX__;18446744073709551615UL;__INT_LEAST8_MAX__;127;__INT8_C(c);c;__INT_LEAST16_MAX__;32767;__INT16_C(c);c;__INT_LEAST32_MAX__;2147483647;__INT32_C(c);c;__INT_LEAST64_MAX__;9223372036854775807L;__INT64_C(c);c ## L;__UINT_LEAST8_MAX__;255;__UINT8_C(c);c;__UINT_LEAST16_MAX__;65535;__UINT16_C(c);c;__UINT_LEAST32_MAX__;4294967295U;__UINT32_C(c);c ## U;__UINT_LEAST64_MAX__;18446744073709551615UL;__UINT64_C(c);c ## UL;__INT_FAST8_MAX__;127;__INT_FAST16_MAX__;9223372036854775807L;__INT_FAST32_MAX__;9223372036854775807L;__INT_FAST64_MAX__;9223372036854775807L;__UINT_FAST8_MAX__;255;__UINT_FAST16_MAX__;18446744073709551615UL;__UINT_FAST32_MAX__;18446744073709551615UL;__UINT_FAST64_MAX__;18446744073709551615UL;__INTPTR_MAX__;9223372036854775807L;__UINTPTR_MAX__;18446744073709551615UL;__FLT_EVAL_METHOD__; ;__DEC_EVAL_METHOD__;2;__FLT_RADIX__;2;__FLT_MANT_DIG__;24;__FLT_DIG__;6;__FLT_MIN_EXP__;(-125);__FLT_MIN_10_EXP__;(-37);__FLT_MAX_EXP__;128;__FLT_MAX_10_EXP__;38;__FLT_DECIMAL_DIG__;9;__FLT_MAX__;3.40282346638528859812e+38F;__FLT_MIN__;1.17549435082228750797e-38F;__FLT_EPSILON__;1.19209289550781250000e-7F;__FLT_DENORM_MIN__;1.40129846432481707092e-45F;__FLT_HAS_DENORM__;1;__FLT_HAS_INFINITY__;1;__FLT_HAS_QUIET_NAN__;1;__DBL_MANT_DIG__;53;__DBL_DIG__;15;__DBL_MIN_EXP__;(-1021);__DBL_MIN_10_EXP__;(-307);__DBL_MAX_EXP__;1024;__DBL_MAX_10_EXP__;308;__DBL_DECIMAL_DIG__;17;__DBL_MAX__;double(1.79769313486231570815e+308L);__DBL_MIN__;double(2.22507385850720138309e-308L);__DBL_EPSILON__;double(2.22044604925031308085e-16L);__DBL_DENORM_MIN__;double(4.94065645841246544177e-324L);__DBL_HAS_DENORM__;1;__DBL_HAS_INFINITY__;1;__DBL_HAS_QUIET_NAN__;1;__LDBL_MANT_DIG__;64;__LDBL_DIG__;18;__LDBL_MIN_EXP__;(-16381);__LDBL_MIN_10_EXP__;(-4931);__LDBL_MAX_EXP__;16384;__LDBL_MAX_10_EXP__;4932;__DECIMAL_DIG__;21;__LDBL_MAX__;1.18973149535723176502e+4932L;__LDBL_MIN__;3.36210314311209350626e-4932L;__LDBL_EPSILON__;1.08420217248550443401e-19L;__LDBL_DENORM_MIN__;3.64519953188247460253e-4951L;__LDBL_HAS_DENORM__;1;__LDBL_HAS_INFINITY__;1;__LDBL_HAS_QUIET_NAN__;1;__DEC32_MANT_DIG__;7;__DEC32_MIN_EXP__;(-94);__DEC32_MAX_EXP__;97;__DEC32_MIN__;1E-95DF;__DEC32_MAX__;9.999999E96DF;__DEC32_EPSILON__;1E-6DF;__DEC32_SUBNORMAL_MIN__;0.000001E-95DF;__DEC64_MANT_DIG__;16;__DEC64_MIN_EXP__;(-382);__DEC64_MAX_EXP__;385;__DEC64_MIN__;1E-383DD;__DEC64_MAX__;9.999999999999999E384DD;__DEC64_EPSILON__;1E-15DD;__DEC64_SUBNORMAL_MIN__;0.000000000000001E-383DD;__DEC128_MANT_DIG__;34;__DEC128_MIN_EXP__;(-6142);__DEC128_MAX_EXP__;6145;__DEC128_MIN__;1E-6143DL;__DEC128_MAX__;9.999999999999999999999999999999999E6144DL;__DEC128_EPSILON__;1E-33DL;__DEC128_SUBNORMAL_MIN__;0.000000000000000000000000000000001E-6143DL;__REGISTER_PREFIX__; ;__USER_LABEL_PREFIX__; ;__GNUC_GNU_INLINE__;1;__NO_INLINE__;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8;1;__GCC_ATOMIC_BOOL_LOCK_FREE;2;__GCC_ATOMIC_CHAR_LOCK_FREE;2;__GCC_ATOMIC_CHAR16_T_LOCK_FREE;2;__GCC_ATOMIC_CHAR32_T_LOCK_FREE;2;__GCC_ATOMIC_WCHAR_T_LOCK_FREE;2;__GCC_ATOMIC_SHORT_LOCK_FREE;2;__GCC_ATOMIC_INT_LOCK_FREE;2;__GCC_ATOMIC_LONG_LOCK_FREE;2;__GCC_ATOMIC_LLONG_LOCK_FREE;2;__GCC_ATOMIC_TEST_AND_SET_TRUEVAL;1;__GCC_ATOMIC_POINTER_LOCK_FREE;2;__GCC_HAVE_DWARF2_CFI_ASM;1;__PRAGMA_REDEFINE_EXTNAME;1;__SSP__;1;__SIZEOF_INT128__;16;__SIZEOF_WCHAR_T__;4;__SIZEOF_WINT_T__;4;__SIZEOF_PTRDIFF_T__;8;__amd64;1;__amd64__;1;__x86_64;1;__x86_64__;1;__ATOMIC_HLE_ACQUIRE;65536;__ATOMIC_HLE_RELEASE;131072;__k8;1;__k8__;1;__code_model_small__;1;__MMX__;1;__SSE__;1;__SSE2__;1;__FXSR__;1;__SSE_MATH__;1;__SSE2_MATH__;1;__gnu_linux__;1;__linux;1;__linux__;1;linux;1;__unix;1;__unix__;1;unix;1;__ELF__;1;__DECIMAL_BID_FORMAT__;1;_GNU_SOURCE;1;_STDC_PREDEF_H;1;__STDC_IEC_559__;1;__STDC_IEC_559_COMPLEX__;1;__STDC_ISO_10646__;201103L;__STDC_NO_THREADS__;1";
cmakeVars["CMAKE_EXTRA_GENERATOR_CXX_SYSTEM_INCLUDE_DIRS"]="/usr/include/c++/4.8;/usr/include/x86_64-linux-gnu/c++/4.8;/usr/include/c++/4.8/backward;/usr/lib/gcc/x86_64-linux-gnu/4.8/include;/usr/local/include;/usr/lib/gcc/x86_64-linux-gnu/4.8/include-fixed;/usr/include/x86_64-linux-gnu;/usr/include";
cmakeVars["CMAKE_EXTRA_GENERATOR_C_SYSTEM_DEFINED_MACROS"]="__STDC__;1;__STDC_HOSTED__;1;__GNUC__;4;__GNUC_MINOR__;8;__GNUC_PATCHLEVEL__;4;__VERSION__;\"4.8.4\";__ATOMIC_RELAXED; ;__ATOMIC_SEQ_CST;5;__ATOMIC_ACQUIRE;2;__ATOMIC_RELEASE;3;__ATOMIC_ACQ_REL;4;__ATOMIC_CONSUME;1;__FINITE_MATH_ONLY__; ;_LP64;1;__LP64__;1;__SIZEOF_INT__;4;__SIZEOF_LONG__;8;__SIZEOF_LONG_LONG__;8;__SIZEOF_SHORT__;2;__SIZEOF_FLOAT__;4;__SIZEOF_DOUBLE__;8;__SIZEOF_LONG_DOUBLE__;16;__SIZEOF_SIZE_T__;8;__CHAR_BIT__;8;__BIGGEST_ALIGNMENT__;16;__ORDER_LITTLE_ENDIAN__;1234;__ORDER_BIG_ENDIAN__;4321;__ORDER_PDP_ENDIAN__;3412;__BYTE_ORDER__;__ORDER_LITTLE_ENDIAN__;__FLOAT_WORD_ORDER__;__ORDER_LITTLE_ENDIAN__;__SIZEOF_POINTER__;8;__SIZE_TYPE__;long unsigned int;__PTRDIFF_TYPE__;long int;__WCHAR_TYPE__;int;__WINT_TYPE__;unsigned int;__INTMAX_TYPE__;long int;__UINTMAX_TYPE__;long unsigned int;__CHAR16_TYPE__;short unsigned int;__CHAR32_TYPE__;unsigned int;__SIG_ATOMIC_TYPE__;int;__INT8_TYPE__;signed char;__INT16_TYPE__;short int;__INT32_TYPE__;int;__INT64_TYPE__;long int;__UINT8_TYPE__;unsigned char;__UINT16_TYPE__;short unsigned int;__UINT32_TYPE__;unsigned int;__UINT64_TYPE__;long unsigned int;__INT_LEAST8_TYPE__;signed char;__INT_LEAST16_TYPE__;short int;__INT_LEAST32_TYPE__;int;__INT_LEAST64_TYPE__;long int;__UINT_LEAST8_TYPE__;unsigned char;__UINT_LEAST16_TYPE__;short unsigned int;__UINT_LEAST32_TYPE__;unsigned int;__UINT_LEAST64_TYPE__;long unsigned int;__INT_FAST8_TYPE__;signed char;__INT_FAST16_TYPE__;long int;__INT_FAST32_TYPE__;long int;__INT_FAST64_TYPE__;long int;__UINT_FAST8_TYPE__;unsigned char;__UINT_FAST16_TYPE__;long unsigned int;__UINT_FAST32_TYPE__;long unsigned int;__UINT_FAST64_TYPE__;long unsigned int;__INTPTR_TYPE__;long int;__UINTPTR_TYPE__;long unsigned int;__GXX_ABI_VERSION;1002;__SCHAR_MAX__;127;__SHRT_MAX__;32767;__INT_MAX__;2147483647;__LONG_MAX__;9223372036854775807L;__LONG_LONG_MAX__;9223372036854775807LL;__WCHAR_MAX__;2147483647;__WCHAR_MIN__;(-__WCHAR_MAX__ - 1);__WINT_MAX__;4294967295U;__WINT_MIN__;0U;__PTRDIFF_MAX__;9223372036854775807L;__SIZE_MAX__;18446744073709551615UL;__INTMAX_MAX__;9223372036854775807L;__INTMAX_C(c);c ## L;__UINTMAX_MAX__;18446744073709551615UL;__UINTMAX_C(c);c ## UL;__SIG_ATOMIC_MAX__;2147483647;__SIG_ATOMIC_MIN__;(-__SIG_ATOMIC_MAX__ - 1);__INT8_MAX__;127;__INT16_MAX__;32767;__INT32_MAX__;2147483647;__INT64_MAX__;9223372036854775807L;__UINT8_MAX__;255;__UINT16_MAX__;65535;__UINT32_MAX__;4294967295U;__UINT64_MAX__;18446744073709551615UL;__INT_LEAST8_MAX__;127;__INT8_C(c);c;__INT_LEAST16_MAX__;32767;__INT16_C(c);c;__INT_LEAST32_MAX__;2147483647;__INT32_C(c);c;__INT_LEAST64_MAX__;9223372036854775807L;__INT64_C(c);c ## L;__UINT_LEAST8_MAX__;255;__UINT8_C(c);c;__UINT_LEAST16_MAX__;65535;__UINT16_C(c);c;__UINT_LEAST32_MAX__;4294967295U;__UINT32_C(c);c ## U;__UINT_LEAST64_MAX__;18446744073709551615UL;__UINT64_C(c);c ## UL;__INT_FAST8_MAX__;127;__INT_FAST16_MAX__;9223372036854775807L;__INT_FAST32_MAX__;9223372036854775807L;__INT_FAST64_MAX__;9223372036854775807L;__UINT_FAST8_MAX__;255;__UINT_FAST16_MAX__;18446744073709551615UL;__UINT_FAST32_MAX__;18446744073709551615UL;__UINT_FAST64_MAX__;18446744073709551615UL;__INTPTR_MAX__;9223372036854775807L;__UINTPTR_MAX__;18446744073709551615UL;__FLT_EVAL_METHOD__; ;__DEC_EVAL_METHOD__;2;__FLT_RADIX__;2;__FLT_MANT_DIG__;24;__FLT_DIG__;6;__FLT_MIN_EXP__;(-125);__FLT_MIN_10_EXP__;(-37);__FLT_MAX_EXP__;128;__FLT_MAX_10_EXP__;38;__FLT_DECIMAL_DIG__;9;__FLT_MAX__;3.40282346638528859812e+38F;__FLT_MIN__;1.17549435082228750797e-38F;__FLT_EPSILON__;1.19209289550781250000e-7F;__FLT_DENORM_MIN__;1.40129846432481707092e-45F;__FLT_HAS_DENORM__;1;__FLT_HAS_INFINITY__;1;__FLT_HAS_QUIET_NAN__;1;__DBL_MANT_DIG__;53;__DBL_DIG__;15;__DBL_MIN_EXP__;(-1021);__DBL_MIN_10_EXP__;(-307);__DBL_MAX_EXP__;1024;__DBL_MAX_10_EXP__;308;__DBL_DECIMAL_DIG__;17;__DBL_MAX__;((double)1.79769313486231570815e+308L);__DBL_MIN__;((double)2.22507385850720138309e-308L);__DBL_EPSILON__;((double)2.22044604925031308085e-16L);__DBL_DENORM_MIN__;((double)4.94065645841246544177e-324L);__DBL_HAS_DENORM__;1;__DBL_HAS_INFINITY__;1;__DBL_HAS_QUIET_NAN__;1;__LDBL_MANT_DIG__;64;__LDBL_DIG__;18;__LDBL_MIN_EXP__;(-16381);__LDBL_MIN_10_EXP__;(-4931);__LDBL_MAX_EXP__;16384;__LDBL_MAX_10_EXP__;4932;__DECIMAL_DIG__;21;__LDBL_MAX__;1.18973149535723176502e+4932L;__LDBL_MIN__;3.36210314311209350626e-4932L;__LDBL_EPSILON__;1.08420217248550443401e-19L;__LDBL_DENORM_MIN__;3.64519953188247460253e-4951L;__LDBL_HAS_DENORM__;1;__LDBL_HAS_INFINITY__;1;__LDBL_HAS_QUIET_NAN__;1;__DEC32_MANT_DIG__;7;__DEC32_MIN_EXP__;(-94);__DEC32_MAX_EXP__;97;__DEC32_MIN__;1E-95DF;__DEC32_MAX__;9.999999E96DF;__DEC32_EPSILON__;1E-6DF;__DEC32_SUBNORMAL_MIN__;0.000001E-95DF;__DEC64_MANT_DIG__;16;__DEC64_MIN_EXP__;(-382);__DEC64_MAX_EXP__;385;__DEC64_MIN__;1E-383DD;__DEC64_MAX__;9.999999999999999E384DD;__DEC64_EPSILON__;1E-15DD;__DEC64_SUBNORMAL_MIN__;0.000000000000001E-383DD;__DEC128_MANT_DIG__;34;__DEC128_MIN_EXP__;(-6142);__DEC128_MAX_EXP__;6145;__DEC128_MIN__;1E-6143DL;__DEC128_MAX__;9.999999999999999999999999999999999E6144DL;__DEC128_EPSILON__;1E-33DL;__DEC128_SUBNORMAL_MIN__;0.000000000000000000000000000000001E-6143DL;__REGISTER_PREFIX__; ;__USER_LABEL_PREFIX__; ;__GNUC_GNU_INLINE__;1;__NO_INLINE__;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8;1;__GCC_ATOMIC_BOOL_LOCK_FREE;2;__GCC_ATOMIC_CHAR_LOCK_FREE;2;__GCC_ATOMIC_CHAR16_T_LOCK_FREE;2;__GCC_ATOMIC_CHAR32_T_LOCK_FREE;2;__GCC_ATOMIC_WCHAR_T_LOCK_FREE;2;__GCC_ATOMIC_SHORT_LOCK_FREE;2;__GCC_ATOMIC_INT_LOCK_FREE;2;__GCC_ATOMIC_LONG_LOCK_FREE;2;__GCC_ATOMIC_LLONG_LOCK_FREE;2;__GCC_ATOMIC_TEST_AND_SET_TRUEVAL;1;__GCC_ATOMIC_POINTER_LOCK_FREE;2;__GCC_HAVE_DWARF2_CFI_ASM;1;__PRAGMA_REDEFINE_EXTNAME;1;__SSP__;1;__SIZEOF_INT128__;16;__SIZEOF_WCHAR_T__;4;__SIZEOF_WINT_T__;4;__SIZEOF_PTRDIFF_T__;8;__amd64;1;__amd64__;1;__x86_64;1;__x86_64__;1;__ATOMIC_HLE_ACQUIRE;65536;__ATOMIC_HLE_RELEASE;131072;__k8;1;__k8__;1;__code_model_small__;1;__MMX__;1;__SSE__;1;__SSE2__;1;__FXSR__;1;__SSE_MATH__;1;__SSE2_MATH__;1;__gnu_linux__;1;__linux;1;__linux__;1;linux;1;__unix;1;__unix__;1;unix;1;__ELF__;1;__DECIMAL_BID_FORMAT__;1;_STDC_PREDEF_H;1;__STDC_IEC_559__;1;__STDC_IEC_559_COMPLEX__;1;__STDC_ISO_10646__;201103L;__STDC_NO_THREADS__;1";
cmakeVars["CMAKE_EXTRA_GENERATOR_C_SYSTEM_INCLUDE_DIRS"]="/usr/lib/gcc/x86_64-linux-gnu/4.8/include;/usr/local/include;/usr/lib/gcc/x86_64-linux-gnu/4.8/include-fixed;/usr/include/x86_64-linux-gnu;/usr/include";
cmakeVars["CMAKE_FILES_DIRECTORY"]="/CMakeFiles";
cmakeVars["CMAKE_FIND_LIBRARY_PREFIXES"]="lib";
cmakeVars["CMAKE_FIND_LIBRARY_SUFFIXES"]=".so;.a";
cmakeVars["CMAKE_GENERATOR"]="Unix Makefiles";
cmakeVars["CMAKE_GENERATOR_TOOLSET"]="";
cmakeVars["CMAKE_HOME_DIRECTORY"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["CMAKE_HOST_PATH"]="/usr/bin";
cmakeVars["CMAKE_HOST_SYSTEM"]="Linux-3.13.0-71-generic";
cmakeVars["CMAKE_HOST_SYSTEM_NAME"]="Linux";
cmakeVars["CMAKE_HOST_SYSTEM_PROCESSOR"]="x86_64";
cmakeVars["CMAKE_HOST_SYSTEM_VERSION"]="3.13.0-71-generic";
cmakeVars["CMAKE_HOST_UNIX"]="1";
cmakeVars["CMAKE_INCLUDE_FLAG_C"]="-I";
cmakeVars["CMAKE_INCLUDE_FLAG_CXX"]="-I";
cmakeVars["CMAKE_INCLUDE_FLAG_C_SEP"]="";
cmakeVars["CMAKE_INCLUDE_SYSTEM_FLAG_C"]="-isystem ";
cmakeVars["CMAKE_INCLUDE_SYSTEM_FLAG_CXX"]="-isystem ";
cmakeVars["CMAKE_INSTALL_DEFAULT_COMPONENT_NAME"]="Unspecified";
cmakeVars["CMAKE_INSTALL_PREFIX"]="/usr/local";
cmakeVars["CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT"]="1";
cmakeVars["CMAKE_INSTALL_SO_NO_EXE"]="1";
cmakeVars["CMAKE_INTERNAL_PLATFORM_ABI"]="ELF";
cmakeVars["CMAKE_LIBRARY_ARCHITECTURE"]="x86_64-linux-gnu";
cmakeVars["CMAKE_LIBRARY_ARCHITECTURE_REGEX"]="[a-z0-9_]+(-[a-z0-9_]+)?-linux-gnu[a-z0-9_]*";
cmakeVars["CMAKE_LIBRARY_PATH_FLAG"]="-L";
cmakeVars["CMAKE_LIBRARY_PATH_TERMINATOR"]="";
cmakeVars["CMAKE_LINKER"]="/usr/bin/ld";
cmakeVars["CMAKE_LINK_LIBRARY_FLAG"]="-l";
cmakeVars["CMAKE_LINK_LIBRARY_SUFFIX"]="";
cmakeVars["CMAKE_MAJOR_VERSION"]="2";
cmakeVars["CMAKE_MAKE_PROGRAM"]="/usr/bin/make";
cmakeVars["CMAKE_MATCH_0"]="";
cmakeVars["CMAKE_MATCH_1"]="";
cmakeVars["CMAKE_MATCH_2"]="";
cmakeVars["CMAKE_MATCH_3"]="";
cmakeVars["CMAKE_MINIMUM_REQUIRED_VERSION"]="2.6.4";
cmakeVars["CMAKE_MINOR_VERSION"]="8";
cmakeVars["CMAKE_MODULE_LINKER_FLAGS"]=" ";
cmakeVars["CMAKE_MODULE_LINKER_FLAGS_DEBUG"]="";
cmakeVars["CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL"]="";
cmakeVars["CMAKE_MODULE_LINKER_FLAGS_RELEASE"]="";
cmakeVars["CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO"]="";
cmakeVars["CMAKE_MODULE_PATH"]="/home/SMBAD/schnell/home/mmmtools/CMakeModules/";
cmakeVars["CMAKE_NM"]="/usr/bin/nm";
cmakeVars["CMAKE_OBJCOPY"]="/usr/bin/objcopy";
cmakeVars["CMAKE_OBJDUMP"]="/usr/bin/objdump";
cmakeVars["CMAKE_PARENT_LIST_FILE"]="/home/SMBAD/schnell/home/mmmtools/CMakeLists.txt";
cmakeVars["CMAKE_PATCH_VERSION"]="12";
cmakeVars["CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES"]="/lib;/usr/lib;/usr/lib32;/usr/lib64";
cmakeVars["CMAKE_PLATFORM_INFO_DIR"]="/home/SMBAD/schnell/home/mmmtools/CMakeFiles/2.8.12.2";
cmakeVars["CMAKE_PLATFORM_USES_PATH_WHEN_NO_SONAME"]="1";
cmakeVars["CMAKE_PROJECT_NAME"]="MMMTools";
cmakeVars["CMAKE_RANLIB"]="/usr/bin/ranlib";
cmakeVars["CMAKE_ROOT"]="/usr/share/cmake-2.8";
cmakeVars["CMAKE_SHARED_LIBRARY_CREATE_CXX_FLAGS"]="-shared";
cmakeVars["CMAKE_SHARED_LIBRARY_CREATE_C_FLAGS"]="-shared";
cmakeVars["CMAKE_SHARED_LIBRARY_CXX_FLAGS"]="-fPIC";
cmakeVars["CMAKE_SHARED_LIBRARY_C_FLAGS"]="-fPIC";
cmakeVars["CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS"]="-rdynamic";
cmakeVars["CMAKE_SHARED_LIBRARY_LINK_C_FLAGS"]="-rdynamic";
cmakeVars["CMAKE_SHARED_LIBRARY_LINK_DYNAMIC_CXX_FLAGS"]="-Wl,-Bdynamic";
cmakeVars["CMAKE_SHARED_LIBRARY_LINK_DYNAMIC_C_FLAGS"]="-Wl,-Bdynamic";
cmakeVars["CMAKE_SHARED_LIBRARY_LINK_STATIC_CXX_FLAGS"]="-Wl,-Bstatic";
cmakeVars["CMAKE_SHARED_LIBRARY_LINK_STATIC_C_FLAGS"]="-Wl,-Bstatic";
cmakeVars["CMAKE_SHARED_LIBRARY_PREFIX"]="lib";
cmakeVars["CMAKE_SHARED_LIBRARY_RPATH_LINK_CXX_FLAG"]="-Wl,-rpath-link,";
cmakeVars["CMAKE_SHARED_LIBRARY_RPATH_LINK_C_FLAG"]="-Wl,-rpath-link,";
cmakeVars["CMAKE_SHARED_LIBRARY_RUNTIME_CXX_FLAG"]="-Wl,-rpath,";
cmakeVars["CMAKE_SHARED_LIBRARY_RUNTIME_CXX_FLAG_SEP"]=":";
cmakeVars["CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG"]="-Wl,-rpath,";
cmakeVars["CMAKE_SHARED_LIBRARY_RUNTIME_C_FLAG_SEP"]=":";
cmakeVars["CMAKE_SHARED_LIBRARY_SONAME_CXX_FLAG"]="-Wl,-soname,";
cmakeVars["CMAKE_SHARED_LIBRARY_SONAME_C_FLAG"]="-Wl,-soname,";
cmakeVars["CMAKE_SHARED_LIBRARY_SUFFIX"]=".so";
cmakeVars["CMAKE_SHARED_LINKER_FLAGS"]=" ";
cmakeVars["CMAKE_SHARED_LINKER_FLAGS_DEBUG"]="";
cmakeVars["CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL"]="";
cmakeVars["CMAKE_SHARED_LINKER_FLAGS_RELEASE"]="";
cmakeVars["CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO"]="";
cmakeVars["CMAKE_SHARED_MODULE_CREATE_CXX_FLAGS"]="-shared";
cmakeVars["CMAKE_SHARED_MODULE_CREATE_C_FLAGS"]="-shared";
cmakeVars["CMAKE_SHARED_MODULE_CXX_FLAGS"]="-fPIC";
cmakeVars["CMAKE_SHARED_MODULE_C_FLAGS"]="-fPIC";
cmakeVars["CMAKE_SHARED_MODULE_LINK_DYNAMIC_CXX_FLAGS"]="-Wl,-Bdynamic";
cmakeVars["CMAKE_SHARED_MODULE_LINK_DYNAMIC_C_FLAGS"]="-Wl,-Bdynamic";
cmakeVars["CMAKE_SHARED_MODULE_LINK_STATIC_CXX_FLAGS"]="-Wl,-Bstatic";
cmakeVars["CMAKE_SHARED_MODULE_LINK_STATIC_C_FLAGS"]="-Wl,-Bstatic";
cmakeVars["CMAKE_SHARED_MODULE_PREFIX"]="lib";
cmakeVars["CMAKE_SHARED_MODULE_SUFFIX"]=".so";
cmakeVars["CMAKE_SIZEOF_VOID_P"]="8";
cmakeVars["CMAKE_SKIP_INSTALL_RPATH"]="NO";
cmakeVars["CMAKE_SKIP_RPATH"]="NO";
cmakeVars["CMAKE_SOURCE_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["CMAKE_STATIC_LIBRARY_PREFIX"]="lib";
cmakeVars["CMAKE_STATIC_LIBRARY_SUFFIX"]=".a";
cmakeVars["CMAKE_STATIC_LINKER_FLAGS"]="";
cmakeVars["CMAKE_STATIC_LINKER_FLAGS_DEBUG"]="";
cmakeVars["CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL"]="";
cmakeVars["CMAKE_STATIC_LINKER_FLAGS_RELEASE"]="";
cmakeVars["CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO"]="";
cmakeVars["CMAKE_STRIP"]="/usr/bin/strip";
cmakeVars["CMAKE_SYSTEM"]="Linux-3.13.0-71-generic";
cmakeVars["CMAKE_SYSTEM_INCLUDE_PATH"]="/usr/include/w32api;/usr/X11R6/include;/usr/include/X11;/usr/pkg/include;/opt/csw/include;/opt/include;/usr/openwin/include";
cmakeVars["CMAKE_SYSTEM_INFO_FILE"]="Platform/Linux";
cmakeVars["CMAKE_SYSTEM_LIBRARY_PATH"]="/usr/lib/w32api;/usr/X11R6/lib;/usr/lib/X11;/usr/pkg/lib;/opt/csw/lib;/opt/lib;/usr/openwin/lib";
cmakeVars["CMAKE_SYSTEM_LOADED"]="1";
cmakeVars["CMAKE_SYSTEM_NAME"]="Linux";
cmakeVars["CMAKE_SYSTEM_PREFIX_PATH"]="/usr/local;/usr;/;/usr;/usr/local";
cmakeVars["CMAKE_SYSTEM_PROCESSOR"]="x86_64";
cmakeVars["CMAKE_SYSTEM_PROGRAM_PATH"]="/usr/pkg/bin";
cmakeVars["CMAKE_SYSTEM_SPECIFIC_INFORMATION_LOADED"]="1";
cmakeVars["CMAKE_SYSTEM_VERSION"]="3.13.0-71-generic";
cmakeVars["CMAKE_TESTING_ENABLED"]="1";
cmakeVars["CMAKE_TWEAK_VERSION"]="2";
cmakeVars["CMAKE_UNAME"]="/bin/uname";
cmakeVars["CMAKE_VERBOSE_MAKEFILE"]="FALSE";
cmakeVars["CMAKE_VERSION"]="2.8.12.2";
cmakeVars["COMPRESS_SUBMISSION"]="ON";
cmakeVars["CONF_MMM_TOOLS_LIB_DIR"]="/home/SMBAD/schnell/home/mmmtools/lib";
cmakeVars["CONVERTER_OUTPUT_MARKER_DEVIATION"]="OFF";
cmakeVars["COVERAGE_COMMAND"]="/usr/bin/gcov";
cmakeVars["COVERAGE_EXTRA_FLAGS"]="-l";
cmakeVars["CTEST_SUBMIT_RETRY_COUNT"]="3";
cmakeVars["CTEST_SUBMIT_RETRY_DELAY"]="5";
cmakeVars["CVSCOMMAND"]="/usr/bin/cvs";
cmakeVars["CVS_UPDATE_OPTIONS"]="-d -A -P";
cmakeVars["CXX_TEST_WAS_RUN"]="1";
cmakeVars["C_TEST_WAS_RUN"]="1";
cmakeVars["DART_COMPILER"]="/usr/bin/c++";
cmakeVars["DART_CXX_NAME"]="c++";
cmakeVars["DART_NAME_COMPONENT"]="NAME";
cmakeVars["DART_TESTING_TIMEOUT"]="1500";
cmakeVars["DEFAULT_CTEST_CONFIGURATION_TYPE"]="Release";
cmakeVars["DEF_INSTALL_CMAKE_DIR"]="lib/CMake/MMMTools";
cmakeVars["DL_LIB"]="dl";
cmakeVars["DROP_METHOD"]="http";
cmakeVars["GITCOMMAND"]="/usr/bin/git";
cmakeVars["HGCOMMAND"]="/usr/bin/hg";
cmakeVars["INSTALL_BIN_DIR"]="/usr/local/bin";
cmakeVars["INSTALL_CMAKE_DIR"]="/usr/local/lib/CMake/MMMTools";
cmakeVars["INSTALL_INCLUDE_DIR"]="/usr/local/include";
cmakeVars["INSTALL_LIB_DIR"]="/usr/local/lib";
cmakeVars["MAKECOMMAND"]="/usr/bin/make -i";
cmakeVars["MAKECOMMAND_DEFAULT_VALUE"]="/usr/bin/make -i";
cmakeVars["MEMORYCHECK_COMMAND"]="/usr/bin/valgrind";
cmakeVars["MEMORYCHECK_SUPPRESSIONS_FILE"]="";
cmakeVars["MMMTools_BINARY_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["MMMTools_BIN_DIR"]="/home/SMBAD/schnell/home/mmmtools/bin";
cmakeVars["MMMTools_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["MMMTools_LIB_DIR"]="/home/SMBAD/schnell/home/mmmtools/lib";
cmakeVars["MMMTools_MAJOR_VERSION"]="0";
cmakeVars["MMMTools_MINOR_VERSION"]="1";
cmakeVars["MMMTools_PATCH_VERSION"]="0";
cmakeVars["MMMTools_SOURCE_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["MMMTools_VERSION"]="0.1.0";
cmakeVars["MMM_TEMPLATES_DIR"]="/home/SMBAD/schnell/home/mmmtools/etc/templates";
cmakeVars["MMM_TEST_DIR"]="/home/SMBAD/schnell/home/mmmtools/bin/tests";
cmakeVars["MMM_TEST_H_DIRECTORY"]="/home/SMBAD/schnell/home/mmmtools/testing/";
cmakeVars["NIGHTLY_START_TIME"]="00:00:00 EDT";
cmakeVars["PRESET_CMAKE_SYSTEM_NAME"]="FALSE";
cmakeVars["PROJECT_BINARY_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["PROJECT_NAME"]="MMMTools";
cmakeVars["PROJECT_SOURCE_DIR"]="/home/SMBAD/schnell/home/mmmtools";
cmakeVars["PURIFYCOMMAND"]="";
cmakeVars["RUN_CONFIGURE"]="ON";
cmakeVars["SCPCOMMAND"]="/usr/bin/scp";
cmakeVars["SITE"]="i61pc017";
cmakeVars["SLURM_SBATCH_COMMAND"]="SLURM_SBATCH_COMMAND-NOTFOUND";
cmakeVars["SLURM_SRUN_COMMAND"]="SLURM_SRUN_COMMAND-NOTFOUND";
cmakeVars["SVNCOMMAND"]="/usr/bin/svn";
cmakeVars["SVN_UPDATE_OPTIONS"]="";
cmakeVars["UNIX"]="1";
cmakeVars["UPDATE_COMMAND"]="/usr/bin/git";
cmakeVars["UPDATE_OPTIONS"]="";
cmakeVars["UPDATE_TYPE"]="git";
cmakeVars["_CMAKE_EXTRA_GENERATOR_NO_SPACES"]="CodeBlocks";
cmakeVars["_CMAKE_INSTALL_DIR"]="/usr";
cmakeVars["_CMAKE_TOOLCHAIN_LOCATION"]="/usr/bin";
cmakeVars["_INCLUDED_FILE"]="/usr/share/cmake-2.8/Modules/Platform/Linux-GNU-CXX.cmake";
cmakeVars["_INCLUDED_MULTIARCH_TOOLCHAIN_FILE"]="/usr/share/cmake-2.8/Modules/MultiArchCross.cmake";
cmakeVars["_INCLUDED_SYSTEM_INFO_FILE"]="/usr/share/cmake-2.8/Modules/Platform/Linux.cmake";
cmakeVars["_IN_TC"]="0";
cmakeVars["__COMPILER_GNU"]="1";
cmakeVars["__LINUX_COMPILER_GNU"]="1";
cmakeVars["__UNIX_PATHS_INCLUDED"]="1";
cmakeVars["__conf_types"]="";
cmakeVars["_arg1"]="";
cmakeVars["_compilerExecutable"]="/usr/bin/c++";
cmakeVars["_defineLines"]="#define __STDC__ 1\n;#define __cplusplus 199711L\n;#define __STDC_HOSTED__ 1\n;#define __GNUC__ 4\n;#define __GNUC_MINOR__ 8\n;#define __GNUC_PATCHLEVEL__ 4\n;#define __VERSION__ \"4.8.4\"\n;#define __ATOMIC_RELAXED 0\n;#define __ATOMIC_SEQ_CST 5\n;#define __ATOMIC_ACQUIRE 2\n;#define __ATOMIC_RELEASE 3\n;#define __ATOMIC_ACQ_REL 4\n;#define __ATOMIC_CONSUME 1\n;#define __FINITE_MATH_ONLY__ 0\n;#define _LP64 1\n;#define __LP64__ 1\n;#define __SIZEOF_INT__ 4\n;#define __SIZEOF_LONG__ 8\n;#define __SIZEOF_LONG_LONG__ 8\n;#define __SIZEOF_SHORT__ 2\n;#define __SIZEOF_FLOAT__ 4\n;#define __SIZEOF_DOUBLE__ 8\n;#define __SIZEOF_LONG_DOUBLE__ 16\n;#define __SIZEOF_SIZE_T__ 8\n;#define __CHAR_BIT__ 8\n;#define __BIGGEST_ALIGNMENT__ 16\n;#define __ORDER_LITTLE_ENDIAN__ 1234\n;#define __ORDER_BIG_ENDIAN__ 4321\n;#define __ORDER_PDP_ENDIAN__ 3412\n;#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__\n;#define __FLOAT_WORD_ORDER__ __ORDER_LITTLE_ENDIAN__\n;#define __SIZEOF_POINTER__ 8\n;#define __GNUG__ 4\n;#define __SIZE_TYPE__ long unsigned int\n;#define __PTRDIFF_TYPE__ long int\n;#define __WCHAR_TYPE__ int\n;#define __WINT_TYPE__ unsigned int\n;#define __INTMAX_TYPE__ long int\n;#define __UINTMAX_TYPE__ long unsigned int\n;#define __CHAR16_TYPE__ short unsigned int\n;#define __CHAR32_TYPE__ unsigned int\n;#define __SIG_ATOMIC_TYPE__ int\n;#define __INT8_TYPE__ signed char\n;#define __INT16_TYPE__ short int\n;#define __INT32_TYPE__ int\n;#define __INT64_TYPE__ long int\n;#define __UINT8_TYPE__ unsigned char\n;#define __UINT16_TYPE__ short unsigned int\n;#define __UINT32_TYPE__ unsigned int\n;#define __UINT64_TYPE__ long unsigned int\n;#define __INT_LEAST8_TYPE__ signed char\n;#define __INT_LEAST16_TYPE__ short int\n;#define __INT_LEAST32_TYPE__ int\n;#define __INT_LEAST64_TYPE__ long int\n;#define __UINT_LEAST8_TYPE__ unsigned char\n;#define __UINT_LEAST16_TYPE__ short unsigned int\n;#define __UINT_LEAST32_TYPE__ unsigned int\n;#define __UINT_LEAST64_TYPE__ long unsigned int\n;#define __INT_FAST8_TYPE__ signed char\n;#define __INT_FAST16_TYPE__ long int\n;#define __INT_FAST32_TYPE__ long int\n;#define __INT_FAST64_TYPE__ long int\n;#define __UINT_FAST8_TYPE__ unsigned char\n;#define __UINT_FAST16_TYPE__ long unsigned int\n;#define __UINT_FAST32_TYPE__ long unsigned int\n;#define __UINT_FAST64_TYPE__ long unsigned int\n;#define __INTPTR_TYPE__ long int\n;#define __UINTPTR_TYPE__ long unsigned int\n;#define __GXX_WEAK__ 1\n;#define __DEPRECATED 1\n;#define __GXX_RTTI 1\n;#define __EXCEPTIONS 1\n;#define __GXX_ABI_VERSION 1002\n;#define __SCHAR_MAX__ 127\n;#define __SHRT_MAX__ 32767\n;#define __INT_MAX__ 2147483647\n;#define __LONG_MAX__ 9223372036854775807L\n;#define __LONG_LONG_MAX__ 9223372036854775807LL\n;#define __WCHAR_MAX__ 2147483647\n;#define __WCHAR_MIN__ (-__WCHAR_MAX__ - 1)\n;#define __WINT_MAX__ 4294967295U\n;#define __WINT_MIN__ 0U\n;#define __PTRDIFF_MAX__ 9223372036854775807L\n;#define __SIZE_MAX__ 18446744073709551615UL\n;#define __INTMAX_MAX__ 9223372036854775807L\n;#define __INTMAX_C(c) c ## L\n;#define __UINTMAX_MAX__ 18446744073709551615UL\n;#define __UINTMAX_C(c) c ## UL\n;#define __SIG_ATOMIC_MAX__ 2147483647\n;#define __SIG_ATOMIC_MIN__ (-__SIG_ATOMIC_MAX__ - 1)\n;#define __INT8_MAX__ 127\n;#define __INT16_MAX__ 32767\n;#define __INT32_MAX__ 2147483647\n;#define __INT64_MAX__ 9223372036854775807L\n;#define __UINT8_MAX__ 255\n;#define __UINT16_MAX__ 65535\n;#define __UINT32_MAX__ 4294967295U\n;#define __UINT64_MAX__ 18446744073709551615UL\n;#define __INT_LEAST8_MAX__ 127\n;#define __INT8_C(c) c\n;#define __INT_LEAST16_MAX__ 32767\n;#define __INT16_C(c) c\n;#define __INT_LEAST32_MAX__ 2147483647\n;#define __INT32_C(c) c\n;#define __INT_LEAST64_MAX__ 9223372036854775807L\n;#define __INT64_C(c) c ## L\n;#define __UINT_LEAST8_MAX__ 255\n;#define __UINT8_C(c) c\n;#define __UINT_LEAST16_MAX__ 65535\n;#define __UINT16_C(c) c\n;#define __UINT_LEAST32_MAX__ 4294967295U\n;#define __UINT32_C(c) c ## U\n;#define __UINT_LEAST64_MAX__ 18446744073709551615UL\n;#define __UINT64_C(c) c ## UL\n;#define __INT_FAST8_MAX__ 127\n;#define __INT_FAST16_MAX__ 9223372036854775807L\n;#define __INT_FAST32_MAX__ 9223372036854775807L\n;#define __INT_FAST64_MAX__ 9223372036854775807L\n;#define __UINT_FAST8_MAX__ 255\n;#define __UINT_FAST16_MAX__ 18446744073709551615UL\n;#define __UINT_FAST32_MAX__ 18446744073709551615UL\n;#define __UINT_FAST64_MAX__ 18446744073709551615UL\n;#define __INTPTR_MAX__ 9223372036854775807L\n;#define __UINTPTR_MAX__ 18446744073709551615UL\n;#define __FLT_EVAL_METHOD__ 0\n;#define __DEC_EVAL_METHOD__ 2\n;#define __FLT_RADIX__ 2\n;#define __FLT_MANT_DIG__ 24\n;#define __FLT_DIG__ 6\n;#define __FLT_MIN_EXP__ (-125)\n;#define __FLT_MIN_10_EXP__ (-37)\n;#define __FLT_MAX_EXP__ 128\n;#define __FLT_MAX_10_EXP__ 38\n;#define __FLT_DECIMAL_DIG__ 9\n;#define __FLT_MAX__ 3.40282346638528859812e+38F\n;#define __FLT_MIN__ 1.17549435082228750797e-38F\n;#define __FLT_EPSILON__ 1.19209289550781250000e-7F\n;#define __FLT_DENORM_MIN__ 1.40129846432481707092e-45F\n;#define __FLT_HAS_DENORM__ 1\n;#define __FLT_HAS_INFINITY__ 1\n;#define __FLT_HAS_QUIET_NAN__ 1\n;#define __DBL_MANT_DIG__ 53\n;#define __DBL_DIG__ 15\n;#define __DBL_MIN_EXP__ (-1021)\n;#define __DBL_MIN_10_EXP__ (-307)\n;#define __DBL_MAX_EXP__ 1024\n;#define __DBL_MAX_10_EXP__ 308\n;#define __DBL_DECIMAL_DIG__ 17\n;#define __DBL_MAX__ double(1.79769313486231570815e+308L)\n;#define __DBL_MIN__ double(2.22507385850720138309e-308L)\n;#define __DBL_EPSILON__ double(2.22044604925031308085e-16L)\n;#define __DBL_DENORM_MIN__ double(4.94065645841246544177e-324L)\n;#define __DBL_HAS_DENORM__ 1\n;#define __DBL_HAS_INFINITY__ 1\n;#define __DBL_HAS_QUIET_NAN__ 1\n;#define __LDBL_MANT_DIG__ 64\n;#define __LDBL_DIG__ 18\n;#define __LDBL_MIN_EXP__ (-16381)\n;#define __LDBL_MIN_10_EXP__ (-4931)\n;#define __LDBL_MAX_EXP__ 16384\n;#define __LDBL_MAX_10_EXP__ 4932\n;#define __DECIMAL_DIG__ 21\n;#define __LDBL_MAX__ 1.18973149535723176502e+4932L\n;#define __LDBL_MIN__ 3.36210314311209350626e-4932L\n;#define __LDBL_EPSILON__ 1.08420217248550443401e-19L\n;#define __LDBL_DENORM_MIN__ 3.64519953188247460253e-4951L\n;#define __LDBL_HAS_DENORM__ 1\n;#define __LDBL_HAS_INFINITY__ 1\n;#define __LDBL_HAS_QUIET_NAN__ 1\n;#define __DEC32_MANT_DIG__ 7\n;#define __DEC32_MIN_EXP__ (-94)\n;#define __DEC32_MAX_EXP__ 97\n;#define __DEC32_MIN__ 1E-95DF\n;#define __DEC32_MAX__ 9.999999E96DF\n;#define __DEC32_EPSILON__ 1E-6DF\n;#define __DEC32_SUBNORMAL_MIN__ 0.000001E-95DF\n;#define __DEC64_MANT_DIG__ 16\n;#define __DEC64_MIN_EXP__ (-382)\n;#define __DEC64_MAX_EXP__ 385\n;#define __DEC64_MIN__ 1E-383DD\n;#define __DEC64_MAX__ 9.999999999999999E384DD\n;#define __DEC64_EPSILON__ 1E-15DD\n;#define __DEC64_SUBNORMAL_MIN__ 0.000000000000001E-383DD\n;#define __DEC128_MANT_DIG__ 34\n;#define __DEC128_MIN_EXP__ (-6142)\n;#define __DEC128_MAX_EXP__ 6145\n;#define __DEC128_MIN__ 1E-6143DL\n;#define __DEC128_MAX__ 9.999999999999999999999999999999999E6144DL\n;#define __DEC128_EPSILON__ 1E-33DL\n;#define __DEC128_SUBNORMAL_MIN__ 0.000000000000000000000000000000001E-6143DL\n;#define __REGISTER_PREFIX__ \n;#define __USER_LABEL_PREFIX__ \n;#define __GNUC_GNU_INLINE__ 1\n;#define __NO_INLINE__ 1\n;#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 1\n;#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 1\n;#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 1\n;#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 1\n;#define __GCC_ATOMIC_BOOL_LOCK_FREE 2\n;#define __GCC_ATOMIC_CHAR_LOCK_FREE 2\n;#define __GCC_ATOMIC_CHAR16_T_LOCK_FREE 2\n;#define __GCC_ATOMIC_CHAR32_T_LOCK_FREE 2\n;#define __GCC_ATOMIC_WCHAR_T_LOCK_FREE 2\n;#define __GCC_ATOMIC_SHORT_LOCK_FREE 2\n;#define __GCC_ATOMIC_INT_LOCK_FREE 2\n;#define __GCC_ATOMIC_LONG_LOCK_FREE 2\n;#define __GCC_ATOMIC_LLONG_LOCK_FREE 2\n;#define __GCC_ATOMIC_TEST_AND_SET_TRUEVAL 1\n;#define __GCC_ATOMIC_POINTER_LOCK_FREE 2\n;#define __GCC_HAVE_DWARF2_CFI_ASM 1\n;#define __PRAGMA_REDEFINE_EXTNAME 1\n;#define __SSP__ 1\n;#define __SIZEOF_INT128__ 16\n;#define __SIZEOF_WCHAR_T__ 4\n;#define __SIZEOF_WINT_T__ 4\n;#define __SIZEOF_PTRDIFF_T__ 8\n;#define __amd64 1\n;#define __amd64__ 1\n;#define __x86_64 1\n;#define __x86_64__ 1\n;#define __ATOMIC_HLE_ACQUIRE 65536\n;#define __ATOMIC_HLE_RELEASE 131072\n;#define __k8 1\n;#define __k8__ 1\n;#define __code_model_small__ 1\n;#define __MMX__ 1\n;#define __SSE__ 1\n;#define __SSE2__ 1\n;#define __FXSR__ 1\n;#define __SSE_MATH__ 1\n;#define __SSE2_MATH__ 1\n;#define __gnu_linux__ 1\n;#define __linux 1\n;#define __linux__ 1\n;#define linux 1\n;#define __unix 1\n;#define __unix__ 1\n;#define unix 1\n;#define __ELF__ 1\n;#define __DECIMAL_BID_FORMAT__ 1\n;#define _GNU_SOURCE 1\n;#define _STDC_PREDEF_H 1\n;#define __STDC_IEC_559__ 1\n;#define __STDC_IEC_559_COMPLEX__ 1\n;#define __STDC_ISO_10646__ 201103L\n;#define __STDC_NO_THREADS__ 1\n";
cmakeVars["_defines"]="__STDC__;1;__STDC_HOSTED__;1;__GNUC__;4;__GNUC_MINOR__;8;__GNUC_PATCHLEVEL__;4;__VERSION__;\"4.8.4\";__ATOMIC_RELAXED; ;__ATOMIC_SEQ_CST;5;__ATOMIC_ACQUIRE;2;__ATOMIC_RELEASE;3;__ATOMIC_ACQ_REL;4;__ATOMIC_CONSUME;1;__FINITE_MATH_ONLY__; ;_LP64;1;__LP64__;1;__SIZEOF_INT__;4;__SIZEOF_LONG__;8;__SIZEOF_LONG_LONG__;8;__SIZEOF_SHORT__;2;__SIZEOF_FLOAT__;4;__SIZEOF_DOUBLE__;8;__SIZEOF_LONG_DOUBLE__;16;__SIZEOF_SIZE_T__;8;__CHAR_BIT__;8;__BIGGEST_ALIGNMENT__;16;__ORDER_LITTLE_ENDIAN__;1234;__ORDER_BIG_ENDIAN__;4321;__ORDER_PDP_ENDIAN__;3412;__BYTE_ORDER__;__ORDER_LITTLE_ENDIAN__;__FLOAT_WORD_ORDER__;__ORDER_LITTLE_ENDIAN__;__SIZEOF_POINTER__;8;__SIZE_TYPE__;long unsigned int;__PTRDIFF_TYPE__;long int;__WCHAR_TYPE__;int;__WINT_TYPE__;unsigned int;__INTMAX_TYPE__;long int;__UINTMAX_TYPE__;long unsigned int;__CHAR16_TYPE__;short unsigned int;__CHAR32_TYPE__;unsigned int;__SIG_ATOMIC_TYPE__;int;__INT8_TYPE__;signed char;__INT16_TYPE__;short int;__INT32_TYPE__;int;__INT64_TYPE__;long int;__UINT8_TYPE__;unsigned char;__UINT16_TYPE__;short unsigned int;__UINT32_TYPE__;unsigned int;__UINT64_TYPE__;long unsigned int;__INT_LEAST8_TYPE__;signed char;__INT_LEAST16_TYPE__;short int;__INT_LEAST32_TYPE__;int;__INT_LEAST64_TYPE__;long int;__UINT_LEAST8_TYPE__;unsigned char;__UINT_LEAST16_TYPE__;short unsigned int;__UINT_LEAST32_TYPE__;unsigned int;__UINT_LEAST64_TYPE__;long unsigned int;__INT_FAST8_TYPE__;signed char;__INT_FAST16_TYPE__;long int;__INT_FAST32_TYPE__;long int;__INT_FAST64_TYPE__;long int;__UINT_FAST8_TYPE__;unsigned char;__UINT_FAST16_TYPE__;long unsigned int;__UINT_FAST32_TYPE__;long unsigned int;__UINT_FAST64_TYPE__;long unsigned int;__INTPTR_TYPE__;long int;__UINTPTR_TYPE__;long unsigned int;__GXX_ABI_VERSION;1002;__SCHAR_MAX__;127;__SHRT_MAX__;32767;__INT_MAX__;2147483647;__LONG_MAX__;9223372036854775807L;__LONG_LONG_MAX__;9223372036854775807LL;__WCHAR_MAX__;2147483647;__WCHAR_MIN__;(-__WCHAR_MAX__ - 1);__WINT_MAX__;4294967295U;__WINT_MIN__;0U;__PTRDIFF_MAX__;9223372036854775807L;__SIZE_MAX__;18446744073709551615UL;__INTMAX_MAX__;9223372036854775807L;__INTMAX_C(c);c ## L;__UINTMAX_MAX__;18446744073709551615UL;__UINTMAX_C(c);c ## UL;__SIG_ATOMIC_MAX__;2147483647;__SIG_ATOMIC_MIN__;(-__SIG_ATOMIC_MAX__ - 1);__INT8_MAX__;127;__INT16_MAX__;32767;__INT32_MAX__;2147483647;__INT64_MAX__;9223372036854775807L;__UINT8_MAX__;255;__UINT16_MAX__;65535;__UINT32_MAX__;4294967295U;__UINT64_MAX__;18446744073709551615UL;__INT_LEAST8_MAX__;127;__INT8_C(c);c;__INT_LEAST16_MAX__;32767;__INT16_C(c);c;__INT_LEAST32_MAX__;2147483647;__INT32_C(c);c;__INT_LEAST64_MAX__;9223372036854775807L;__INT64_C(c);c ## L;__UINT_LEAST8_MAX__;255;__UINT8_C(c);c;__UINT_LEAST16_MAX__;65535;__UINT16_C(c);c;__UINT_LEAST32_MAX__;4294967295U;__UINT32_C(c);c ## U;__UINT_LEAST64_MAX__;18446744073709551615UL;__UINT64_C(c);c ## UL;__INT_FAST8_MAX__;127;__INT_FAST16_MAX__;9223372036854775807L;__INT_FAST32_MAX__;9223372036854775807L;__INT_FAST64_MAX__;9223372036854775807L;__UINT_FAST8_MAX__;255;__UINT_FAST16_MAX__;18446744073709551615UL;__UINT_FAST32_MAX__;18446744073709551615UL;__UINT_FAST64_MAX__;18446744073709551615UL;__INTPTR_MAX__;9223372036854775807L;__UINTPTR_MAX__;18446744073709551615UL;__FLT_EVAL_METHOD__; ;__DEC_EVAL_METHOD__;2;__FLT_RADIX__;2;__FLT_MANT_DIG__;24;__FLT_DIG__;6;__FLT_MIN_EXP__;(-125);__FLT_MIN_10_EXP__;(-37);__FLT_MAX_EXP__;128;__FLT_MAX_10_EXP__;38;__FLT_DECIMAL_DIG__;9;__FLT_MAX__;3.40282346638528859812e+38F;__FLT_MIN__;1.17549435082228750797e-38F;__FLT_EPSILON__;1.19209289550781250000e-7F;__FLT_DENORM_MIN__;1.40129846432481707092e-45F;__FLT_HAS_DENORM__;1;__FLT_HAS_INFINITY__;1;__FLT_HAS_QUIET_NAN__;1;__DBL_MANT_DIG__;53;__DBL_DIG__;15;__DBL_MIN_EXP__;(-1021);__DBL_MIN_10_EXP__;(-307);__DBL_MAX_EXP__;1024;__DBL_MAX_10_EXP__;308;__DBL_DECIMAL_DIG__;17;__DBL_MAX__;((double)1.79769313486231570815e+308L);__DBL_MIN__;((double)2.22507385850720138309e-308L);__DBL_EPSILON__;((double)2.22044604925031308085e-16L);__DBL_DENORM_MIN__;((double)4.94065645841246544177e-324L);__DBL_HAS_DENORM__;1;__DBL_HAS_INFINITY__;1;__DBL_HAS_QUIET_NAN__;1;__LDBL_MANT_DIG__;64;__LDBL_DIG__;18;__LDBL_MIN_EXP__;(-16381);__LDBL_MIN_10_EXP__;(-4931);__LDBL_MAX_EXP__;16384;__LDBL_MAX_10_EXP__;4932;__DECIMAL_DIG__;21;__LDBL_MAX__;1.18973149535723176502e+4932L;__LDBL_MIN__;3.36210314311209350626e-4932L;__LDBL_EPSILON__;1.08420217248550443401e-19L;__LDBL_DENORM_MIN__;3.64519953188247460253e-4951L;__LDBL_HAS_DENORM__;1;__LDBL_HAS_INFINITY__;1;__LDBL_HAS_QUIET_NAN__;1;__DEC32_MANT_DIG__;7;__DEC32_MIN_EXP__;(-94);__DEC32_MAX_EXP__;97;__DEC32_MIN__;1E-95DF;__DEC32_MAX__;9.999999E96DF;__DEC32_EPSILON__;1E-6DF;__DEC32_SUBNORMAL_MIN__;0.000001E-95DF;__DEC64_MANT_DIG__;16;__DEC64_MIN_EXP__;(-382);__DEC64_MAX_EXP__;385;__DEC64_MIN__;1E-383DD;__DEC64_MAX__;9.999999999999999E384DD;__DEC64_EPSILON__;1E-15DD;__DEC64_SUBNORMAL_MIN__;0.000000000000001E-383DD;__DEC128_MANT_DIG__;34;__DEC128_MIN_EXP__;(-6142);__DEC128_MAX_EXP__;6145;__DEC128_MIN__;1E-6143DL;__DEC128_MAX__;9.999999999999999999999999999999999E6144DL;__DEC128_EPSILON__;1E-33DL;__DEC128_SUBNORMAL_MIN__;0.000000000000000000000000000000001E-6143DL;__REGISTER_PREFIX__; ;__USER_LABEL_PREFIX__; ;__GNUC_GNU_INLINE__;1;__NO_INLINE__;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8;1;__GCC_ATOMIC_BOOL_LOCK_FREE;2;__GCC_ATOMIC_CHAR_LOCK_FREE;2;__GCC_ATOMIC_CHAR16_T_LOCK_FREE;2;__GCC_ATOMIC_CHAR32_T_LOCK_FREE;2;__GCC_ATOMIC_WCHAR_T_LOCK_FREE;2;__GCC_ATOMIC_SHORT_LOCK_FREE;2;__GCC_ATOMIC_INT_LOCK_FREE;2;__GCC_ATOMIC_LONG_LOCK_FREE;2;__GCC_ATOMIC_LLONG_LOCK_FREE;2;__GCC_ATOMIC_TEST_AND_SET_TRUEVAL;1;__GCC_ATOMIC_POINTER_LOCK_FREE;2;__GCC_HAVE_DWARF2_CFI_ASM;1;__PRAGMA_REDEFINE_EXTNAME;1;__SSP__;1;__SIZEOF_INT128__;16;__SIZEOF_WCHAR_T__;4;__SIZEOF_WINT_T__;4;__SIZEOF_PTRDIFF_T__;8;__amd64;1;__amd64__;1;__x86_64;1;__x86_64__;1;__ATOMIC_HLE_ACQUIRE;65536;__ATOMIC_HLE_RELEASE;131072;__k8;1;__k8__;1;__code_model_small__;1;__MMX__;1;__SSE__;1;__SSE2__;1;__FXSR__;1;__SSE_MATH__;1;__SSE2_MATH__;1;__gnu_linux__;1;__linux;1;__linux__;1;linux;1;__unix;1;__unix__;1;unix;1;__ELF__;1;__DECIMAL_BID_FORMAT__;1;_STDC_PREDEF_H;1;__STDC_IEC_559__;1;__STDC_IEC_559_COMPLEX__;1;__STDC_ISO_10646__;201103L;__STDC_NO_THREADS__;1;__STDC__;1;__cplusplus;199711L;__STDC_HOSTED__;1;__GNUC__;4;__GNUC_MINOR__;8;__GNUC_PATCHLEVEL__;4;__VERSION__;\"4.8.4\";__ATOMIC_RELAXED; ;__ATOMIC_SEQ_CST;5;__ATOMIC_ACQUIRE;2;__ATOMIC_RELEASE;3;__ATOMIC_ACQ_REL;4;__ATOMIC_CONSUME;1;__FINITE_MATH_ONLY__; ;_LP64;1;__LP64__;1;__SIZEOF_INT__;4;__SIZEOF_LONG__;8;__SIZEOF_LONG_LONG__;8;__SIZEOF_SHORT__;2;__SIZEOF_FLOAT__;4;__SIZEOF_DOUBLE__;8;__SIZEOF_LONG_DOUBLE__;16;__SIZEOF_SIZE_T__;8;__CHAR_BIT__;8;__BIGGEST_ALIGNMENT__;16;__ORDER_LITTLE_ENDIAN__;1234;__ORDER_BIG_ENDIAN__;4321;__ORDER_PDP_ENDIAN__;3412;__BYTE_ORDER__;__ORDER_LITTLE_ENDIAN__;__FLOAT_WORD_ORDER__;__ORDER_LITTLE_ENDIAN__;__SIZEOF_POINTER__;8;__GNUG__;4;__SIZE_TYPE__;long unsigned int;__PTRDIFF_TYPE__;long int;__WCHAR_TYPE__;int;__WINT_TYPE__;unsigned int;__INTMAX_TYPE__;long int;__UINTMAX_TYPE__;long unsigned int;__CHAR16_TYPE__;short unsigned int;__CHAR32_TYPE__;unsigned int;__SIG_ATOMIC_TYPE__;int;__INT8_TYPE__;signed char;__INT16_TYPE__;short int;__INT32_TYPE__;int;__INT64_TYPE__;long int;__UINT8_TYPE__;unsigned char;__UINT16_TYPE__;short unsigned int;__UINT32_TYPE__;unsigned int;__UINT64_TYPE__;long unsigned int;__INT_LEAST8_TYPE__;signed char;__INT_LEAST16_TYPE__;short int;__INT_LEAST32_TYPE__;int;__INT_LEAST64_TYPE__;long int;__UINT_LEAST8_TYPE__;unsigned char;__UINT_LEAST16_TYPE__;short unsigned int;__UINT_LEAST32_TYPE__;unsigned int;__UINT_LEAST64_TYPE__;long unsigned int;__INT_FAST8_TYPE__;signed char;__INT_FAST16_TYPE__;long int;__INT_FAST32_TYPE__;long int;__INT_FAST64_TYPE__;long int;__UINT_FAST8_TYPE__;unsigned char;__UINT_FAST16_TYPE__;long unsigned int;__UINT_FAST32_TYPE__;long unsigned int;__UINT_FAST64_TYPE__;long unsigned int;__INTPTR_TYPE__;long int;__UINTPTR_TYPE__;long unsigned int;__GXX_WEAK__;1;__DEPRECATED;1;__GXX_RTTI;1;__EXCEPTIONS;1;__GXX_ABI_VERSION;1002;__SCHAR_MAX__;127;__SHRT_MAX__;32767;__INT_MAX__;2147483647;__LONG_MAX__;9223372036854775807L;__LONG_LONG_MAX__;9223372036854775807LL;__WCHAR_MAX__;2147483647;__WCHAR_MIN__;(-__WCHAR_MAX__ - 1);__WINT_MAX__;4294967295U;__WINT_MIN__;0U;__PTRDIFF_MAX__;9223372036854775807L;__SIZE_MAX__;18446744073709551615UL;__INTMAX_MAX__;9223372036854775807L;__INTMAX_C(c);c ## L;__UINTMAX_MAX__;18446744073709551615UL;__UINTMAX_C(c);c ## UL;__SIG_ATOMIC_MAX__;2147483647;__SIG_ATOMIC_MIN__;(-__SIG_ATOMIC_MAX__ - 1);__INT8_MAX__;127;__INT16_MAX__;32767;__INT32_MAX__;2147483647;__INT64_MAX__;9223372036854775807L;__UINT8_MAX__;255;__UINT16_MAX__;65535;__UINT32_MAX__;4294967295U;__UINT64_MAX__;18446744073709551615UL;__INT_LEAST8_MAX__;127;__INT8_C(c);c;__INT_LEAST16_MAX__;32767;__INT16_C(c);c;__INT_LEAST32_MAX__;2147483647;__INT32_C(c);c;__INT_LEAST64_MAX__;9223372036854775807L;__INT64_C(c);c ## L;__UINT_LEAST8_MAX__;255;__UINT8_C(c);c;__UINT_LEAST16_MAX__;65535;__UINT16_C(c);c;__UINT_LEAST32_MAX__;4294967295U;__UINT32_C(c);c ## U;__UINT_LEAST64_MAX__;18446744073709551615UL;__UINT64_C(c);c ## UL;__INT_FAST8_MAX__;127;__INT_FAST16_MAX__;9223372036854775807L;__INT_FAST32_MAX__;9223372036854775807L;__INT_FAST64_MAX__;9223372036854775807L;__UINT_FAST8_MAX__;255;__UINT_FAST16_MAX__;18446744073709551615UL;__UINT_FAST32_MAX__;18446744073709551615UL;__UINT_FAST64_MAX__;18446744073709551615UL;__INTPTR_MAX__;9223372036854775807L;__UINTPTR_MAX__;18446744073709551615UL;__FLT_EVAL_METHOD__; ;__DEC_EVAL_METHOD__;2;__FLT_RADIX__;2;__FLT_MANT_DIG__;24;__FLT_DIG__;6;__FLT_MIN_EXP__;(-125);__FLT_MIN_10_EXP__;(-37);__FLT_MAX_EXP__;128;__FLT_MAX_10_EXP__;38;__FLT_DECIMAL_DIG__;9;__FLT_MAX__;3.40282346638528859812e+38F;__FLT_MIN__;1.17549435082228750797e-38F;__FLT_EPSILON__;1.19209289550781250000e-7F;__FLT_DENORM_MIN__;1.40129846432481707092e-45F;__FLT_HAS_DENORM__;1;__FLT_HAS_INFINITY__;1;__FLT_HAS_QUIET_NAN__;1;__DBL_MANT_DIG__;53;__DBL_DIG__;15;__DBL_MIN_EXP__;(-1021);__DBL_MIN_10_EXP__;(-307);__DBL_MAX_EXP__;1024;__DBL_MAX_10_EXP__;308;__DBL_DECIMAL_DIG__;17;__DBL_MAX__;double(1.79769313486231570815e+308L);__DBL_MIN__;double(2.22507385850720138309e-308L);__DBL_EPSILON__;double(2.22044604925031308085e-16L);__DBL_DENORM_MIN__;double(4.94065645841246544177e-324L);__DBL_HAS_DENORM__;1;__DBL_HAS_INFINITY__;1;__DBL_HAS_QUIET_NAN__;1;__LDBL_MANT_DIG__;64;__LDBL_DIG__;18;__LDBL_MIN_EXP__;(-16381);__LDBL_MIN_10_EXP__;(-4931);__LDBL_MAX_EXP__;16384;__LDBL_MAX_10_EXP__;4932;__DECIMAL_DIG__;21;__LDBL_MAX__;1.18973149535723176502e+4932L;__LDBL_MIN__;3.36210314311209350626e-4932L;__LDBL_EPSILON__;1.08420217248550443401e-19L;__LDBL_DENORM_MIN__;3.64519953188247460253e-4951L;__LDBL_HAS_DENORM__;1;__LDBL_HAS_INFINITY__;1;__LDBL_HAS_QUIET_NAN__;1;__DEC32_MANT_DIG__;7;__DEC32_MIN_EXP__;(-94);__DEC32_MAX_EXP__;97;__DEC32_MIN__;1E-95DF;__DEC32_MAX__;9.999999E96DF;__DEC32_EPSILON__;1E-6DF;__DEC32_SUBNORMAL_MIN__;0.000001E-95DF;__DEC64_MANT_DIG__;16;__DEC64_MIN_EXP__;(-382);__DEC64_MAX_EXP__;385;__DEC64_MIN__;1E-383DD;__DEC64_MAX__;9.999999999999999E384DD;__DEC64_EPSILON__;1E-15DD;__DEC64_SUBNORMAL_MIN__;0.000000000000001E-383DD;__DEC128_MANT_DIG__;34;__DEC128_MIN_EXP__;(-6142);__DEC128_MAX_EXP__;6145;__DEC128_MIN__;1E-6143DL;__DEC128_MAX__;9.999999999999999999999999999999999E6144DL;__DEC128_EPSILON__;1E-33DL;__DEC128_SUBNORMAL_MIN__;0.000000000000000000000000000000001E-6143DL;__REGISTER_PREFIX__; ;__USER_LABEL_PREFIX__; ;__GNUC_GNU_INLINE__;1;__NO_INLINE__;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_1;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_2;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_4;1;__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8;1;__GCC_ATOMIC_BOOL_LOCK_FREE;2;__GCC_ATOMIC_CHAR_LOCK_FREE;2;__GCC_ATOMIC_CHAR16_T_LOCK_FREE;2;__GCC_ATOMIC_CHAR32_T_LOCK_FREE;2;__GCC_ATOMIC_WCHAR_T_LOCK_FREE;2;__GCC_ATOMIC_SHORT_LOCK_FREE;2;__GCC_ATOMIC_INT_LOCK_FREE;2;__GCC_ATOMIC_LONG_LOCK_FREE;2;__GCC_ATOMIC_LLONG_LOCK_FREE;2;__GCC_ATOMIC_TEST_AND_SET_TRUEVAL;1;__GCC_ATOMIC_POINTER_LOCK_FREE;2;__GCC_HAVE_DWARF2_CFI_ASM;1;__PRAGMA_REDEFINE_EXTNAME;1;__SSP__;1;__SIZEOF_INT128__;16;__SIZEOF_WCHAR_T__;4;__SIZEOF_WINT_T__;4;__SIZEOF_PTRDIFF_T__;8;__amd64;1;__amd64__;1;__x86_64;1;__x86_64__;1;__ATOMIC_HLE_ACQUIRE;65536;__ATOMIC_HLE_RELEASE;131072;__k8;1;__k8__;1;__code_model_small__;1;__MMX__;1;__SSE__;1;__SSE2__;1;__FXSR__;1;__SSE_MATH__;1;__SSE2_MATH__;1;__gnu_linux__;1;__linux;1;__linux__;1;linux;1;__unix;1;__unix__;1;unix;1;__ELF__;1;__DECIMAL_BID_FORMAT__;1;_GNU_SOURCE;1;_STDC_PREDEF_H;1;__STDC_IEC_559__;1;__STDC_IEC_559_COMPLEX__;1;__STDC_ISO_10646__;201103L;__STDC_NO_THREADS__;1";
cmakeVars["_dirs"]="/usr/include/c++/4.8;/usr/include/x86_64-linux-gnu/c++/4.8;/usr/include/c++/4.8/backward;/usr/lib/gcc/x86_64-linux-gnu/4.8/include;/usr/local/include;/usr/lib/gcc/x86_64-linux-gnu/4.8/include-fixed;/usr/include/x86_64-linux-gnu;/usr/include";
cmakeVars["_dummy"]="#define __STDC_NO_THREADS__ 1\n";
cmakeVars["_gccOutput"]="Using built-in specs.\nCOLLECT_GCC=/usr/bin/c++\nTarget: x86_64-linux-gnu\nConfigured with: ../src/configure -v --with-pkgversion='Ubuntu 4.8.4-2ubuntu1~14.04' --with-bugurl=file:///usr/share/doc/gcc-4.8/README.Bugs --enable-languages=c,c++,java,go,d,fortran,objc,obj-c++ --prefix=/usr --program-suffix=-4.8 --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --with-gxx-include-dir=/usr/include/c++/4.8 --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --enable-gnu-unique-object --disable-libmudflap --enable-plugin --with-system-zlib --disable-browser-plugin --enable-java-awt=gtk --enable-gtk-cairo --with-java-home=/usr/lib/jvm/java-1.5.0-gcj-4.8-amd64/jre --enable-java-home --with-jvm-root-dir=/usr/lib/jvm/java-1.5.0-gcj-4.8-amd64 --with-jvm-jar-dir=/usr/lib/jvm-exports/java-1.5.0-gcj-4.8-amd64 --with-arch-directory=amd64 --with-ecj-jar=/usr/share/java/eclipse-ecj.jar --enable-objc-gc --enable-multiarch --disable-werror --with-arch-32=i686 --with-abi=m64 --with-multilib-list=m32,m64,mx32 --with-tune=generic --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu\nThread model: posix\ngcc version 4.8.4 (Ubuntu 4.8.4-2ubuntu1~14.04) \nCOLLECT_GCC_OPTIONS='-v' '-E' '-dD' '-shared-libgcc' '-mtune=generic' '-march=x86-64'\n /usr/lib/gcc/x86_64-linux-gnu/4.8/cc1plus -E -quiet -v -imultiarch x86_64-linux-gnu -D_GNU_SOURCE dummy -mtune=generic -march=x86-64 -fstack-protector -Wformat -Wformat-security -dD\nignoring duplicate directory \"/usr/include/x86_64-linux-gnu/c++/4.8\"\nignoring nonexistent directory \"/usr/local/include/x86_64-linux-gnu\"\nignoring nonexistent directory \"/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../x86_64-linux-gnu/include\"\n#include \"...\" search starts here:\n#include <...> search starts here:\n /usr/include/c++/4.8\n /usr/include/x86_64-linux-gnu/c++/4.8\n /usr/include/c++/4.8/backward\n /usr/lib/gcc/x86_64-linux-gnu/4.8/include\n /usr/local/include\n /usr/lib/gcc/x86_64-linux-gnu/4.8/include-fixed\n /usr/include/x86_64-linux-gnu\n /usr/include\nEnd of search list.\nCOMPILER_PATH=/usr/lib/gcc/x86_64-linux-gnu/4.8/:/usr/lib/gcc/x86_64-linux-gnu/4.8/:/usr/lib/gcc/x86_64-linux-gnu/:/usr/lib/gcc/x86_64-linux-gnu/4.8/:/usr/lib/gcc/x86_64-linux-gnu/\nLIBRARY_PATH=/usr/lib/gcc/x86_64-linux-gnu/4.8/:/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../x86_64-linux-gnu/:/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../../lib/:/lib/x86_64-linux-gnu/:/lib/../lib/:/usr/lib/x86_64-linux-gnu/:/usr/lib/../lib/:/usr/lib/gcc/x86_64-linux-gnu/4.8/../../../:/lib/:/usr/lib/\nCOLLECT_GCC_OPTIONS='-v' '-E' '-dD' '-shared-libgcc' '-mtune=generic' '-march=x86-64'\n";
cmakeVars["_gccStdout"]="# 1 \"dummy\"\n# 1 \"<built-in>\"\n#define __STDC__ 1\n#define __cplusplus 199711L\n#define __STDC_HOSTED__ 1\n#define __GNUC__ 4\n#define __GNUC_MINOR__ 8\n#define __GNUC_PATCHLEVEL__ 4\n#define __VERSION__ \"4.8.4\"\n#define __ATOMIC_RELAXED 0\n#define __ATOMIC_SEQ_CST 5\n#define __ATOMIC_ACQUIRE 2\n#define __ATOMIC_RELEASE 3\n#define __ATOMIC_ACQ_REL 4\n#define __ATOMIC_CONSUME 1\n#define __FINITE_MATH_ONLY__ 0\n#define _LP64 1\n#define __LP64__ 1\n#define __SIZEOF_INT__ 4\n#define __SIZEOF_LONG__ 8\n#define __SIZEOF_LONG_LONG__ 8\n#define __SIZEOF_SHORT__ 2\n#define __SIZEOF_FLOAT__ 4\n#define __SIZEOF_DOUBLE__ 8\n#define __SIZEOF_LONG_DOUBLE__ 16\n#define __SIZEOF_SIZE_T__ 8\n#define __CHAR_BIT__ 8\n#define __BIGGEST_ALIGNMENT__ 16\n#define __ORDER_LITTLE_ENDIAN__ 1234\n#define __ORDER_BIG_ENDIAN__ 4321\n#define __ORDER_PDP_ENDIAN__ 3412\n#define __BYTE_ORDER__ __ORDER_LITTLE_ENDIAN__\n#define __FLOAT_WORD_ORDER__ __ORDER_LITTLE_ENDIAN__\n#define __SIZEOF_POINTER__ 8\n#define __GNUG__ 4\n#define __SIZE_TYPE__ long unsigned int\n#define __PTRDIFF_TYPE__ long int\n#define __WCHAR_TYPE__ int\n#define __WINT_TYPE__ unsigned int\n#define __INTMAX_TYPE__ long int\n#define __UINTMAX_TYPE__ long unsigned int\n#define __CHAR16_TYPE__ short unsigned int\n#define __CHAR32_TYPE__ unsigned int\n#define __SIG_ATOMIC_TYPE__ int\n#define __INT8_TYPE__ signed char\n#define __INT16_TYPE__ short int\n#define __INT32_TYPE__ int\n#define __INT64_TYPE__ long int\n#define __UINT8_TYPE__ unsigned char\n#define __UINT16_TYPE__ short unsigned int\n#define __UINT32_TYPE__ unsigned int\n#define __UINT64_TYPE__ long unsigned int\n#define __INT_LEAST8_TYPE__ signed char\n#define __INT_LEAST16_TYPE__ short int\n#define __INT_LEAST32_TYPE__ int\n#define __INT_LEAST64_TYPE__ long int\n#define __UINT_LEAST8_TYPE__ unsigned char\n#define __UINT_LEAST16_TYPE__ short unsigned int\n#define __UINT_LEAST32_TYPE__ unsigned int\n#define __UINT_LEAST64_TYPE__ long unsigned int\n#define __INT_FAST8_TYPE__ signed char\n#define __INT_FAST16_TYPE__ long int\n#define __INT_FAST32_TYPE__ long int\n#define __INT_FAST64_TYPE__ long int\n#define __UINT_FAST8_TYPE__ unsigned char\n#define __UINT_FAST16_TYPE__ long unsigned int\n#define __UINT_FAST32_TYPE__ long unsigned int\n#define __UINT_FAST64_TYPE__ long unsigned int\n#define __INTPTR_TYPE__ long int\n#define __UINTPTR_TYPE__ long unsigned int\n#define __GXX_WEAK__ 1\n#define __DEPRECATED 1\n#define __GXX_RTTI 1\n#define __EXCEPTIONS 1\n#define __GXX_ABI_VERSION 1002\n#define __SCHAR_MAX__ 127\n#define __SHRT_MAX__ 32767\n#define __INT_MAX__ 2147483647\n#define __LONG_MAX__ 9223372036854775807L\n#define __LONG_LONG_MAX__ 9223372036854775807LL\n#define __WCHAR_MAX__ 2147483647\n#define __WCHAR_MIN__ (-__WCHAR_MAX__ - 1)\n#define __WINT_MAX__ 4294967295U\n#define __WINT_MIN__ 0U\n#define __PTRDIFF_MAX__ 9223372036854775807L\n#define __SIZE_MAX__ 18446744073709551615UL\n#define __INTMAX_MAX__ 9223372036854775807L\n#define __INTMAX_C(c) c ## L\n#define __UINTMAX_MAX__ 18446744073709551615UL\n#define __UINTMAX_C(c) c ## UL\n#define __SIG_ATOMIC_MAX__ 2147483647\n#define __SIG_ATOMIC_MIN__ (-__SIG_ATOMIC_MAX__ - 1)\n#define __INT8_MAX__ 127\n#define __INT16_MAX__ 32767\n#define __INT32_MAX__ 2147483647\n#define __INT64_MAX__ 9223372036854775807L\n#define __UINT8_MAX__ 255\n#define __UINT16_MAX__ 65535\n#define __UINT32_MAX__ 4294967295U\n#define __UINT64_MAX__ 18446744073709551615UL\n#define __INT_LEAST8_MAX__ 127\n#define __INT8_C(c) c\n#define __INT_LEAST16_MAX__ 32767\n#define __INT16_C(c) c\n#define __INT_LEAST32_MAX__ 2147483647\n#define __INT32_C(c) c\n#define __INT_LEAST64_MAX__ 9223372036854775807L\n#define __INT64_C(c) c ## L\n#define __UINT_LEAST8_MAX__ 255\n#define __UINT8_C(c) c\n#define __UINT_LEAST16_MAX__ 65535\n#define __UINT16_C(c) c\n#define __UINT_LEAST32_MAX__ 4294967295U\n#define __UINT32_C(c) c ## U\n#define __UINT_LEAST64_MAX__ 18446744073709551615UL\n#define __UINT64_C(c) c ## UL\n#define __INT_FAST8_MAX__ 127\n#define __INT_FAST16_MAX__ 9223372036854775807L\n#define __INT_FAST32_MAX__ 9223372036854775807L\n#define __INT_FAST64_MAX__ 9223372036854775807L\n#define __UINT_FAST8_MAX__ 255\n#define __UINT_FAST16_MAX__ 18446744073709551615UL\n#define __UINT_FAST32_MAX__ 18446744073709551615UL\n#define __UINT_FAST64_MAX__ 18446744073709551615UL\n#define __INTPTR_MAX__ 9223372036854775807L\n#define __UINTPTR_MAX__ 18446744073709551615UL\n#define __FLT_EVAL_METHOD__ 0\n#define __DEC_EVAL_METHOD__ 2\n#define __FLT_RADIX__ 2\n#define __FLT_MANT_DIG__ 24\n#define __FLT_DIG__ 6\n#define __FLT_MIN_EXP__ (-125)\n#define __FLT_MIN_10_EXP__ (-37)\n#define __FLT_MAX_EXP__ 128\n#define __FLT_MAX_10_EXP__ 38\n#define __FLT_DECIMAL_DIG__ 9\n#define __FLT_MAX__ 3.40282346638528859812e+38F\n#define __FLT_MIN__ 1.17549435082228750797e-38F\n#define __FLT_EPSILON__ 1.19209289550781250000e-7F\n#define __FLT_DENORM_MIN__ 1.40129846432481707092e-45F\n#define __FLT_HAS_DENORM__ 1\n#define __FLT_HAS_INFINITY__ 1\n#define __FLT_HAS_QUIET_NAN__ 1\n#define __DBL_MANT_DIG__ 53\n#define __DBL_DIG__ 15\n#define __DBL_MIN_EXP__ (-1021)\n#define __DBL_MIN_10_EXP__ (-307)\n#define __DBL_MAX_EXP__ 1024\n#define __DBL_MAX_10_EXP__ 308\n#define __DBL_DECIMAL_DIG__ 17\n#define __DBL_MAX__ double(1.79769313486231570815e+308L)\n#define __DBL_MIN__ double(2.22507385850720138309e-308L)\n#define __DBL_EPSILON__ double(2.22044604925031308085e-16L)\n#define __DBL_DENORM_MIN__ double(4.94065645841246544177e-324L)\n#define __DBL_HAS_DENORM__ 1\n#define __DBL_HAS_INFINITY__ 1\n#define __DBL_HAS_QUIET_NAN__ 1\n#define __LDBL_MANT_DIG__ 64\n#define __LDBL_DIG__ 18\n#define __LDBL_MIN_EXP__ (-16381)\n#define __LDBL_MIN_10_EXP__ (-4931)\n#define __LDBL_MAX_EXP__ 16384\n#define __LDBL_MAX_10_EXP__ 4932\n#define __DECIMAL_DIG__ 21\n#define __LDBL_MAX__ 1.18973149535723176502e+4932L\n#define __LDBL_MIN__ 3.36210314311209350626e-4932L\n#define __LDBL_EPSILON__ 1.08420217248550443401e-19L\n#define __LDBL_DENORM_MIN__ 3.64519953188247460253e-4951L\n#define __LDBL_HAS_DENORM__ 1\n#define __LDBL_HAS_INFINITY__ 1\n#define __LDBL_HAS_QUIET_NAN__ 1\n#define __DEC32_MANT_DIG__ 7\n#define __DEC32_MIN_EXP__ (-94)\n#define __DEC32_MAX_EXP__ 97\n#define __DEC32_MIN__ 1E-95DF\n#define __DEC32_MAX__ 9.999999E96DF\n#define __DEC32_EPSILON__ 1E-6DF\n#define __DEC32_SUBNORMAL_MIN__ 0.000001E-95DF\n#define __DEC64_MANT_DIG__ 16\n#define __DEC64_MIN_EXP__ (-382)\n#define __DEC64_MAX_EXP__ 385\n#define __DEC64_MIN__ 1E-383DD\n#define __DEC64_MAX__ 9.999999999999999E384DD\n#define __DEC64_EPSILON__ 1E-15DD\n#define __DEC64_SUBNORMAL_MIN__ 0.000000000000001E-383DD\n#define __DEC128_MANT_DIG__ 34\n#define __DEC128_MIN_EXP__ (-6142)\n#define __DEC128_MAX_EXP__ 6145\n#define __DEC128_MIN__ 1E-6143DL\n#define __DEC128_MAX__ 9.999999999999999999999999999999999E6144DL\n#define __DEC128_EPSILON__ 1E-33DL\n#define __DEC128_SUBNORMAL_MIN__ 0.000000000000000000000000000000001E-6143DL\n#define __REGISTER_PREFIX__ \n#define __USER_LABEL_PREFIX__ \n#define __GNUC_GNU_INLINE__ 1\n#define __NO_INLINE__ 1\n#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_1 1\n#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_2 1\n#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 1\n#define __GCC_HAVE_SYNC_COMPARE_AND_SWAP_8 1\n#define __GCC_ATOMIC_BOOL_LOCK_FREE 2\n#define __GCC_ATOMIC_CHAR_LOCK_FREE 2\n#define __GCC_ATOMIC_CHAR16_T_LOCK_FREE 2\n#define __GCC_ATOMIC_CHAR32_T_LOCK_FREE 2\n#define __GCC_ATOMIC_WCHAR_T_LOCK_FREE 2\n#define __GCC_ATOMIC_SHORT_LOCK_FREE 2\n#define __GCC_ATOMIC_INT_LOCK_FREE 2\n#define __GCC_ATOMIC_LONG_LOCK_FREE 2\n#define __GCC_ATOMIC_LLONG_LOCK_FREE 2\n#define __GCC_ATOMIC_TEST_AND_SET_TRUEVAL 1\n#define __GCC_ATOMIC_POINTER_LOCK_FREE 2\n#define __GCC_HAVE_DWARF2_CFI_ASM 1\n#define __PRAGMA_REDEFINE_EXTNAME 1\n#define __SSP__ 1\n#define __SIZEOF_INT128__ 16\n#define __SIZEOF_WCHAR_T__ 4\n#define __SIZEOF_WINT_T__ 4\n#define __SIZEOF_PTRDIFF_T__ 8\n#define __amd64 1\n#define __amd64__ 1\n#define __x86_64 1\n#define __x86_64__ 1\n#define __ATOMIC_HLE_ACQUIRE 65536\n#define __ATOMIC_HLE_RELEASE 131072\n#define __k8 1\n#define __k8__ 1\n#define __code_model_small__ 1\n#define __MMX__ 1\n#define __SSE__ 1\n#define __SSE2__ 1\n#define __FXSR__ 1\n#define __SSE_MATH__ 1\n#define __SSE2_MATH__ 1\n#define __gnu_linux__ 1\n#define __linux 1\n#define __linux__ 1\n#define linux 1\n#define __unix 1\n#define __unix__ 1\n#define unix 1\n#define __ELF__ 1\n#define __DECIMAL_BID_FORMAT__ 1\n# 1 \"<command-line>\"\n#define _GNU_SOURCE 1\n# 1 \"/usr/include/stdc-predef.h\" 1 3 4\n# 19 \"/usr/include/stdc-predef.h\" 3 4\n#define _STDC_PREDEF_H 1\n# 41 \"/usr/include/stdc-predef.h\" 3 4\n#define __STDC_IEC_559__ 1\n\n\n\n\n\n\n\n#define __STDC_IEC_559_COMPLEX__ 1\n\n\n\n\n#define __STDC_ISO_10646__ 201103L\n\n\n#define __STDC_NO_THREADS__ 1\n# 1 \"<command-line>\" 2\n# 1 \"dummy\"\n";
cmakeVars["_includeLines"]="/usr/include/c++/4.8\n; /usr/include/x86_64-linux-gnu/c++/4.8\n; /usr/include/c++/4.8/backward\n; /usr/lib/gcc/x86_64-linux-gnu/4.8/include\n; /usr/local/include\n; /usr/lib/gcc/x86_64-linux-gnu/4.8/include-fixed\n; /usr/include/x86_64-linux-gnu\n; /usr/include\n";
cmakeVars["_includePath"]="/usr/include";
cmakeVars["_name"]="__STDC_NO_THREADS__";
cmakeVars["_orig_lang"]="de_DE.UTF-8";
cmakeVars["_update_type"]="git";
cmakeVars["_value"]="1";
cmakeVars["f"]="";
cmakeVars["l"]="";
cmakeVars["mode"]="";
cmakeVars["nextLine"]="";
cmakeVars["nextLineNoFramework"]=" /usr/include\n";
cmakeVars["p"]="";
cmakeVars["testtype"]="";
cmakeVars["type"]="";
cmakeVars["val"]="0";
cmakeVars["var"]="INSTALL_CMAKE_DIR";

        return cmakeVars[varName];
    }
}

/**
 * The class OutputConfiguration redirects the output of all
 * testcases into files located in /home/SMBAD/schnell/home/mmmtools as well as onto the console.
 */
struct OutputConfiguration
{
    /**
     * Setup for writing the output to file and redirect it to stout.
     */
    OutputConfiguration()
    {
        // The path where the tests are put into is taken from CMake
        std::string logFileName("/home/SMBAD/schnell/home/mmmtools/testing//");
        logFileName.append(boost::unit_test::framework::master_test_suite().p_name);
        logFileName.append(".xml");
        logFile.open(logFileName.c_str());

        tee = new testout::ostream_tee_device(std::cout, logFile);
        teeStream = new testout::tee_ostream(*tee);

        boost::unit_test::unit_test_log.set_stream(*teeStream);
    }

    /**
     * Flush the stream and append a tag missing from boost::unit_test framework.
     * Afterwards reset the output to stout.
     */
    ~OutputConfiguration()
    {
        // this line is required to write the final "</TestLog>" tag
        // after the testrun is complete
        boost::unit_test::unit_test_log.test_finish();
        boost::unit_test::unit_test_log.set_stream(std::cout);

        teeStream->flush();

        delete tee;
        delete teeStream;

        logFile.close();
    }
    /**
     * Tee device for redirecting the console output to a file specified in OutputConfiguration::logFile.
     */
    testout::ostream_tee_device* tee;
    /**
     * Tee device for redirecting the output of OutputConfiguration::tee to stdout.
     */
    testout::tee_ostream* teeStream;
    /**
     * Path to the output file into which the test results get written.
     */
    std::ofstream logFile;
};

BOOST_GLOBAL_FIXTURE(OutputConfiguration);

#endif

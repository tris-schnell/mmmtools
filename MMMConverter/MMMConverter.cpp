

#include <VirtualRobot/RuntimeEnvironment.h>

#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>
#include <string>
#include <map>
#include <iostream>
#include <fstream>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionReaderC3D.h>
#include <MMM/Model/ModelReaderXML.h>
#include <MMM/Model/ModelProcessor.h>
#include <MMM/Model/ModelProcessorFactory.h>
#include <MMM/ConverterFactory.h>

#include "MMMConverterConfiguration.h"

using std::cout;
using std::endl;
using namespace VirtualRobot;

MMM::ModelPtr readModel(std::string f)
{
	MMM::ModelReaderXMLPtr r(new MMM::ModelReaderXML());
	MMM::ModelPtr model = r->loadModel(f);

	return model;
}

MMM::MarkerMotionPtr readViconData(std::string f, std::string MarkerPrefix)
{
	MMM::MotionReaderC3DPtr r(new MMM::MotionReaderC3D());
    r->markerPrefix = MarkerPrefix;
    MMM::MarkerMotionPtr markerMotion = r->loadC3D(f);
	return markerMotion;
}


MMM::MotionPtr readMMMData(std::string f)
{
	MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
	MMM::MotionPtr mmmMotion = r->loadMotion(f);
	return mmmMotion;
}


MMM::ModelProcessorPtr setupModelProcessor(std::string modelProcessorType, std::string modelProcessorFile)
{
	MMM::ModelProcessorPtr modelProcessor;
	if (modelProcessorType.empty())
		return modelProcessor;
	cout << "Setting up model processor..." << endl;
	MMM::ModelProcessorFactoryPtr modelFactory = MMM::ModelProcessorFactory::fromName(modelProcessorType, NULL);
	if (!modelFactory)
	{
		cout << "Could not create model processing factory of type " << modelProcessorType << endl;
		cout << "Setting up model processor... Failed..." << endl;
		return modelProcessor;
	}
	modelProcessor = modelFactory->createModelProcessor();
	if (!modelProcessorFile.empty())
	{
		if (!modelProcessor->setupFile(modelProcessorFile))
		{
			cout << "Error while configuring model processor '" << modelProcessorType << "' from file " << modelProcessorFile << endl;
			//cout << "Setting up model processor... Failed..." << endl;
		}
	}
	cout << "Setting up model processor... OK..." << endl;
	return modelProcessor;
}


bool convertViconData(std::string outModelFile, std::string outputMotionName, std::string modelProcessorName, std::string modelprocessorConfigfile, std::string inputDataFile, /*MMM::ConverterFactoryPtr converterFactory,*/ std::string converterName, std::string converterFile, std::string outFile, std::string MarkerPrefix)
{
	MMM::ModelPtr mmmOrigModel = readModel(outModelFile);
	MMM::ModelPtr model = mmmOrigModel;
	if (!model)
	{
		MMM_ERROR << "Could not read model " << outModelFile << endl;
		return false;
	}

	MMM::ModelProcessorPtr modelProcessor = setupModelProcessor(modelProcessorName, modelprocessorConfigfile);
	if (modelProcessor)
	{
		model = modelProcessor->convertModel(mmmOrigModel);
	}

    MMM::MarkerMotionPtr inputMotion = readViconData(inputDataFile, MarkerPrefix);
	if (!inputMotion)
	{
		MMM_ERROR << "Could not read vicon motion " << inputDataFile << endl;
		return false;
	}
	
	boost::shared_ptr<MMM::ConverterFactory> converterFactory = MMM::ConverterFactory::fromName(converterName,NULL);
	if (!converterFactory)
	{
		MMM_ERROR << "Converter unknown: " << converterName << endl;
		return false;
	}

	MMM::ConverterPtr converter = converterFactory->createConverter();
	if (!converter)
	{
        MMM_ERROR << "Could not create Converter " << endl;
		return false;
	}
	if (!converterFile.empty())
	{
		if (!converter->setupFile(converterFile))
		{
			cout << "Error while configuring converter '" << converterName << "' from file " << converterFile << endl;
			cout << "Setting up Converter... Failed..." << endl;
		}
	}
	converter->setup(MMM::ModelPtr(), inputMotion, model);

	MMM::AbstractMotionPtr outputMotion = converter->convertMotion();
	if (!outputMotion)
	{
        MMM_ERROR << "Could not convert motion with converter" << endl;
		return false;
	}

	outFile = MMM::XML::getAbsoluteFile(outFile);
    cout << "Storing converted motion to " << outFile << endl;

	// setup used data, so that it shows up in the XML
	MMM::MotionPtr outputMotionCasted = boost::dynamic_pointer_cast<MMM::Motion>(outputMotion);
	if (outputMotionCasted)
	{
		if (modelProcessor)
			outputMotionCasted->setModelProcessor(modelProcessor);
		if (model)
			outputMotionCasted->setModel(model, mmmOrigModel);
		if (!outputMotionName.empty())
			outputMotionCasted->setName(outputMotionName);

		std::string filePath = MMM::XML::getPath(outFile);
		outputMotionCasted->setMotionFilePath(filePath);
	}

	std::string motionXML = outputMotion->toXML();

	return MMM::XML::saveXML(outFile, motionXML);
}


bool convertViconData(MMMConverterConfiguration &c)
{
    return convertViconData(c.outModelFile, c.outputMotionName, c.modelProcessorName, c.modelProcessorConfigFile, c.viconInputFile, c.converterName, c.converterConfigFile, c.outFile, c.markerPrefix);
}


bool convertMMMData(std::string outModelFile, std::string modelPorcessorName, std::string modelProcessorConfigFile, std::string inputDataFile, std::string converterName, std::string converterConfigFile, std::string outFile, std::string inModelFile)
{
	MMM::ModelPtr mmmOrigModel = readModel(inModelFile);
	MMM::ModelPtr outOrigModel = readModel(outModelFile);
	MMM::ModelPtr inputModel = mmmOrigModel;
	MMM::ModelPtr outputModel = outOrigModel; //for now, no out model processors are supported.
	if (!inputModel)
	{
		MMM_ERROR << "Could not read model " << inModelFile << endl;
		return false;
	}

	MMM::ModelProcessorPtr modelProcessor = setupModelProcessor(modelPorcessorName, modelProcessorConfigFile);
	if (modelProcessor)
	{
		inputModel = modelProcessor->convertModel(mmmOrigModel);
	}

	//for now, no out model processors are supported.
	//	MMM::ModelProcessorPtr outModelProcessor = setupModelProcessor(outModelPorcessorName, outModelProcessorConfigFile);
	//	if (outModelProcessor)
	//	{
	//		outputModel = outModelProcessor->convertModel(outOrigModel);
	//	}

	MMM::MotionPtr inputMotion = readMMMData(inputDataFile);
	if (!inputMotion)
	{
		MMM_ERROR << "Could not read input mmm motion " << inputDataFile << endl;
		return false;
	}

	boost::shared_ptr<MMM::ConverterFactory> converterFactory = MMM::ConverterFactory::fromName(converterName, NULL);
	if (!converterFactory)
	{
		MMM_ERROR << "Converter unknown: " << converterName << endl;
		return false;
	}

	MMM::ConverterPtr converter = converterFactory->createConverter();
	if (!converter)
	{
		MMM_ERROR << "Could not create Converter " << endl;
		return false;
	}

	if (!converterConfigFile.empty())
	{
		if (!converter->setupFile(converterConfigFile))
		{
			cout << "Error while configuring converter '" << converterName << "' from file " << converterConfigFile << endl;
			cout << "Setting up Converter... Failed..." << endl;
		}
	}
	converter->setup(inputModel, inputMotion, outputModel);

	MMM::AbstractMotionPtr outputMotion = converter->convertMotion();
	if (!outputMotion)
	{
		MMM_ERROR << "Could not convert motion with converter" << endl;
		return false;
	}

	std::string motionXML;

	// setup used data, so that it shows up in the XML
	MMM::MotionPtr outputMotionCasted = boost::dynamic_pointer_cast<MMM::Motion>(outputMotion);
	if (outputMotionCasted)
	{
		//if (outModelProcessor)
		//	outputMotionCasted->setModelProcessor(outModelProcessor);
		if (outputModel)
			outputMotionCasted->setModel(outputModel, outOrigModel);
		motionXML = outputMotionCasted->toXML();
	}
	else
		motionXML = outputMotion->toXML();

	return MMM::XML::saveXML(outFile, motionXML);
}


bool convertMMMData(MMMConverterConfiguration &c)
{
	return convertMMMData(c.outModelFile, c.modelProcessorName, c.modelProcessorConfigFile, c.mmmInputFile, c.converterName, c.converterConfigFile, c.outFile, c.inModelFile);
}


// exemplary setup:
// --converter=RigidBodyConverterVicon2MMM --converterConfigFile "C:\Projects\MMM\mmmtools\data\ConverterVicon2MMM_WhiskConfig.xml"  --outputModel "C:\Projects\MMM\mmmtools\data\Model\Objects\Whisk\Whisk.xml" --inputDataVicon "C:\Projects\MMM\mmmtools\data\Motions\Trial12_whisk_lying.c3d" --outputModelProcessor "" --outputModelProcessorConfigFile ""
int main(int argc, char *argv[])
{
	cout << " --- MMM CONVERTER --- " << endl;

    MMMConverterConfiguration c;
    if (!c.processCommandLine(argc,argv))
    {
        MMM_ERROR << "Could not process command line, aborting..." << endl;
        return -1;
    }
    c.print();
    if (c.converterMode == c.eHuman2MMM)
    {
        bool res = convertViconData(c);
        if (res)
            cout << "Successfully converted motion..." << endl;
        else
        {
            cout << "Failed to convert motion..." << endl;
            return -2;
        }
    } else if (c.converterMode == c.eMMM2Robot)
    {
    	bool res = convertMMMData(c);
    	if (res)
    		cout << "Successfully converted motion..." << endl;
    	else
    	{
    		cout << "Failed to convert motion..." << endl;
    		return -3;
    	}
    } else
    {
        MMM_ERROR << "Unknown mode is not yet implemented..." << endl;
        return -4;
    }
	cout << " --- END --- " << endl;
	return 0;
}

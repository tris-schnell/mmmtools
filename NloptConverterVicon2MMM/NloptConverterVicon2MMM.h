/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_NloptConverterVicon2MMM_H_
#define __MMM_NloptConverterVicon2MMM_H_

#include <MMM/MarkerBasedConverter.h>

#include "NloptConverterVicon2MMMImportExport.h"

namespace MMM
{

/*!
	\brief A standard converter for converting Vicon marker motions to the MMM format.
*/
class NloptConverterVicon2MMM_IMPORT_EXPORT NloptConverterVicon2MMM : public MarkerBasedConverter
{
public:
    NloptConverterVicon2MMM(const std::string &name = "NloptConverterVicon2MMM");

    virtual AbstractMotionPtr convertMotion();

    virtual AbstractMotionPtr initializeStepwiseConvertion();

    virtual bool convertMotionStep(AbstractMotionPtr currentOutput, bool increment=true);

protected:
    virtual bool _setup(rapidxml::xml_node<char>* rootTag);

    /* NloptConverterVicon2MMM(const std::string &name = "NloptConverterVicon2MMM");

    virtual bool isInitialized();

	Basic setup of a converter
	\param inputModel Specifies to which model the inputMotion is linked. In case MarkerMotions are used as input (e.g. Vicon->MMM) the input model can be empty.
	\param inputMotion The motion to convert.
	\param outputModel Setup the target model.
    \return true on success.
	virtual bool setup(ModelPtr inputModel, AbstractMotionPtr inputMotion, ModelPtr outputModel);


	virtual AbstractMotionPtr convertMotion();

	virtual AbstractMotionPtr initializeStepwiseConvertion();

    virtual bool convertMotionStep(AbstractMotionPtr currentOutput);

    virtual bool convertMotionStep(AbstractMotionPtr currentOutput, bool increment);

protected:
    //! first tag in xml configuration
    virtual bool _setup(rapidxml::xml_node<char>* rootTag);

    virtual bool buildModel(MMM::ModelPtr model);

	//! Computes the center of all markers and moves model to this position
	bool moveModelToCenter(MMM::MarkerDataPtr frame);

	//! Computes the average and maximum distance of the markers of the current model position to the given frame f.
	void getDistance(MMM::MarkerDataPtr f, float &maxD, float &avgD);

	//! Fits model to frame f while considering the configurated marker mappings
    bool fitModel(MMM::MarkerDataPtr f, bool quickImprovementCheck = true, float ikStepSize = 0.2f, float ikMinChange = 0.1f, int ikSteps = 10, bool performMinOneStep = true, bool boxConstraints = true);
	
	//! Searches best intial rotation of model at curent position (assuming z == upright). Considers markermapping for distance computation.
	bool findBestModelRotation(MMM::MarkerDataPtr f, int nrRotationsToCheck = 10);

	VirtualRobot::RobotPtr mmmModel; // the simox model 
	VirtualRobot::RobotNodeSetPtr rns; // the joints to manipulate
    std::map<std::string, VirtualRobot::SensorPtr> modelMarkers; // the markers on the mmm model

    RobotPoseDifferentialIKPtr ik;
public:
	float paramIKStepSize;
	float paramIKMinChange;
	int paramIKSteps;
    bool paramCheckImprovement;
    bool paramPerfomMinOneStep;
    bool paramJointLimitsBoxConstraints;
	float paramInitialIKStepSize;
    float paramInitialIKMinChange;
    int paramInitialIKSteps;
    bool paramInitialCheckImprovement;
    bool paramInitialPerfomMinOneStep;
    bool paramInitialJointLimitsBoxConstraints;

	std::string modelName;
    Eigen::MatrixXf _jacobian; */
};

typedef boost::shared_ptr<NloptConverterVicon2MMM> NloptConverterVicon2MMMPtr;

}

#endif 

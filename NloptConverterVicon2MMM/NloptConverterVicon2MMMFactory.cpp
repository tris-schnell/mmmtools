/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <boost/extension/extension.hpp>

#include "NloptConverterVicon2MMMFactory.h"
#include "NloptConverterVicon2MMM.h"

namespace MMM
{

// register this factory
ConverterFactory::SubClassRegistry NloptConverterVicon2MMMFactory::registry(NloptConverterVicon2MMMFactory::getName(), &NloptConverterVicon2MMMFactory::createInstance);

NloptConverterVicon2MMMFactory::NloptConverterVicon2MMMFactory()
    : ConverterFactory()
{}

NloptConverterVicon2MMMFactory::~NloptConverterVicon2MMMFactory()
{}

ConverterPtr NloptConverterVicon2MMMFactory::createConverter()
{
    ConverterPtr converter(new NloptConverterVicon2MMM(getName()));
    return converter;
}

std::string NloptConverterVicon2MMMFactory::getName()
{
    return "NloptConverterVicon2MMM";
}

boost::shared_ptr<ConverterFactory> NloptConverterVicon2MMMFactory::createInstance(void*)
{
    boost::shared_ptr<ConverterFactory> converterFactory(new NloptConverterVicon2MMMFactory());
    return converterFactory;
}

}
#ifdef WIN32
#pragma warning(push)
#pragma warning(disable: 4190) // C-linkage warning can be ignored in our case
#endif

extern "C"
BOOST_EXTENSION_EXPORT_DECL MMM::ConverterFactoryPtr getFactory() {
    MMM::NloptConverterVicon2MMMFactoryPtr f(new MMM::NloptConverterVicon2MMMFactory());
    return f;
}
#ifdef WIN32
#pragma warning(pop)
#endif

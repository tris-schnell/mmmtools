/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _NloptConverterVicon2MMMImportExport_h
#define _NloptConverterVicon2MMMImportExport_h

namespace MMM
{
#ifdef WIN32
#  pragma warning ( disable : 4251 )
#  if defined(ConverterVicon2MMM_EXPORTS)
#    define NloptConverterVicon2MMM_IMPORT_EXPORT __declspec(dllexport)
#  else
#    define NloptConverterVicon2MMM_IMPORT_EXPORT __declspec(dllimport)
#  endif
#else
#  define NloptConverterVicon2MMM_IMPORT_EXPORT
#endif
}

#endif

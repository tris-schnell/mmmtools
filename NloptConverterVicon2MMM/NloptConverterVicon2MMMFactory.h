/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef _MMM_NloptConverterVicon2MMMFactory_h_
#define _MMM_NloptConverterVicon2MMMFactory_h_

// TODO: Cleanup includes
#include <MMM/AbstractFactoryMethod.h>
#include <MMM/MMMCore.h>
#include <MMM/MMMImportExport.h>
#include <MMM/ConverterFactory.h>
#include <boost/shared_ptr.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <string>

#include "NloptConverterVicon2MMMImportExport.h"

namespace MMM
{

/*!
    A factory to create a NloptConverterVicon2MMM converter.
*/
class NloptConverterVicon2MMM_IMPORT_EXPORT NloptConverterVicon2MMMFactory : public ConverterFactory
{
public:
    NloptConverterVicon2MMMFactory();
    virtual ~NloptConverterVicon2MMMFactory();

    ConverterPtr createConverter();

    static std::string getName();

    static boost::shared_ptr<ConverterFactory> createInstance(void*);

private:
    static SubClassRegistry registry;
};

typedef boost::shared_ptr<NloptConverterVicon2MMMFactory> NloptConverterVicon2MMMFactoryPtr;
}

#endif

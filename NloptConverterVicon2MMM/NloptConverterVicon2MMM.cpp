/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include "NloptConverterVicon2MMM.h"

/* #include "ConverterVicon2MMMFactory.h"
#include <VirtualRobot/MathTools.h>
#include <MMM/Motion/Motion.h>
#include <MMM/rapidxml.hpp>
#include <MMMSimoxTools/MMMSimoxTools.h> */

using namespace MMM;

NloptConverterVicon2MMM::NloptConverterVicon2MMM(const std::string &name) :
    MarkerBasedConverter(name) {}

AbstractMotionPtr NloptConverterVicon2MMM::convertMotion() {
    // ...
}

AbstractMotionPtr NloptConverterVicon2MMM::initializeStepwiseConvertion() {
    // ...
}

bool NloptConverterVicon2MMM::convertMotionStep(AbstractMotionPtr currentOutput, bool increment) {
    // ...
}

bool NloptConverterVicon2MMM::_setup(rapidxml::xml_node<char>* rootTag) {
    // ...
}

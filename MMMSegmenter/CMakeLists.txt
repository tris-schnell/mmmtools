PROJECT ( MMMSegmenter )

MESSAGE (STATUS "\n***************** Configuring ${PROJECT_NAME} *****************")

CMAKE_MINIMUM_REQUIRED(VERSION 2.6.4)
CMAKE_POLICY(VERSION 2.6)

#set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/CMakeModules/")

# search for MMMCore
FIND_PACKAGE(MMMCore REQUIRED)

# search for simox
FIND_PACKAGE(Simox REQUIRED)

FIND_LIBRARY(GSLWRAP_LIB gslwrap ${PROJECT_SOURCE_DIR}/extern/gslwrap-0.2/gslwrap)
INCLUDE_DIRECTORIES(/usr/include/gsl)
INCLUDE_DIRECTORIES(/usr/include/eigen3)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/../../simox)
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/extern/gslwrap-0.2)
LINK_DIRECTORIES(${PROJECT_SOURCE_DIR}/extern/gslwrap-0.2/gslwrap)
# search
#find_package(Qt4 COMPONENTS QtCore QtGui QtDesigner)
#find_package(Coin3D REQUIRED)
#find_package(SoQt REQUIRED)
#find_package(Boost REQUIRED unit_test_framework)

IF(GSLWRAP_LIB_FOUND AND MMMCore_FOUND AND Simox_USE_COIN_VISUALIZATION)

    ADD_DEFINITIONS(-DMMMTools_LIB_DIR="${MMMTools_LIB_DIR}")

    INCLUDE_DIRECTORIES(${MMMCORE_INCLUDE_DIRS})
    INCLUDE_DIRECTORIES("${PROJECT_SOURCE_DIR}/../3rdParty")
    INCLUDE_DIRECTORIES("${PROJECT_SOURCE_DIR}/../MMMSimoxTools")
    
#	FILE(GLOB MMMSEGMENTER_SRCS 
#	    ${PROJECT_SOURCE_DIR}/MMMSegmenter.cpp 
#	    ${PROJECT_SOURCE_DIR}/MatrixOperation.cpp
#	    ${PROJECT_SOURCE_DIR}/PCA.cpp
#	    )
	
#	FILE(GLOB MMMSEGMENTER_INCS 
#	    ${PROJECT_SOURCE_DIR}/MatrixOperation.h
#	    ${PROJECT_SOURCE_DIR}/PCA.h
 #	    )
SET(MMMSEGMENTER_SRCS 
	    ${PROJECT_SOURCE_DIR}/MMMSegmenter.cpp 
	    ${PROJECT_SOURCE_DIR}/MatrixOperation.cpp
	    ${PROJECT_SOURCE_DIR}/PCA.cpp
	    )
	
	SET(MMMSEGMENTER_INCS 
	    ${PROJECT_SOURCE_DIR}/MatrixOperation.h
	    ${PROJECT_SOURCE_DIR}/PCA.h
 	    )

        ########## OS specific setup (including rpath)    
    IF(UNIX)
        # We are on Linux
        IF(CMAKE_SYSTEM_PROCESSOR MATCHES "x86_64")
    	    ADD_DEFINITIONS(-fPIC)
        ENDIF(CMAKE_SYSTEM_PROCESSOR MATCHES "x86_64")
        MESSAGE (STATUS " * BUILDTYPE: ${CMAKE_BUILD_TYPE}")
        IF(NOT DEFINED CMAKE_BUILD_TYPE OR ${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    	    MESSAGE(STATUS " * Configuring Debug build")
    	    ADD_DEFINITIONS(-D_DEBUG) # -Wall -W -Werror -pedantic)
        ELSE()
    	    MESSAGE(STATUS " * Configuring Release build")
        ENDIF()
    	
        # use, i.e. don't skip the full RPATH for the build tree
        SET(CMAKE_SKIP_BUILD_RPATH  FALSE)
    
        # when building, don't use the install RPATH already
        # (but later on when installing)
        SET(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE) 
       
        # add the automatically determined parts of the RPATH
        # which point to directories outside the build tree to the install RPATH
        SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
    
        # the RPATH to be used when installing, but only if it's not a system directory
        LIST(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${INSTALL_LIB_DIR}" isSystemDir)
        IF("${isSystemDir}" STREQUAL "-1")
		    #MESSAGE("INSTALL RPATH MMMSimoxToolsLibDir: ${INSTALL_LIB_DIR}" )
    	    SET(CMAKE_INSTALL_RPATH "${INSTALL_LIB_DIR}")
        ENDIF("${isSystemDir}" STREQUAL "-1")
	    SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_RPATH}" "${MMMCore_LIB_DIRS}")
	    #MESSAGE ("CMAKE_INSTALL_RPATH: ${CMAKE_INSTALL_RPATH}")
    ELSE(UNIX)
        # We are on Windows
        ADD_DEFINITIONS(-D_CRT_SECURE_NO_WARNINGS)
    	
        # On MSVC we compile with /MP flag (use multiple threads)
        IF(MSVC)
    	    ADD_DEFINITIONS(/MP)
        ENDIF(MSVC)
    ENDIF(UNIX)
                     
    # create the executable
#    add_executable(${PROJECT_NAME} "${MMMSEGMENTER_SRCS}" "${MMMSEGMENTER_INCS}")
 add_executable(${PROJECT_NAME} ${PROJECT_SOURCE_DIR}/MMMSegmenter.cpp 
	    ${PROJECT_SOURCE_DIR}/MatrixOperation.cpp
	    ${PROJECT_SOURCE_DIR}/PCA.cpp
	    ${PROJECT_SOURCE_DIR}/MatrixOperation.h
	    ${PROJECT_SOURCE_DIR}/PCA.h)

    TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${VirtualRobot_LIBRARIES} ${MMMCORE_LIBRARIES} MMMSimoxTools gsl gslcblas gslwrap)
    
    SET (MMMSegmenterLibDir ${CMAKE_BINARY_DIR}/lib)
    SET (MMMSegmenterBinDir ${CMAKE_BINARY_DIR}/bin)

ELSE()
    MESSAGE ("GSLWRAP or MMMCore or Simox not found") 
ENDIF()

MESSAGE (STATUS "***************** Finished Configuring ${PROJECT_NAME} *****************\n")

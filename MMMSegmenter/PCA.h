/*
 * PCA.h
 *
 *  Created on: 10.11.2009
 *  Author: 	Martin Do
 *	Copyright: 	Martin Do, Chair Prof. Dillmann (IAIM),
 *            	Institute for Anthropomatics (IFA),
 *		        Karlsruhe Institute of Technology (KIT). All rights reserved.
 *
 *	Changes:
 *
 *	Description:
 */


#ifndef PCA_H_
#define PCA_H_

#include "MatrixOperation.h"
#include <vector>

class CPCA
{
public:
	CPCA();
	~CPCA();
	void computePCA_Cov(std::vector<gsl::vector> dataSet, int nReducedDimensions);
	std::vector<gsl::vector> reduceDim(std::vector<gsl::vector> dataSet);
	std::vector<gsl::vector> restoreDim(std::vector<gsl::vector> dataSet);
private:
	matrix mProjectionMatrix;
	matrix mCovMatrix;
	gsl::vector mRowMeans;
};

#endif /* PCA_H_ */

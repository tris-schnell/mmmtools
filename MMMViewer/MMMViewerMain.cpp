
#include <VirtualRobot/VirtualRobotException.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/RuntimeEnvironment.h>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/Qt/SoQt.h>
#include <boost/shared_ptr.hpp>
#include <string>
#include <iostream>

using std::cout;
using std::endl;
using namespace VirtualRobot;

#include <Eigen/Core>
#include <Eigen/Geometry>
#include "MMMViewerWindow.h"
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionReaderC3D.h>
#include "MMMViewerConfiguration.h"

int main(int argc, char *argv[])
{
	SoDB::init();
	SoQt::init(argc,argv,"MMMViewer");

    cout << " --- MMMViewer --- " << endl;
    if ((argc==2) && (std::string(argv[1]).compare("--help") == 0) )
    {
        std::cout << "Use the following syntax: " << std::endl;
        std::cout << "MMMConverterGUI --<parameter> <value>" << std::endl
                  << "where" << std::endl
                  << "<parameter> \t <value>" << std::endl \
                  << "motion \t\tXML file containing the MMM motion" << std::endl;
        return 0;
    }

    MMMViewerConfiguration c;
    if (!c.processCommandLine(argc,argv))
    {
        cout << "Error while processing command line, aborting..." << endl;
        return -1;
    }

	MMMViewerWindow mgw(c.motionFile);

	mgw.main();

	cout << " --- END --- " << endl;
	return 0;
}

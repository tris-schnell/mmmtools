#ifndef __MMMViewer_WINDOW_H_
#define __MMMViewer_WINDOW_H_

#include "ui_MMMViewer.h"

#include <VirtualRobot/VirtualRobotCommon.h>
#include <string>
#include <Inventor/sensors/SoSensor.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSwitch.h>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>


#ifndef Q_MOC_RUN
#endif
#include <MMM/MMMCore.h>
#include <MMM/Motion/MotionReaderC3D.h>

// workaround for qt moc, causing problems with boost under visual studio
#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Model/ModelProcessorFactory.h>
#include <MMM/Model/ModelProcessor.h>
#endif

//qtable
#include <QStandardItem>
#include <QListWidgetItem>

class MMMViewerWindow : public QMainWindow
{
	Q_OBJECT
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	MMMViewerWindow(const std::string &dataFile);

	~MMMViewerWindow();

	/*!< Executes the SoQt mainLoop. You need to call this in order to execute the application. */
	int main();
	void progress();

public slots:

	void quit();
	void redraw();
	void closeEvent(QCloseEvent *event);
	void sliderMoved(int pos);
	void updateVisu();
    void fpsChanged(double newValue);
	

protected:
	void setupUI();
	
	void load();

	//qtable stuff
	void setupTable();
	void updateTable();
	bool checkStateInTable(int row);
	void lockSelectedJointValuesFromTable(const Eigen::VectorXf pose);


	static void timerCB(void * data, SoSensor * sensor);
	void createMotionVisu(VirtualRobot::TrajectoryPtr tr, SoSeparator* addToThisSep);

	void jumpToFrame(int pos);

	// filenames
	std::string dataFile;
	std::string modelFile;
	// Motion and Poses
    MMM::MotionList motions;


	// organizing timer
	SoSensorManager* _pSensorMgr;
	SoTimerSensor* _pSensorTimer;
	int _nLastPosition;

    std::map<std::string, VirtualRobot::RobotNodeSetPtr> robotNodeSets;

	MMM::ModelProcessorFactoryPtr modelFactory;

	// Robot Pointer and Seperator Nodes
    std::map<std::string, VirtualRobot::RobotPtr> robots;
    std::map<std::string, SoSeparator*> robotSeps;
	SoSeparator *sceneSep;
	SoSeparator *motionSep;
	SoSeparator *rootCoordSep;
	SoSeparator *floorSep;
    SoSwitch *swFloor;
	// UI Class and Viewer
	Ui::MainWindowMMMViewer UI;
	SoQtExaminerViewer *viewer;
	bool _bShowValues;
	bool timerSensorAdded;


private slots:
	void on_tableWidget_cellClicked(int row, int column);

	void on_playButton_clicked();
	void on_stopButton_clicked();
};

#endif 

#ifndef __MMMViewerConfiguration_H_
#define __MMMViewerConfiguration_H_

#include <MMM/ConverterFactory.h>

#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <string>
#include <iostream>
#include <VirtualRobot/RuntimeEnvironment.h>

/*!
    Configuration of viewer gui.
    By default some standard parameters are set.
*/
struct MMMViewerConfiguration
{
    //! Initialize with standard parameter set
    MMMViewerConfiguration()
    {
//        motionFile = std::string(MMMVIEWER_BASE_DIR)+std::string("/../data/Motions/WalkingStraightForward07.xml"); //original version
         motionFile = std::string(MMMVIEWER_BASE_DIR)+std::string("/../build/bin/converter_output.xml");

    }

    //! checks for command line parameters and updates configuration accordingly.
    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("motion");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        VirtualRobot::RuntimeEnvironment::addDataPath(MMMVIEWER_BASE_DIR);

        if (VirtualRobot::RuntimeEnvironment::hasValue("motion"))
            motionFile = VirtualRobot::RuntimeEnvironment::getValue("motion");

        if (!VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(motionFile))
        {
            MMM_ERROR << "Could not find MMM motion file" << std::endl;
            return false;
        }
        return true;
    }
    

    void print()
    {
        MMM_INFO << "*** MMMViewer Configuration ***" << std::endl;
        std::cout << "Motion file " << motionFile << std::endl;
    }

    std::string motionFile;
};

#endif //__MMMViewerConfiguration_H_


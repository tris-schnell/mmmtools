#ifndef __MMMInverseDynamicsConfiguration_H_
#define __MMMInverseDynamicsConfiguration_H_

#include <boost/extension/shared_library.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

#include <string>
#include <iostream>
#include <VirtualRobot/RuntimeEnvironment.h>

/*!
    Configuration of MMMInverseDynamics.
    By default some standard parameters are set.
*/
struct MMMInverseDynamicsConfiguration
{
    //! Initialize with standard parameter set
    MMMInverseDynamicsConfiguration()
    {
        motionFile = std::string(MMMINVERSEDYNAMICS_BASE_DIR)+std::string("/../data/Motions/sagittal_flexion_elbow_right_completed.xml");
        model = std::string(MMMINVERSEDYNAMICS_BASE_DIR)+std::string("/../data/Model/Winter/mmm_RightArm.xml");
        modelProcessorConfig = std::string(MMMINVERSEDYNAMICS_BASE_DIR)+std::string("/../data/ModelProcessor_Winter_1.86.xml");
        nodeSetName = "RightArm";
    }

    //! checks for command line parameters and updates configuration accordingly.
    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("motion");
        VirtualRobot::RuntimeEnvironment::considerKey("model");
        VirtualRobot::RuntimeEnvironment::considerKey("modelProcessorConfigFile");
        VirtualRobot::RuntimeEnvironment::considerKey("nodeSetName");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        VirtualRobot::RuntimeEnvironment::addDataPath(MMMINVERSEDYNAMICS_BASE_DIR);

        if (VirtualRobot::RuntimeEnvironment::hasValue("motion"))
            motionFile = VirtualRobot::RuntimeEnvironment::getValue("motion");

        if (VirtualRobot::RuntimeEnvironment::hasValue("model"))
            model = VirtualRobot::RuntimeEnvironment::getValue("model");

        if (VirtualRobot::RuntimeEnvironment::hasValue("modelProcessorConfigFile"))
            modelProcessorConfig = VirtualRobot::RuntimeEnvironment::getValue("modelProcessorConfigFile");

        if(VirtualRobot::RuntimeEnvironment::hasValue("nodeSetName"))
            nodeSetName = VirtualRobot::RuntimeEnvironment::getValue("nodeSetName");

        if (!VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(motionFile))
        {
            MMM_ERROR << "Could not find MMM motion file" << std::endl;
            return false;
        }

        if (!VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(model))
        {
            MMM_ERROR << "Could not find model" << std::endl;
            return false;
        }

        if (!VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(modelProcessorConfig))
        {
            MMM_ERROR << "Could not find Model Processor config file" << std::endl;
            return false;
        }
        return true;
    }


    void print()
    {
        MMM_INFO << "*** MMMInverseDynamics Configuration ***" << std::endl;
        std::cout << "Motion file " << motionFile << std::endl;
        std::cout << "Model "  << model << std::endl;
        std::cout << "Model Processor config" << modelProcessorConfig << std::endl;
    }

    std::string motionFile;
    std::string model;
    std::string modelProcessorConfig;
    std::string nodeSetName;
};

#endif //__MMMInverseDynamicsConfiguration_H_


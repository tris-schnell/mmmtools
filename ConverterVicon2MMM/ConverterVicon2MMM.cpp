#include "ConverterVicon2MMM.h"
#include "ConverterVicon2MMMFactory.h"
#include <VirtualRobot/MathTools.h>
#include <MMM/Motion/Motion.h>
#include <MMM/rapidxml.hpp>
#include <MMMSimoxTools/MMMSimoxTools.h>

using std::cout;
using std::endl;
using namespace VirtualRobot;

namespace MMM
{
	ConverterVicon2MMM::ConverterVicon2MMM(const std::string &name)
		: MarkerBasedConverter(name)
{
    std::srand(static_cast<unsigned int>(std::time(0)));
    paramIKStepSize = 0.6f;//0.17f;//0.03f;
    paramIKMinChange = 0.0f;
    paramIKSteps = 10;
    paramCheckImprovement = true;
    paramPerfomMinOneStep = false;
    paramJointLimitsBoxConstraints = true;
    paramInitialIKStepSize = 0.8f;//0.17f;//0.03f;
    paramInitialIKMinChange = 0.0f;
    paramInitialIKSteps = 50;
    paramInitialCheckImprovement = false;
    paramInitialPerfomMinOneStep = false;
    paramInitialJointLimitsBoxConstraints = true;
    }


	bool ConverterVicon2MMM::setup(ModelPtr inputModel, AbstractMotionPtr inputMotion, ModelPtr outputModel)
{
	if (!inputMotion || !outputModel)
		return false;
	inputMarkerMotion = boost::dynamic_pointer_cast<MarkerMotion>(inputMotion);
	if (!inputMarkerMotion)
	{
		MMM_ERROR << "Need a MarkerMotion as input..." << endl;
		return false;
	}
	buildModel(outputModel);
	return Converter::setup(inputModel, inputMotion, outputModel);
}

bool ConverterVicon2MMM::buildModel(MMM::ModelPtr model)
{
    mmmModel = MMM::SimoxTools::buildModel(model, false);
	if (!mmmModel)
		return false;

	// update RNS
	if (mmmModel->hasRobotNodeSet("ConverterVicon2MMM"))
		mmmModel->deregisterRobotNodeSet(mmmModel->getRobotNodeSet("ConverterVicon2MMM"));
    rns.reset();
    if (jointOrder.size()>0)
    {
        std::vector<RobotNodePtr> nodes;
    //    nodes.push_back(mmmModel->getRootNode());
        for (size_t i = 0; i < jointOrder.size(); i++)
        {
            if (mmmModel->hasRobotNode(jointOrder[i]))
            {
                nodes.push_back(mmmModel->getRobotNode(jointOrder[i]));
            }
            else
            {
                cout << "Skipping invalid joint name in converter config:" << jointOrder[i] << endl;
            }
        }

        rns = RobotNodeSet::createRobotNodeSet(mmmModel, "ConverterVicon2MMM", nodes, mmmModel->getRootNode(), VirtualRobot::RobotNodePtr(), true);
    } else
        MMM_ERROR << "Could not initialize ConverterVicon2MMM correctly due to missing joint definitions" << endl;
	// store markers
	std::vector<SensorPtr> sensors = mmmModel->getSensors();
	for (size_t i = 0; i < sensors.size(); i++)
	{
		modelMarkers[sensors[i]->getName()] = sensors[i];
	}

	if (modelName.empty())
		mmmModel->setName(mmmModel->getType());
	else
		mmmModel->setName(modelName);
	return true;
}

MMM::AbstractMotionPtr ConverterVicon2MMM::convertMotion()
{
    if (!isInitialized())
    {
        return AbstractMotionPtr();
    }
    AbstractMotionPtr res = initializeStepwiseConvertion();
    while (res->getNumFrames() < inputMarkerMotion->getNumFrames())
    {
        if (!convertMotionStep(res))
        {
            MMM_ERROR << "Failed to convert motion step " << res->getNumFrames() << endl;
            return res;
        }
		// hack to stop earlier:
		//if (res->getNumFrames() >= 100)
		//	return res;
    }
    return res;
}


bool ConverterVicon2MMM::findBestModelRotation(MMM::MarkerDataPtr f, int nrRotationsToCheck)
{
	if (nrRotationsToCheck <= 0)
		return false;

	float maxD, avgD;
	getDistance(f, maxD, avgD);

	float bestD = avgD;

	// rotate around z axis
	Eigen::Vector3f z(0.0f, 0.0f, 1.0f);

	Eigen::Matrix4f newGP = mmmModel->getGlobalPose();
	Eigen::Matrix4f startGP = mmmModel->getGlobalPose();
	Eigen::Matrix4f bestGP = mmmModel->getGlobalPose();
	for (int i = 0; i < nrRotationsToCheck; i++)
	{
		float currentStep = (float)M_PI * 2.0f / (float)nrRotationsToCheck * (float)i;
		//cout << "Rot:" << currentStep << endl;
		Eigen::Matrix4f rotMat = MathTools::axisangle2eigen4f(z, currentStep);
		newGP = startGP*rotMat;
		mmmModel->setGlobalPose(newGP);
		getDistance(f, maxD, avgD);
        //cout << "maxD:" << maxD << ",avgD:" << avgD << endl;
		if (avgD < bestD)
		{
			bestD = avgD;
			bestGP = newGP;
		}
	}

	//cout << "Min avg marker distance=" << bestD << endl;
	mmmModel->setGlobalPose(bestGP);
	return true;
}

bool ConverterVicon2MMM::fitModel(MMM::MarkerDataPtr f, bool quickImprovementCheck, float ikStepSize, float ikMinChange, int ikSteps, bool performMinOneStep, bool boxConstraints)
{
	if (!mmmModel || !rns || !inputMarkerMotion)
		return false;

    //float maxD, avgD;
	//getDistance(f, maxD, avgD);
	//cout << "IK start" << endl;
	//cout << "Dist Avg: " << avgD << endl;
	//cout << "Dist Max: " << maxD << endl;

    // update goals for ik solver
    Eigen::Matrix4f goalPose = Eigen::Matrix4f::Identity();
    float tolRot = float(3.0f/180.0f * M_PI);
	std::map<std::string, std::string>::iterator it = markerMapping.begin();
	while (it != markerMapping.end())
	{
		std::string c3dMarker = it->first;
		std::string mmmMarker = it->second;

		if (f->data.find(c3dMarker) == f->data.end())
		{
            MMM_ERROR << "c3d marker " << c3dMarker << " not present?!" << endl;
			it++;
			continue;
		}
		if (modelMarkers.find(mmmMarker) == modelMarkers.end())
		{
            MMM_ERROR << "mmm marker " << mmmMarker << " not present?!" << endl;
			it++;
			continue;
		}

		//Eigen::Vector3f posC3D = f->data[c3dMarker];
		//Eigen::Vector3f posMMM = modelMarkers[mmmMarker]->getGlobalPose().block(0, 3, 3, 1);
		goalPose.block(0, 3, 3, 1) = f->data[c3dMarker];
        ik->setGoal(goalPose, modelMarkers[mmmMarker], VirtualRobot::IKSolver::Position, 5.0f, tolRot, false);
        it++;
	}

#ifdef _DEBUG
    ik->setVerbose(true);
#endif
    ik->checkImprovements(quickImprovementCheck);
    ik->boxConstraints(boxConstraints);
    ik->computeSteps(ikStepSize, ikMinChange, ikSteps, performMinOneStep);

    _jacobian = ik->getJacobianMatrix();
    //std::cout << "\tJacobian rows/columns: [" << _jacobian.rows() << "|" << _jacobian.cols() << "]" << std::endl;
    //std::cout << "\tRobotNodeSet size : " << rns->getSize() << std::endl;
    //std::cout << "\tMarkerMapping size: " << markerMapping.size() << std::endl;

#ifdef CONVERTER_OUTPUT_MARKER_DEVIATION
    float maxD, avgD;
    getDistance(f, maxD, avgD);
    std::cout << "Frame #" << f->frame << " finished: max error = " << maxD << ", avg error = " << avgD << std::endl;
#endif

    return true;
}


bool ConverterVicon2MMM::moveModelToCenter(MMM::MarkerDataPtr frame)
{
	if (!isInitialized() || !frame)
		return false;
	//MMM::MarkerDataPtr markerData = inputMarkerMotion->getFrame(currentFrame);

	std::map<std::string, Eigen::Vector3f> data = frame->data;
	if (data.size() == 0)
		return false;
	Eigen::Vector3f avgPos(0.0f, 0.0f, 0.0f);
    /*
	for (std::map<std::string, Eigen::Vector3f>::iterator it = data.begin(); it != data.end(); it++)
	{
		avgPos += it->second;
    } 
	avgPos = 1.0f / (float)data.size() * avgPos;
    */

    int nrMarkers = 0;
    std::map<std::string, std::string>::iterator it = markerMapping.begin();
    while (it != markerMapping.end())
    {
        std::string c3dMarker = it->first;

        if (data.find(c3dMarker) != data.end())
        {
            avgPos += data[c3dMarker];
            nrMarkers++;
        }
        it++;
    }
    if (nrMarkers > 0)
        avgPos = 1.0f / (float)nrMarkers * avgPos;
    else
        MMM_WARNING << "Could not find match any marker name of mapping configuration" << endl;
	Eigen::Matrix4f globalPose = Eigen::Matrix4f::Identity();
	globalPose.block(0, 3, 3, 1) = avgPos;
	mmmModel->setGlobalPose(globalPose);
	return true;
}


void ConverterVicon2MMM::getDistance(MMM::MarkerDataPtr f, float &maxD, float &avgD)
{

	if (!isInitialized() || !f)
	{
		avgD = FLT_MAX;
		maxD = FLT_MAX;
		return;
	}

	// compute distance
	avgD = 0.0f;
	maxD = 0.0f;

	std::map<std::string, std::string>::iterator it = markerMapping.begin();
	while (it != markerMapping.end())
	{
		std::string c3dMarker = it->first;
		std::string mmmMarker = it->second;
		if (f->data.find(c3dMarker) == f->data.end())
		{
            //MMM_ERROR << "c3d marker " << c3dMarker << " not present?!" << endl;
			it++;
			continue;
		}
		if (modelMarkers.find(mmmMarker) == modelMarkers.end())
		{
            MMM_ERROR << "mmm marker " << mmmMarker << " not present?!" << endl;
			it++;
			continue;
		}
		Eigen::Vector3f posC3D = f->data[c3dMarker];
		Eigen::Vector3f posMMM = modelMarkers[mmmMarker]->getGlobalPose().block(0, 3, 3, 1);


		float dist = (posC3D - posMMM).norm();
		//cout << " DIST " << c3dMarker << " <-> " << mmmMarker << ": " << dist << endl;

		if (dist > maxD)
			maxD = dist;

		avgD += dist;

		it++;
	}
	avgD /= markerMapping.size();

	//cout << "Max dist:" << maxD << endl;
	//cout << "Avg Dist:" << avgD << endl;
}


MMM::AbstractMotionPtr ConverterVicon2MMM::initializeStepwiseConvertion()
{
    if (!isInitialized())
    {
		MMM_WARNING << "not initialized..." << endl;
        return AbstractMotionPtr();
    }

    // init ik solver
    ik.reset(new RobotPoseDifferentialIK(mmmModel, rns));
    ik->setVerbose(false);
    float tolRot = float(3.0f/180.0f * M_PI);

    std::map<std::string, std::string>::iterator it = markerMapping.begin();
    Eigen::Matrix4f goalPose = Eigen::Matrix4f::Identity(); // init with standard pose, will be updated later
    while (it != markerMapping.end())
    {
        std::string c3dMarker = it->first;
        std::string mmmMarker = it->second;
        /*if (f->data.find(c3dMarker) == f->data.end())
        {
            MMM_ERROR << "c3d marker " << c3dMarker << " not present?!" << endl;
            it++;
            continue;
        }*/
        if (modelMarkers.find(mmmMarker) == modelMarkers.end())
        {
            MMM_ERROR << "mmm marker " << mmmMarker << " not present?!" << endl;
            it++;
            continue;
        }
        ik->setGoal(goalPose, modelMarkers[mmmMarker], VirtualRobot::IKSolver::Position, 5.0f, tolRot, false);
        it++;
    }
    ik->initialize();

    MotionPtr res(new Motion(outputModel->getName()));
    res->setJointOrder(jointOrder);
	MMM::MarkerDataPtr frame = inputMarkerMotion->getFrame(0);
	moveModelToCenter(frame);
	findBestModelRotation(frame);
    // initially we search harder
    fitModel(frame, paramInitialCheckImprovement, paramInitialIKStepSize, paramInitialIKMinChange, paramInitialIKSteps, paramInitialPerfomMinOneStep, paramInitialJointLimitsBoxConstraints);
    initialModelPose = mmmModel->getGlobalPose();
    initialJointValues.clear();
    for (size_t i=0;i<jointOrder.size();i++)
    {
        if (mmmModel->hasRobotNode(jointOrder[i]))
            initialJointValues[jointOrder[i]] = mmmModel->getRobotNode(jointOrder[i])->getJointValue();
        else
            VR_WARNING << "No robot node with name " << jointOrder[i] << endl;
    }

    return res;
}

bool ConverterVicon2MMM::convertMotionStep(AbstractMotionPtr currentOutput)
{
    return convertMotionStep(currentOutput, true);
}

bool ConverterVicon2MMM::convertMotionStep(AbstractMotionPtr currentOutput, bool increment)
{
    if (!isInitialized() || !currentOutput)
    {
        return false;
    }
    if (increment && currentOutput->getNumFrames()>=inputMarkerMotion->getNumFrames())
    {
        MMM_WARNING << "Could not proceed. End of input motion reached..." << endl;
        return false;
    }
    MotionPtr outputMotion = boost::dynamic_pointer_cast<Motion>(currentOutput);
    if (!outputMotion)
    {
        MMM_ERROR << "Expecting an outputMotion of type Motion..." << endl;
        return false;
    }


    unsigned int pos = currentOutput->getNumFrames();

    if (!increment) // etwas unschoen
       pos = inputMarkerMotion->getNumFrames()-1;

    MMM::MarkerDataPtr frame = inputMarkerMotion->getFrame(pos);
	if (!frame)
	{
		MMM_ERROR << "Could not grab frame " << pos << " from input marker motion" << endl;
		return false;
	}

    if (!fitModel(frame, paramCheckImprovement, paramIKStepSize, paramIKMinChange, paramIKSteps, paramPerfomMinOneStep, paramJointLimitsBoxConstraints))
	{
		MMM_ERROR << "Error while fitting model..." << endl;
		return false;
	}
	int nrDOF = (int)rns->getSize();
	Eigen::VectorXf jv(nrDOF);
	rns->getJointValues(jv);

    MotionFramePtr mf;
    if (increment || outputMotion->getNumFrames()==0){
        mf.reset(new MotionFrame(nrDOF));
        outputMotion->addMotionFrame(mf);
    }

    mf=outputMotion->getMotionFrame(outputMotion->getNumFrames()-1);
    mf->joint = jv;
    mf->timestep = frame->timestamp;
    mf->setRootPose(mmmModel->getGlobalPose());
    mf->_customMatrix = _jacobian;

    return true;
}

bool ConverterVicon2MMM::isInitialized()
{
    if (!inputMotion)
        return false;
    if(!outputModel)
        return false;
    if(!inputMarkerMotion)
        return false;
    if(!mmmModel)
        return false;
    if(!rns)
        return false;
    if(inputMarkerMotion->getNumFrames()==0)
        return false;
    if(markerMapping.size() == 0)
        return false;
    return true;
}

bool ConverterVicon2MMM::_setup( rapidxml::xml_node<char>* rootTag )
{
	rapidxml::xml_node<>* node = rootTag;
	std::map<std::string, std::string> markerMappingViconMMM;
    std::string expectedName = name; //ConverterVicon2MMMFactory::getName();
    node = rootTag;//rootTag->first_node();//getChildNodeWithName(expectedName);
    rapidxml::xml_node<>* configNode = rootTag;
	std::string nodeName;
	while (node)
	{

		nodeName = XML::toLowerCase(node->name());
		if (nodeName == "converterconfig" || nodeName == "mapperconfig")
		{
			rapidxml::xml_attribute<> *attr = node->first_attribute("type", 0, false);
			if (!attr)
			{
				MMM_ERROR << "xml Tag: missing attribute 'type'" << endl;
				return false;
			}
			std::string jn = attr->value();
			if (jn.empty())
			{
				MMM_ERROR << "xml tag: null type string" << endl;
				return false;
			}
			XML::toLowerCase(jn);

			XML::toLowerCase(expectedName);
			if (jn != expectedName)
			{
				MMM_ERROR << "xml type tag: Expecting " << expectedName << ", got " << jn << endl;
				return false;
			}
			// found correct node
            configNode = node;
			break;
		}
		else
		{
			// skipping other entries....
			//MMM_ERROR << "Expecting modelprocessorconfig tag, but got " << nodeName << endl;
			//return false;
		}
		node = node->next_sibling();
	}
	if (!node)
	{
		MMM_ERROR << "Could not find modelprocessorconfig tag" << endl;
		return false;
	}
    node = configNode->first_node("model", 0, false);
	if (!node)
	{
		MMM_ERROR << "Could not find model tag" << endl;
		return false;
	}

	// get first sub node
	node = node->first_node();

	std::vector<std::string> jointList;
	std::string name;
	while (node)
	{
		nodeName = XML::toLowerCase(node->name());
		if (nodeName == "markermapping")
		{
			// retrieve marker mapping
			rapidxml::xml_node<>* subNode = node->first_node();
			while (subNode)
			{
				std::string subNodeName = XML::toLowerCase(subNode->name());
				if (subNodeName == "mapping")
				{
					rapidxml::xml_attribute<> *c3dAttr = subNode->first_attribute("c3d", 0, false);
					rapidxml::xml_attribute<> *mmmAttr = subNode->first_attribute("mmm", 0, false);
					if (!c3dAttr)
					{
						MMM_ERROR << "Missing Attribute C3D in mapping tag " << endl;
					}
					else if (!mmmAttr)
					{
						MMM_ERROR << "Missing Attribute MMM in mapping tag " << endl;
					}
					else
					{
						std::string c3dMarkerName = c3dAttr->value();
						std::string mmmMarkerName = mmmAttr->value();
						if (c3dMarkerName.empty() || mmmMarkerName.empty())
						{
							MMM_ERROR << "Empty marker names not allowed in mapping tag " << endl;
						}
						else
						{
							bool performMapping = true;
							if (markerMappingViconMMM.find(c3dMarkerName) != markerMappingViconMMM.end())
							{
								MMM_ERROR << "C3D marker name " << c3dMarkerName << " is mapped twice!" << endl;
								performMapping = false;
							}
							std::map<std::string, std::string>::iterator i = markerMappingViconMMM.begin();
							while (i != markerMappingViconMMM.end())
							{
								if (i->second == mmmMarkerName)
								{
									MMM_ERROR << "MMM marker name " << mmmMarkerName << " is mapped twice!" << endl;
									performMapping = false;
									break;
								}
								i++;
							}
							if (inputMarkerMotion && !inputMarkerMotion->hasMarkerLabel(c3dMarkerName))
							{
								MMM_ERROR << "Skipping entry: Input c3d marker label not found:" << c3dMarkerName << endl;
								performMapping = false;
							}
							if (outputModel && !outputModel->hasMarker(mmmMarkerName))
							{
								MMM_ERROR << "Skipping entry: Output mmm marker label not found:" << mmmMarkerName << endl;
								performMapping = false;
							}
							if (performMapping)
							{
								markerMappingViconMMM[c3dMarkerName] = mmmMarkerName;
							}
							else
							{
								MMM_ERROR << "Error in marker mapping " << c3dMarkerName << "<->" << mmmMarkerName << "." << endl;
							}
						}
					}
				}
				else
				{
					MMM_ERROR << "Ignoring unknown XML tag " << subNodeName << endl;
				}

				subNode = subNode->next_sibling();
			}
		} else if (nodeName == "jointset")
		{
			rapidxml::xml_node<>* subNode = node->first_node();
			while (subNode)
			{
				std::string subNodeName = XML::toLowerCase(subNode->name());
				if (subNodeName == "joint")
				{
					rapidxml::xml_attribute<> *nameattr = subNode->first_attribute("name", 0, false);

					if (!nameattr)
					{
						MMM_ERROR << "Missing Attribute 'name' in joint tag " << endl;
					}
					else
					{
						std::string jointname = nameattr->value();
						if (!jointname.empty())
							jointList.push_back(jointname);
					}
				}
				else
				{
					MMM_ERROR << "Ignoring unknown XML tag " << subNodeName << endl;
				}
				subNode = subNode->next_sibling();
			}
		} else if (nodeName == "name")
		{
			name = node->value();
		}
		else
		{
			MMM_ERROR << "Ignoring unknown XML tag " << nodeName << endl;
		}
		node = node->next_sibling();
	}

	this->markerMapping = markerMappingViconMMM;
	if (jointList.size() == 0)
	{
		MMM_INFO << "No JointSet tag given in XML config, using all revolute joints" << endl;
		if (inputModel)
		{
			MMM_INFO << "No JointSet given, using all revolute joints" << endl;
			std::vector<ModelNodePtr> models = inputModel->getModels();
			for (size_t i = 0; i < models.size(); i++)
			{
				if (models[i]->joint.jointType == eRevolute && !models[i]->name.empty())
				{
					jointList.push_back(models[i]->name);
				}
			}
		}
	}
	modelName = name;
	setupJointOrder(jointList);

    // check for IK parameters
    node = configNode->first_node("ik", 0, false);
    if (node)
    {
        // get first sub node
        node = node->first_node();

        std::vector<std::string> jointList;
        std::string name;
        while (node)
        {
            nodeName = XML::toLowerCase(node->name());
            if (nodeName == "initialikparams")
            {
                paramInitialIKSteps = (int)(XML::getOptionalFloatByAttributeName(node, "iksteps", (float)paramInitialIKSteps));
                paramInitialIKMinChange = XML::getOptionalFloatByAttributeName(node, "minchange", paramInitialIKMinChange);
                paramInitialIKStepSize = XML::getOptionalFloatByAttributeName(node, "stepsize", paramInitialIKStepSize);
                paramInitialCheckImprovement = XML::getOptionalBoolByAttributeName(node, "checkimprovement", paramInitialCheckImprovement);
                paramInitialPerfomMinOneStep = XML::getOptionalBoolByAttributeName(node, "minonestep", paramInitialPerfomMinOneStep);
                paramInitialJointLimitsBoxConstraints = XML::getOptionalBoolByAttributeName(node, "boxconstraints", paramInitialJointLimitsBoxConstraints);
            }
            else if (nodeName == "stepikparams")
            {
                paramIKSteps = (int)(XML::getOptionalFloatByAttributeName(node, "iksteps", (float)paramIKSteps));
                paramIKMinChange = XML::getOptionalFloatByAttributeName(node, "minchange", paramIKMinChange);
                paramIKStepSize = XML::getOptionalFloatByAttributeName(node, "stepsize", paramIKStepSize);
                paramCheckImprovement = XML::getOptionalBoolByAttributeName(node, "checkimprovement", paramCheckImprovement);
                paramPerfomMinOneStep = XML::getOptionalBoolByAttributeName(node, "minonestep", paramPerfomMinOneStep);
                paramJointLimitsBoxConstraints = XML::getOptionalBoolByAttributeName(node, "boxconstraints", paramJointLimitsBoxConstraints);
            }
            else
            {
                MMM_ERROR << "Skipping IK tag: Unknown xml tag:" << nodeName << endl;
            }
            node = node->next_sibling();
        }
    }

    return true;
}



}
